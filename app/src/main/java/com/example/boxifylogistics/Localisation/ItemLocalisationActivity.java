package com.example.boxifylogistics.Localisation;

import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.boxifylogistics.ItemsSheets.ItemsSheetsActivity;
import com.example.boxifylogistics.LoginActivity;
import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.LocalisationResponse;
import com.example.boxifylogistics.Response.LoginResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jean Hempelmann on 24/07/2020.
 * Boxify
 * #StagiaireVie
 */
public class ItemLocalisationActivity extends AppCompatActivity {

    private static final String TAG = "ItemLocalisationActivity";

    LinearLayout itemSearchLayout,palletSearchLayout;
    private EditText itemLocalisation, palletLocalisation;
    private ImageButton searchLocalisation;
    private TextView itemSearchTitle,palletSearchTitle,resultLocalisation;
    private String searchType,qrCode;

    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);

    private String position;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Mise en place de la vue
        setContentView(R.layout.activity_item_localisation);
        // Mise en place de la Toolbar personnalisée
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView logoToolBar = toolbar.findViewById(R.id.logoXmarks);
        logoToolBar.setImageResource(R.drawable.ic_title_localisation);
        setSupportActionBar(toolbar);

        // Initilisation des vues
        itemSearchLayout = findViewById(R.id.ll_item_search_title);
        itemSearchTitle = findViewById(R.id.tv_item_search_title);
        palletSearchLayout = findViewById(R.id.ll_pallet_search_title);
        palletSearchTitle = findViewById(R.id.tv_pallet_search_title);
        itemLocalisation = findViewById(R.id.edt_item_localisation);
        palletLocalisation = findViewById(R.id.edt_pallet_localisation);
        searchLocalisation = findViewById(R.id.search_localisation);
        resultLocalisation = findViewById(R.id.tv_result_localisation);

        // Forcer les lettres en MAJ
        palletLocalisation.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        // Mise en place des transitions
        final TransitionDrawable transitionItemLayout = (TransitionDrawable) itemSearchLayout.getBackground();
        final TransitionDrawable transitionItemTitle = (TransitionDrawable) itemSearchTitle.getBackground();
        final TransitionDrawable transitionPalletLayout = (TransitionDrawable) palletSearchLayout.getBackground();
        final TransitionDrawable transitionPalletTitle = (TransitionDrawable) palletSearchTitle.getBackground();

        searchType = "item";

        /*itemSearchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionItemLayout.reverseTransition(1000);
                transitionItemTitle.reverseTransition(1000);
                transitionPalletLayout.reverseTransition(1000);
                transitionPalletTitle.reverseTransition(1000);
            }
        });
        palletSearchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionItemLayout.startTransition(1000);
                transitionItemTitle.startTransition(1000);
                transitionPalletLayout.startTransition(1000);
                transitionPalletTitle.startTransition(1000);
            }
        });
        */

        // Gestion du focus de la recherche d'article
        itemLocalisation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if(!searchType.equals("item")){
                        transitionItemLayout.reverseTransition(200);
                        transitionItemTitle.reverseTransition(200);
                        transitionPalletLayout.reverseTransition(200);
                        transitionPalletTitle.reverseTransition(200);

                        searchType="item";
                    }
                }
            }
        });
        // Gestion du focus de la recherche de palette
        palletLocalisation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if(!searchType.equals("pallet")) {
                        transitionItemLayout.reverseTransition(200);
                        transitionItemTitle.reverseTransition(200);
                        transitionPalletLayout.reverseTransition(200);
                        transitionPalletTitle.reverseTransition(200);

                        searchType="pallet";
                    }
                }
            }
        });
        // Gestion de l'appui sur le bouton recherche
        searchLocalisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchType.equals("item")){
                        if(itemLocalisation.getText().toString().trim().length() > 0){
                            qrCode=itemLocalisation.getText().toString();
                            if(Character.isDigit(qrCode.charAt(0))){

                                // TODO : recherche d'objet
                                String response = getItemLocalisation(qrCode);
                                resultLocalisation.setText(response);
                                //resultLocalisation.setText(qrCode);

                            }else{
                                Toast.makeText(ItemLocalisationActivity.this,"Ce QR Code ne correspond pas à un objet",Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(ItemLocalisationActivity.this,"Entrez le QR code de l'article",Toast.LENGTH_SHORT).show();
                        }
                }else if(searchType.equals("pallet")){
                    if(palletLocalisation.getText().toString().trim().length() > 0){
                        qrCode=palletLocalisation.getText().toString();
                        if(qrCode.startsWith("P")){

                            // TODO : recherche de palette
                            String response = getPalletLocalisation(qrCode);
                            resultLocalisation.setText(response);
                            //resultLocalisation.setText(qrCode);

                        }else{
                            Toast.makeText(ItemLocalisationActivity.this,"Ce QR Code ne correspond pas à une palette",Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(ItemLocalisationActivity.this,"Entrez le QR code de la palette",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    /**
     * Récupération de la position de l'article
     * @param qrCode
     */
    private String getItemLocalisation(final String qrCode) {
        Log.e(TAG,qrCode);
        apiRequest.getItemLocalisation(GetAndSetToken.getBearerWithToken(),qrCode).enqueue(new Callback<JsonObject>(){
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                Log.e("QR Code envoyé",qrCode);
                Log.e(TAG, String.valueOf(response.body()));

                // Si on reçoit un "OK"
                if(response.code() == 200){
                    //position = ""+response.body().addressString;
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    if(data.get("address_string").getAsString() == null){
                        Toast.makeText(ItemLocalisationActivity.this , "Pas de position de définie pour cet article", Toast.LENGTH_SHORT).show();
                        position = " ";
                    }else{
                        position = data.get("address_string").getAsString();
                    }
                } else if(response.code() == 404){
                    Toast.makeText(ItemLocalisationActivity.this , "L'article n'existe pas", Toast.LENGTH_SHORT).show();
                    position = " ";
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG,t.getMessage());
                Toast.makeText(ItemLocalisationActivity.this , "Vérifiez la connexion internet", Toast.LENGTH_SHORT).show();
                position = " ";
            }
        });
        return position;
    }

    /**
     * Récupération de la position de la palette
     * @param qrCode
     */
    private String getPalletLocalisation(String qrCode) {
        apiRequest.getPalletLocalisation(GetAndSetToken.getBearerWithToken(),qrCode).enqueue(new Callback<JsonObject>(){
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                // Si on reçoit un "OK"
                if(response.code() == 200){
                    // TODO : Afficher la position
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    if(data.get("address_string").getAsString() == null){
                        Toast.makeText(ItemLocalisationActivity.this , "Pas de position de définie pour cette palette", Toast.LENGTH_SHORT).show();
                        position = " ";
                    }else{
                        position = data.get("address_string").getAsString();
                    }
                } else if(response.code() == 404){
                    Toast.makeText(ItemLocalisationActivity.this , "La pallette n'existe pas", Toast.LENGTH_SHORT).show();
                    position = " ";
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG,t.getMessage());
                Toast.makeText(ItemLocalisationActivity.this , "Vérifiez la connexion internet", Toast.LENGTH_SHORT).show();
                position = String.valueOf(t.getMessage());
            }
        });
        return position;
    }

    /**
     * Méthode appelée à la création du menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Méthode permettant de gérer l'appui sur les différents items du menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
