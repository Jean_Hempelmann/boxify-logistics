package com.example.boxifylogistics;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.model.ItemType;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.ObjectTypeViewHolder> implements Filterable {

    List<ItemType> objectTypes;
    List<ItemType> objectTypesFull;
    private Context context;

    @Override
    public Filter getFilter() {
        return TypeFilter;
    }


    public interface ItemClickListener {
        void onItemClick(int position);
    }

    ItemClickListener itemClickListener;


    public class ObjectTypeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public  ImageView image ;
        public TextView name ;

        public ObjectTypeViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.iv_object_type);
            name=itemView.findViewById(R.id.tv_object_type);
            image.setOnClickListener(this);
            name.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(getAdapterPosition());
        }

    }


    public void setItemClickListener(ItemClickListener itemClickListener){

        this.itemClickListener=itemClickListener;

    }


    public void setContext(Context context) {
        this.context = context;
    }



    @Override
    public ObjectTypeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View objectTypeView = layoutInflater.inflate(R.layout.object_type_item,viewGroup,false);

        return new ObjectTypeViewHolder(objectTypeView);
    }


    public void setData(List<ItemType> objectTypes) {
        if (objectTypes != null && !objectTypes.isEmpty()) {
            objectTypes.sort(new Comparator<ItemType>() {
                @Override
                public int compare(ItemType o1, ItemType o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
        }
        this.objectTypes = objectTypes;
        if(objectTypes != null)
            this.objectTypesFull = new ArrayList<>(objectTypes);
        else
            this.objectTypesFull = new ArrayList<>();


    }


    @Override
    public void onBindViewHolder(ObjectTypeViewHolder objectTypeViewHolder, int i) {

        populateItemRows((ObjectTypeViewHolder) objectTypeViewHolder, i);

    }

    @Override
    public int getItemCount() {

        if (objectTypes != null)
            return objectTypes.size();
        else
            return 0;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private void populateItemRows(ObjectTypeViewHolder mainViewHolder, int i) {


        Log.i("imageUrlBoxify ", objectTypes.get(i).getImage());

        Glide.with(context)
                .load(objectTypes.get(i).getImage())
                .into(mainViewHolder.image)
        ;

        mainViewHolder.name.setText(objectTypes.get(i).getFr().replace("Ã¨","è").replace("Ã©","é").replace("Ã ","à"));

        Log.i("objectTypeVerif ", objectTypes.get(i).toString());
    }

    private Filter TypeFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ItemType> filteredIT = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredIT.addAll(objectTypesFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (ItemType it : objectTypesFull) {
                    if (it.getName().toLowerCase().contains(filterPattern)) {
                        filteredIT.add(it);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredIT;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            objectTypes.clear();
            objectTypes.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


}


