package com.example.boxifylogistics;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.boxifylogistics.Indexation.Fragments.ParcelListFragment;
import com.example.boxifylogistics.Indexation.StartIndexationFragment;

public class PageAdapter extends FragmentPagerAdapter {

    /**
     * Constructeur
     * @param mgr
     */
    public PageAdapter(FragmentManager mgr) {
        super(mgr);
    }

    /**
     * Retourne le nombre de page du ViewPager, ici 2
     * @return
     */
    @Override
    public int getCount() {
        return(2);
    }

    /**
     * Méthode renvoyant le bon fragment en fonction de la page
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: //Page number 1
                return ParcelListFragment.newInstance();
            case 1: //Page number 2
                return StartIndexationFragment.newInstance();
            default:
                return null;
        }
    }

    /**
     * Méthode renvoyant le bon titre en fonction de la page
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: //Page number 1
                return "Rendez-vous";
            case 1: //Page number 2
                return "Actions";
            default:
                return null;
        }
    }
}