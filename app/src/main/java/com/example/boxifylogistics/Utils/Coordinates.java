package com.example.boxifylogistics.Utils;

public class Coordinates {
    private double latitude;
    private double longitude;

    public Coordinates(double longitude, double latitude){
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getDistance( Coordinates c){
        double y = c.latitude - this.latitude;
        double x = c.longitude - this.longitude;
        x = Math.pow(x,2);
        y = Math.pow(y,2);
        return Math.sqrt(y-x);
    }
}
