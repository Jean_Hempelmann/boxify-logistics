package com.example.boxifylogistics.Utils;

import android.content.Context;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Classe permettant de gérer le stockage interne du téléphone (notamment du token)
 */
public class StorageUtils {

    private static final String FILENAME = "token.txt";
    private static final String FOLDERNAME = "boxyToken";

    /**
     * Méthode appelée pour créer ou récupérer un fichier
     * @param destination
     * @param fileName
     * @param folderName
     * @return
     */
    private static File createOrGetFile(File destination, String fileName, String folderName){
        File folder = new File(destination, folderName);
        return new File(folder, fileName);
    }

    /**
     * Permet de lire le contenu d'un fichier passé en paramètre
     * @param context
     * @param file
     * @return
     */
    private static String readOnFile(Context context, File file){

        String result = null;
        // Vérification si le fichier existe
        if (file.exists()) {
            // BufferedReader permet de lire, grâce à sa mémoire tampon, un flux de données de manière efficiente
            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(file));
                try {
                    StringBuilder sb = new StringBuilder();
                    String line = br.readLine();
                    while (line != null) {
                        sb.append(line);
                        sb.append("\n");
                        line = br.readLine();
                    }
                    result = sb.toString();
                }
                finally {
                    br.close();
                }
            }
            catch (IOException e) {
                Toast.makeText(context, "Une erreur est survenue", Toast.LENGTH_LONG).show();
            }
        }

        return result;
    }

    // ---

    /**
     * Permet d'écrire du texte dans un fichier
     * @param context
     * @param text
     * @param file
     */
    private static void writeOnFile(Context context, String text, File file){

        try {
            file.getParentFile().mkdirs();
            FileOutputStream fos = new FileOutputStream(file);
            Writer w = new BufferedWriter(new OutputStreamWriter(fos));

            try {
                w.write(text);
                w.flush();
                fos.getFD().sync();
            } finally {
                w.close();
            }

        } catch (IOException e) {
            Toast.makeText(context, "Une erreur est survenue", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Permet de lire du texte dans un fichier se trouvant dans un espace de stockage défini en paramètre.
     * @param rootDestination
     * @param context
     * @return
     */
    public static String getTextFromStorage(File rootDestination, Context context){
        File file = createOrGetFile(rootDestination, FILENAME, FOLDERNAME);
        return readOnFile(context, file);
    }

    /**
     * Permet d'écrire du texte dans un fichier se trouvant dans un espace de stockage défini en paramètre.
     * @param rootDestination
     * @param context
     * @param text
     */
    public static void setTextInStorage(File rootDestination, Context context, String text){
        File file = createOrGetFile(rootDestination, FILENAME, FOLDERNAME);
        writeOnFile(context, text, file);
    }

}
