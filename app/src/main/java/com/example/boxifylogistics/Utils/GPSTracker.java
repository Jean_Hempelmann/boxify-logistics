package com.example.boxifylogistics.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class GPSTracker extends Service implements LocationListener {


    private static final int REQUEST_CODE_PERMISSION = 2;
    String[] mPermission = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private MutableLiveData<Location> actualLocation = new MutableLiveData<>();
    private MutableLiveData<Double> actualLatitude = new MutableLiveData<>();
    private MutableLiveData<Double> actualLongitude = new MutableLiveData<>();

    boolean isGPSEnabled = false;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES =  10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 10 * 1; // 1 minute

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    boolean canGetLocation = false;


    protected LocationManager locationManager;

    private Activity mActivity;

    public GPSTracker(Activity context) {
        this.mActivity = context;
        getLocation();
    }

    public Location getLocation() {
        checkPermission();
        locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);

        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!isGPSEnabled) {
            this.showSettingsAlert();
        }
        else {
            this.canGetLocation = true;
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("GPS Enabled", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        actualLocation.setValue(location);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            actualLatitude.setValue(latitude);
                            actualLongitude.setValue(longitude);
                        }
                    }
                }
            }
        }
        return location;
    }

    public void checkPermission() {
        if (ActivityCompat.checkSelfPermission(mActivity, mPermission[0]) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mActivity, mPermission[2]) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(mActivity, new String[]{mPermission[0], mPermission[2]}, REQUEST_CODE_PERMISSION);
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mActivity.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Function to get latitude
     */

    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        return latitude;
    }

    /**
     * Function to get longitude
     */

    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }
        return longitude;
    }

    public LiveData<Double> getLongitudeBinded(){
        return actualLongitude;
    }
    public LiveData<Double> getLatitudeBinded(){
        return actualLatitude;
    }
    public LiveData<Location> getLocationBinded(){
        return actualLocation;
    }



    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "onLocationChanged: "+location);
        this.location = location;
        actualLocation.setValue(location);
        actualLongitude.setValue(location.getLongitude());
        actualLatitude.setValue(location.getLatitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }




}
