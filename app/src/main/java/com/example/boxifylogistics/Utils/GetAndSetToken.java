package com.example.boxifylogistics.Utils;

import android.app.Application;

public class GetAndSetToken extends Application {

    private static String token;

    public static String getToken() {
        return token;
    }

    public static String getBearerWithToken() {
        if(token!=null){
            return "Bearer "+token.trim();
        }else{
            return null;
        }
    }

    public void setToken(String token) {
        this.token = token;
    }
}