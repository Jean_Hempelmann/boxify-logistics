package com.example.boxifylogistics.Repository;

import android.util.ArrayMap;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.model.Parcel;
import com.example.boxifylogistics.model.PickUp;
import com.example.boxifylogistics.Response.ListResponse;
import com.example.boxifylogistics.Response.Response;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.model.Parcel;
import com.example.boxifylogistics.model.PickUp;
import com.example.boxifylogistics.Response.ListResponse;
import com.example.boxifylogistics.Response.Response;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;


public class PickUpRepository {
    private ApiRequest apiRequest;
    String TAG = "PickUpRepository";

    public PickUpRepository() {
        apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    }

    public MutableLiveData<Boolean> checkQrCode(String qr, int idPickup)  {
        final MutableLiveData<Boolean> qrValid = new MutableLiveData<>(true);
        apiRequest.checkScan((GetAndSetToken.getBearerWithToken()),qr,idPickup).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "onResponse: " + response);
                Log.e(TAG, "onResponse: " + call.request().toString());
                qrValid.setValue(response.isSuccessful());
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onResponse: " + call.request().toString());
                Log.e(TAG, "onFailurrre: " + t);
            }
        });
        return qrValid;
    }

    public MutableLiveData<Boolean> checkFirstQrCode(String qr, int idPickup) {
        final MutableLiveData<Boolean> value = new MutableLiveData<>(false);
        apiRequest.checkFirstScan((GetAndSetToken.getBearerWithToken()),qr, idPickup).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "onResponse: " + response.body());
                value.setValue(response.isSuccessful());

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        return value;
    }

    public MutableLiveData<PickUp> getPickUpDetails(int delivery_id) {
        final MutableLiveData<PickUp> pickup = new MutableLiveData<>();
        apiRequest.getDetailsPickup((GetAndSetToken.getBearerWithToken()),delivery_id).enqueue(new Callback<Response<PickUp>>() {
            @Override
            public void onResponse(Call<Response<PickUp>> call, retrofit2.Response<Response<PickUp>> response) {
                pickup.setValue(response.body().getData());
            }
            @Override
            public void onFailure(Call<Response<PickUp>> call, Throwable t) {
                Log.e(TAG, "getDeliveryDetails + onFailure: " + t);
            }
        });
        return pickup;
    }

    public MutableLiveData<Boolean> checkSecondQrCode(String qr, int idPickup) {
        final MutableLiveData<Boolean> value = new MutableLiveData<>();
        apiRequest.checkSecondScan((GetAndSetToken.getBearerWithToken()),qr, idPickup).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "onResponse: " + response);
                Log.e(TAG, "onResponse: " + call.request().toString());
                value.setValue(response.isSuccessful());
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onResponse: " + call.request().toString());
                Log.e(TAG, "onFailurrre: " + t);
            }
        });
        return value;
    }

    public MutableLiveData<ArrayList<PickUp>> getPickup(Date date) {
        final MutableLiveData<ArrayList<PickUp>> packages = new MutableLiveData<>(new ArrayList<PickUp>());
        apiRequest.getAppoinmentsList((GetAndSetToken.getBearerWithToken()),date).enqueue(new Callback<ListResponse<Parcel>>() {
            @Override
            public void onResponse(Call<ListResponse<Parcel>> call, retrofit2.Response<ListResponse<Parcel>> response) {

            }

            @Override
            public void onFailure(Call<ListResponse<Parcel>> call, Throwable t) {

            }
        });

        return packages;
    }

    public MutableLiveData<Map<String,Integer>> getCheckUpState(int pickupId) {
        final MutableLiveData<Map<String, Integer>> dataChekupState = new MutableLiveData<>();
        final Map<String, Integer> dtcus = new ArrayMap<>();
        dataChekupState.setValue(dtcus);
        apiRequest.getCheckupState((GetAndSetToken.getBearerWithToken()),pickupId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    if (data.get("first_qr_code").isJsonNull())
                        dtcus.put("first_qr_code", 0);
                    else
                        dtcus.put("first_qr_code", data.get("first_qr_code").getAsInt());
                    if (data.get("last_qr_code").isJsonNull())
                        dtcus.put("last_qr_code", 0);
                    else
                        dtcus.put("last_qr_code", data.get("last_qr_code").getAsInt());
                    dtcus.put("nb_items_loaded", data.get("nb_items_loaded").getAsInt());
                    dtcus.put("nb_items_not_loaded", data.get("nb_items_not_loaded").getAsInt());
                    dtcus.put("nb_items_total", data.get("nb_items_total").getAsInt());
                    dataChekupState.setValue(dtcus);
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t);
            }
        });

        return dataChekupState;
    }
    public MutableLiveData<ArrayList<String>> getPickupItemRemaining(int pickupId){
        final MutableLiveData<ArrayList<String>> st = new MutableLiveData<>(new ArrayList<String>());
        final MutableLiveData<ArrayList<String>> st2 = new MutableLiveData<>(new ArrayList<String>());

        apiRequest.getCheckupState((GetAndSetToken.getBearerWithToken()),pickupId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if(response.isSuccessful()){
                    Log.e(TAG, "onResponse: " + response.body());
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    Log.e(TAG, "onResponse: " + data.get("items"));
                        /*for (JsonElement it: data.get("items").getAsJsonArray()) {

                        st.getValue().add(it.getAsString());
                        st.setValue(st.getValue());

                    }*/
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );
            }
        });

        return st ;
    }

    /*public MutableLiveData<Boolean> validatePickUp(int pickup_id, String signature) {
        final MutableLiveData<Boolean> bl = new MutableLiveData<>(false);
        apiRequest.signPickup((GetAndSetToken.getBearerWithToken()),pickup_id, signature).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                bl.setValue(response.isSuccessful());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );
            }
        });
        return bl;
    }*/

    public MutableLiveData<PickUp> getPickupfromIdList_Temporary(String date, final int pickup_id) {
        final MutableLiveData<PickUp> pickup = new MutableLiveData<>();
        apiRequest.getAppoinments((GetAndSetToken.getBearerWithToken()),date).enqueue(new Callback<ListResponse<Parcel>>() {
            @Override
            public void onResponse(Call<ListResponse<Parcel>> call, retrofit2.Response<ListResponse<Parcel>> response) {
                Parcel p = null;
                for (Parcel par : response.body().getData()) {
                    if (par.getAppointment().getAppointment_id() == pickup_id) {
                        p = par;
                    }
                }
                if (p != null) {
                    PickUp pu = new PickUp(p.getAppointment(), p.getUser(), p.getAddress());
                    System.out.println(pu.getAppointment());
                    pickup.setValue(pu);
                }
            }

            @Override
            public void onFailure(Call<ListResponse<Parcel>> call, Throwable t) {

            }
        });
        return pickup;
    }

    public void endPickup(int delivertId, File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("signature", file.getName(), requestFile);
        apiRequest.signPickup((GetAndSetToken.getBearerWithToken()),delivertId, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "onResponse: " + response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t);
            }
        });
    }
}
