package com.example.boxifylogistics.Repository;

import android.util.ArrayMap;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.boxifylogistics.Response.ListResponse;
import com.example.boxifylogistics.Response.Response;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.model.Parcel;
import com.example.boxifylogistics.model.DropOff;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;


public class DropOffRepository {
    private ApiRequest apiRequest;
    String TAG = "DropOffRepository";

    public DropOffRepository() {
        apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    }

    public MutableLiveData<DropOff> getDropOffDetails(int delivery_id) {
        final MutableLiveData<DropOff> dropOff = new MutableLiveData<>();
        apiRequest.getDetailsDropOff((GetAndSetToken.getBearerWithToken()),delivery_id).enqueue(new Callback<Response<DropOff>>() {
            @Override
            public void onResponse(Call<Response<DropOff>> call, retrofit2.Response<Response<DropOff>> response) {
                dropOff.setValue(response.body().getData());
            }
            @Override
            public void onFailure(Call<Response<DropOff>> call, Throwable t) {
                Log.e(TAG, "getDropOff + onFailure: " + t);
            }
        });
        return dropOff;
    }


    public void endDropOff(int delivertId, File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("signature", file.getName(), requestFile);
        apiRequest.signDropOff((GetAndSetToken.getBearerWithToken()),delivertId, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "onResponse: " + response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t);
            }
        });
    }
}
