package com.example.boxifylogistics.Repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.model.Parcel;
import com.example.boxifylogistics.Response.ListResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;

import java.util.ArrayList;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ParcelRepository {
    String TAG = "PackageRepository";
    private ApiRequest apiRequest;

    public ParcelRepository() {
        apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    }

    public LiveData<ArrayList<Parcel>> getPackagesList(String date){
        final MutableLiveData<ArrayList<Parcel>> parcels = new MutableLiveData<>(new ArrayList<Parcel>());
        apiRequest.getAppoinments((GetAndSetToken.getBearerWithToken()),date).enqueue(new Callback<ListResponse<Parcel>>() {
            @Override
            public void onResponse(Call<ListResponse<Parcel>> call, Response<ListResponse<Parcel>> response) {
                if(response.isSuccessful()){
                    parcels.setValue((ArrayList<Parcel>) response.body().getData());
                    parcels.getValue().sort(new Comparator<Parcel>() {
                        @Override
                        public int compare(Parcel o1, Parcel o2) {
                            return o1.getAppointment().getHourAppoint().compareTo(o2.getAppointment().getHourAppoint());
                        }
                    });
                }
                else
                    parcels.setValue(null);
            }

            @Override
            public void onFailure(Call<ListResponse<Parcel>> call, Throwable t) {
                Log.e(TAG, "onFailure: "+call.request() );
                Log.e(TAG, "onFailure: getPackagesList "+t);
                parcels.setValue(null);
            }
        });
        return parcels;
    }
}
