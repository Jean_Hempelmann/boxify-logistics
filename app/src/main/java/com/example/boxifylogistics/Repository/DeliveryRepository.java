package com.example.boxifylogistics.Repository;

import android.util.ArrayMap;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.model.Delivery;
import com.example.boxifylogistics.Response.Response;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class DeliveryRepository {

    private ApiRequest apiRequest;
    String TAG = "DeliveryRepository";


    public DeliveryRepository() {
        apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    }

    public MutableLiveData<Delivery> getDeliveryDetails(int delivery_id) {
        final MutableLiveData<Delivery> delivery = new MutableLiveData<>();
        apiRequest.getDetailsDelivery((GetAndSetToken.getBearerWithToken()),delivery_id).enqueue(new Callback<Response<Delivery>>() {
            @Override
            public void onResponse(Call<Response<Delivery>> call, retrofit2.Response<Response<Delivery>> response) {

                delivery.setValue(response.body().getData());
                Log.e(TAG,"onResponse: "+delivery.getValue().toString() );
                Log.e(TAG,"onResponse: 2");
            }
            @Override
            public void onFailure(Call<Response<Delivery>> call, Throwable t) {
                Log.e(TAG, "getDeliveryDetails + onFailure: "+t );
            }
        });
        return delivery;
    }

    public MutableLiveData<Map<String, Integer>> getDeliveryState(int deliveryId) {
        final MutableLiveData<Map<String, Integer>> dataChekupState = new MutableLiveData<>();
        final Map<String, Integer> dtcus = new ArrayMap<>();
        dataChekupState.setValue(dtcus);
        apiRequest.getDeliveryState((GetAndSetToken.getBearerWithToken()),deliveryId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    dtcus.put("nb_items_total", data.get("nb_items_total").getAsInt());
                    dtcus.put("nb_items_loaded", data.get("nb_items_loaded").getAsInt());
                    dtcus.put("nb_items_not_loaded", data.get("nb_items_not_loaded").getAsInt());
                    dtcus.put("nb_items_delivered", data.get("nb_items_delivered").getAsInt());
                    dataChekupState.setValue(dtcus);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t);
            }
        });
        return dataChekupState;
    }

    public MutableLiveData<ArrayList<String>> getItemsAlreadyLoaded(int deliveryId) {
        final MutableLiveData<ArrayList<String>> dataChekupState = new MutableLiveData<>();
        final ArrayList<String> dtcus = new ArrayList<>();
        apiRequest.getDeliveryState((GetAndSetToken.getBearerWithToken()),deliveryId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    for (JsonElement e : data.get("items_loaded").getAsJsonArray()) {
                        dtcus.add(e.getAsString());
                    }
                    dataChekupState.setValue(dtcus);
                    Log.e(TAG,"onResponse: "+dataChekupState.getValue());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t);
            }
        });
        return dataChekupState;
    }

    public MutableLiveData<Map<String,ArrayList<String>>> getStateDelivery(int deliveryId) {
        final MutableLiveData<Map<String,ArrayList<String>>> dataChekupState = new MutableLiveData<>();
        final Map<String,ArrayList<String>> ok = new ArrayMap<>();
        final ArrayList<String> dtcus = new ArrayList<>();
        apiRequest.getDeliveryState((GetAndSetToken.getBearerWithToken()),deliveryId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    for (JsonElement e : data.get("items_delivered").getAsJsonArray()) {
                        dtcus.add(e.getAsString());
                    }
                    ok.put("items_delivered",new ArrayList<String>(dtcus));
                    dtcus.clear();
                    for (JsonElement e : data.get("items_loaded").getAsJsonArray()) {
                        dtcus.add(e.getAsString());
                    }
                    ok.put("items_loaded",new ArrayList<String>(dtcus));
                    dtcus.clear();
                    for (JsonElement e : data.get("items_not_loaded").getAsJsonArray()) {
                        dtcus.add(e.getAsString());
                    }
                    ok.put("items_not_loaded",new ArrayList<String>(dtcus));
                    dataChekupState.postValue(ok);
                    Log.e(TAG, "getItemsAlreadyDelivered ; onResponse: "+dataChekupState.getValue() );
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t);
            }
        });
        return dataChekupState;
    }

    public MutableLiveData<ArrayList<String>> getItemsAlreadyDelivered(int deliveryId) {
        final MutableLiveData<ArrayList<String>> dataChekupState = new MutableLiveData<>();
        final ArrayList<String> dtcus = new ArrayList<>();
        apiRequest.getDeliveryState((GetAndSetToken.getBearerWithToken()),deliveryId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    for (JsonElement e : data.get("items_delivered").getAsJsonArray()) {
                        dtcus.add(e.getAsString());
                    }
                    dataChekupState.setValue(dtcus);
                    Log.e(TAG, "getItemsAlreadyDelivered ; onResponse: "+dataChekupState.getValue() );
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t);
            }
        });
        return dataChekupState;
    }


    public MutableLiveData<Boolean> loadItem(int deliveryId, String qr) {
        final MutableLiveData<Boolean> qrValid = new MutableLiveData<>(true);
        apiRequest.loadItem((GetAndSetToken.getBearerWithToken()),deliveryId,qr).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "onResponse: "+response );
                Log.e(TAG, "onResponse: "+call );
                qrValid.setValue(response.isSuccessful());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );

            }
        });
        return qrValid;
    }

    public MutableLiveData<Boolean> deliverItem(int deliveryId, String qr) {
        final MutableLiveData<Boolean> qrValid = new MutableLiveData<>(true);
        apiRequest.deliverItem((GetAndSetToken.getBearerWithToken()),deliveryId,qr).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "onResponse: "+response );
                qrValid.setValue(response.isSuccessful());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: "+ call.request().toString() );
                Log.e(TAG, "onFailure: "+t );
            }
        });
        return qrValid;
    }

    public void endDelivery(int delivertId,File file){
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("signature", file.getName(), requestFile);

            apiRequest.signDelivery((GetAndSetToken.getBearerWithToken()),delivertId,body).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                    Log.e(TAG, "onResponse: "+response );
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e(TAG, "onFailure: "+t );
                }
            });
    }

    /*public MutableLiveData<Boolean> validateDelivery(int delivery_id, String signature){
        final MutableLiveData<Boolean> bl = new MutableLiveData<>(false);
        apiRequest.signPickup((GetAndSetToken.getBearerWithToken()),delivery_id,signature).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                bl.setValue(response.isSuccessful());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );
            }
        });
        return bl;
    }*/

}
