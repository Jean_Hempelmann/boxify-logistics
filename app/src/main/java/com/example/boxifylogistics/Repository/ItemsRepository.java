package com.example.boxifylogistics.Repository;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.boxifylogistics.model.IndexedItem;
import com.example.boxifylogistics.model.ItemType;
import com.example.boxifylogistics.Response.IndexedItemResponse;
import com.example.boxifylogistics.Response.ItemTypesResponse;
import com.example.boxifylogistics.Retrofit.ApiPhotoRequest;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitPhotoRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemsRepository {

    private static final String TAG = ItemsRepository.class.getSimpleName();
    private ApiRequest apiRequest;
    private ApiPhotoRequest apiPhotoRequest;

    public ItemsRepository() {
        apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
        apiPhotoRequest = RetrofitPhotoRequest.getRetrofitInstance().create(ApiPhotoRequest.class);
    }


    /** Utilisé exclusivemen pour convertir l'objet d'indexation car bug de conversion automatique avec Retrofit**/


    /*
    public MutableLiveData<Response<ResponseBody>> getItemByQrCode(String qr)  {
        final MutableLiveData<Response<ResponseBody>> data = new MutableLiveData<>();

        apiRequest.getItemByQrCode(qr).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {


                    Log.i("chesh2Resp ", String.valueOf(response.code()));

                   // if(response.code(){

                        IndexedItem indexedItem=new IndexedItem(response.body().string());
                        Log.i("chesh2Resp ", indexedItem.toString());

                  //  }

                    //data.setValue(response);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                data.setValue(null);
            }
        });
        return data;
    } */

    public LiveData<IndexedItemResponse> getItemByQrCode(String qr)  {
        final MutableLiveData<IndexedItemResponse> data = new MutableLiveData<>();
        Log.e(TAG, "getItemByQrCode: "+qr );
        apiRequest.getItemByQrCode((GetAndSetToken.getBearerWithToken()),qr).enqueue(new Callback<IndexedItemResponse>() {
            @Override
            public void onResponse(Call<IndexedItemResponse> call, Response<IndexedItemResponse> response) {
                Log.e(TAG, String.valueOf(response.body()));
                if(response.isSuccessful()==true){

                    data.setValue(response.body());
                }
                else
                    data.setValue(null);

            }

            @Override
            public void onFailure(Call<IndexedItemResponse> call, Throwable t) {
                data.setValue(null);
                Log.e(TAG, String.valueOf(t.getCause()));
                Log.e(TAG, String.valueOf(t.getMessage()));
                Log.e(TAG, String.valueOf(t.getSuppressed()));
                Log.e(TAG, String.valueOf(t.getStackTrace()));
            }
        });
        return data;
    }
    public MutableLiveData<ItemType> getItemTypeOf(int id) {
        final MutableLiveData<ItemType> data = new MutableLiveData<>();
        apiRequest.getItemTypeByitem((GetAndSetToken.getBearerWithToken()),id).enqueue(new Callback<com.example.boxifylogistics.Response.Response<ItemType>>() {
            @Override
            public void onResponse(Call<com.example.boxifylogistics.Response.Response<ItemType>> call, Response<com.example.boxifylogistics.Response.Response<ItemType>> response) {
              if(response.isSuccessful())
                    data.setValue(response.body().getData());
              else
                  data.setValue(null);
            }

            @Override
            public void onFailure(Call<com.example.boxifylogistics.Response.Response<ItemType>> call, Throwable t) {
                data.setValue(null);
            }
        });


        return data;
    }
    public LiveData<ItemTypesResponse> getItemType() {
        final MutableLiveData<ItemTypesResponse> data = new MutableLiveData<>();
        apiRequest.getItemType()
                .enqueue(new Callback<ItemTypesResponse>() {

                    @Override
                    public void onResponse(Call<ItemTypesResponse> call, Response<ItemTypesResponse> response) {


                        if (response.body() != null) {
                            data.setValue(response.body());

                        }

                    }

                    @Override
                    public void onFailure(Call<ItemTypesResponse> call, Throwable t) {

                        Log.i("onFailureCall ", t.getLocalizedMessage());
                        data.setValue(null);

                    }
                });


        return data;
    }

    public LiveData<IndexedItem> postIndexation(IndexedItem indexedItem) {
        final MutableLiveData<IndexedItem> data = new MutableLiveData<>();
        apiRequest.postIndexation((GetAndSetToken.getBearerWithToken()),indexedItem).enqueue(new Callback<IndexedItem>() {
            @Override
            public void onResponse(Call<IndexedItem> call, Response<IndexedItem> response) {
                Log.e(TAG, "onResponse: "+response );
                if(response.isSuccessful()) {
                    data.setValue(response.body());
                    Log.i("chesh22", "post submitted to API." + response.body().toString());
                }
                else
                    data.setValue(null);
            }

            @Override
            public void onFailure(Call<IndexedItem> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t );
                Log.i("chesh22", "Unable to submit post to API.");
                data.setValue(null);
            }
        });
        return data;
    }
    public LiveData<IndexedItem> putIndexation(IndexedItem indexedItem) {
        final MutableLiveData<IndexedItem> data = new MutableLiveData<>();
        Log.e(TAG, "putIndexation: "+indexedItem );
        apiRequest.putIndexation((GetAndSetToken.getBearerWithToken()),indexedItem.getBoxify_id(),indexedItem).enqueue(new Callback<IndexedItem>() {
            @Override
            public void onResponse(Call<IndexedItem> call, Response<IndexedItem> response) {
                Log.e(TAG, "onResponse: "+response );
                if(response.isSuccessful()) {
                    data.setValue(response.body());
                    Log.i("chesh22", "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<IndexedItem> call, Throwable t) {
                Log.i("chesh22", "Unable to submit post to API.");
                data.setValue(null);
            }
        });
        return data;
    }
    public LiveData<IndexedItem> getPhoto(final IndexedItem it, String img){
        final MutableLiveData<IndexedItem> data = new MutableLiveData<>();

        it.setPhoto(img);
        Log.e(TAG, "getPhoto: "+it );
        apiPhotoRequest.getItemByQrCode(img.substring(img.lastIndexOf("/") + 1)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                    it.setPhotoBitmap(bmp);

                    data.setValue(it);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t);

            }
        });

        return data;

    }

}
