package com.example.boxifylogistics;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class TestScanActivity extends Activity {
  private ScanDevice sm;
  private final static String SCAN_ACTION = "scan.rcv.message";
  private String barcodeStr;
  private EditText showScanResult;

  private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {

    @Override
    public void onReceive(Context context, Intent intent) {

      byte[] barocode = intent.getByteArrayExtra("barocode");
      int barocodelen = intent.getIntExtra("length", 0);
      byte temp = intent.getByteExtra("barcodeType", (byte) 0);
      byte[] aimid = intent.getByteArrayExtra("aimid");
      barcodeStr = new String(barocode, 0, barocodelen);
      showScanResult.append(barcodeStr);
      showScanResult.append("\n");
      sm.stopScan();
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.testcan_main_activity);
    sm = new ScanDevice();
    sm.setOutScanMode(0);//启动就是广播模式
    Spinner spinner = (Spinner) findViewById(R.id.spinner);
    String[] arr = { getString(R.string.brodcast_mode), getString(R.string.input_mode) };
    //创建ArrayAdapter对象
    ArrayAdapter<String> adapter =
        new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arr);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner.setAdapter(adapter);
    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
          sm.setOutScanMode(0);
        } else if (position == 1) {
          sm.setOutScanMode(1);
        }
      }
      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    showScanResult = (EditText) findViewById(R.id.editText1);
  }

  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.openScanner:
        System.out.println("openScanner = " + sm.getOutScanMode());
        sm.openScan();
        break;
      case R.id.closeScanner:
        sm.closeScan();
        break;
      case R.id.startDecode:
        sm.startScan();
        break;
      case R.id.stopDecode:
        sm.stopScan();
        break;
      case R.id.start_continue:
        sm.setScanLaserMode(4);
        break;
      case R.id.stop_continue:
        sm.setScanLaserMode(8);
        break;
      case R.id.close:
        finish();
        break;
      case R.id.clear:
        showScanResult.setText("");
        break;
      default:
        break;
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mScanReceiver);
  }

  @Override
  protected void onResume() {
    super.onResume();
    IntentFilter filter = new IntentFilter();
    filter.addAction(SCAN_ACTION);
    registerReceiver(mScanReceiver, filter);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (sm != null) {
      sm.stopScan();
      sm.setScanLaserMode(8);
      sm.closeScan();
    }
  }
}