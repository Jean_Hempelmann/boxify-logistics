package com.example.boxifylogistics.Indexation.Fragments;

import android.app.SearchManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.R;
import com.example.boxifylogistics.TypeAdapter;
import com.example.boxifylogistics.model.ItemType;
import com.example.boxifylogistics.Response.ItemTypesResponse;
import com.example.boxifylogistics.ViewModel.IndexViewModel;

import java.util.List;


public class ObjectTypeFragment extends Fragment implements TypeAdapter.ItemClickListener {

    private static final String TAG = "ObjectTypeFragment";
    private RecyclerView recyclerView;

    private LinearLayoutManager linearLayoutManager;
    private TypeAdapter typeAdapter;


    SearchManager searchManager;

    //private final ActivityHelper<ObjectTypeActivity> at = new ActivityHelper<>(getActivity());

    List<ItemType> objectTypes;

    IndexViewModel indexationViewModel;

    private ProgressBar progressBar;
    private SearchView searchView;

    boolean fromEdit=false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_object_type, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*objectTypes=Type.createTypes();  */

        if(getArguments()!=null)
           fromEdit=getArguments().getBoolean("fromEdit");


        initialization();
        getObjectTypes();

     //   String qrCode=getIntent().getStringExtra("qrCode");
        // at.showToastShort("Qr Code="+qrCode);




    }


    private void initialization() {


        recyclerView = getActivity().findViewById(R.id.rv_object_type);
        searchView = getActivity().findViewById(R.id.scv);


        typeAdapter = new TypeAdapter();
        typeAdapter.setContext(getContext());
        typeAdapter.setItemClickListener(this);


        typeAdapter.setData(objectTypes);
        recyclerView.setAdapter(typeAdapter);


        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager = new GridLayoutManager(getContext(),2);
        recyclerView.setLayoutManager(linearLayoutManager);

        progressBar = getActivity().findViewById(R.id.progressBar);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                typeAdapter.getFilter().filter(newText);
                return false;
            }
        });
        // View Model
        indexationViewModel = ViewModelProviders.of(requireActivity()).get(IndexViewModel.class);





    }

    /**
     * get movies articles from news api
     *
     * @param @null
     */


    private void getObjectTypes() {
        indexationViewModel.getObjectTypes().observe(getActivity(), new Observer<ItemTypesResponse>() {
            @Override
            public void onChanged(ItemTypesResponse itemTypesResponse) {

                if (itemTypesResponse != null) {

                    progressBar.setVisibility(View.GONE);
                    objectTypes = itemTypesResponse.getItems();
                  //  objectTypes.addAll(objectTypes);
                    typeAdapter.setData(objectTypes);

                    typeAdapter.notifyDataSetChanged();

                }
            }
        });
    }


    @Override
    public void onItemClick(int position) {

        indexationViewModel.setItemType(objectTypes.get(position));
        indexationViewModel.setCalculatorItemId(objectTypes.get(position).getCalculator_item_id());
        indexationViewModel.setName(objectTypes.get(position).getName());
        indexationViewModel.setVolume(objectTypes.get(position).getVolume());
        /*if(!indexationViewModel.isEditing())
            indexationViewModel.setDefaultValues(objectTypes.get(position));*/
        loadFragment();
    }

    private void loadFragment() {
        Fragment fragment = new EditFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }


}
