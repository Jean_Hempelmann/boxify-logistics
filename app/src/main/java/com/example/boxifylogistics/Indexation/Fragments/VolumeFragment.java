package com.example.boxifylogistics.Indexation.Fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Utils.Utils;
import com.example.boxifylogistics.databinding.FragmentVolumeBinding;
import com.example.boxifylogistics.ViewModel.IndexViewModel;


public class VolumeFragment extends Fragment {

    EditText et_input_volume;
    ImageButton button;
    CheckBox chb_two_persons;
    CheckBox chb_a_paletiser;

    IndexViewModel indexationViewModel;
    private FragmentVolumeBinding binding;
    private boolean fromEdit;


    public VolumeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentVolumeBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        et_input_volume =getActivity().findViewById(R.id.et_volume);
        button=getActivity().findViewById(R.id.btn_volume_valider);
        chb_two_persons =getActivity().findViewById(R.id.cb_two_persons);
        chb_a_paletiser=getActivity().findViewById(R.id.cb_palettisation);

        btnListener();

        indexationViewModel = ViewModelProviders.of(requireActivity()).get(IndexViewModel.class);

        /*
        if(indexationViewModel.getItemType()!=null) {
            chb_two_persons.setChecked(indexationViewModel.getItemType().getIs_large()==1);
            et_input_volume.setText(indexationViewModel.getItemType().getVolume().toString());

        }*/
        bindViews();
        if(getArguments()!=null)
            fromEdit=getArguments().getBoolean("fromEdit");



    }
    private void bindViews(){
        binding.etVolume.setText( indexationViewModel.getIndexedItem().getVolume().toString());
        binding.cbPalettisation.setChecked(indexationViewModel.getIndexedItem().isOn_pallet()==1);
        binding.cbTwoPersons.setChecked(indexationViewModel.getIndexedItem().isIs_large()==1);
    }


    private void btnListener(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean validationsOk=true;
                if(!indexationViewModel.setVolume(Double.parseDouble(et_input_volume.getText().toString()))){
                    Toast.makeText(getContext() , "Le volume doit être supérieur à 0", Toast.LENGTH_SHORT).show();
                    validationsOk=false;
                }

                //conversion du boolean en int pr envoyer a l api
                int isLarge=0;
                int aPaletiser=0;
                if(chb_two_persons.isChecked())
                    isLarge=1;
                if(chb_a_paletiser.isChecked())
                    aPaletiser=1;


                indexationViewModel.setIsLarge(isLarge);
                indexationViewModel.setOnPallet(aPaletiser);
                // Create new fragment and transaction
                if(validationsOk) {

                    Fragment fragment;
                    if(fromEdit==true)
                        fragment=new EditFragment();
                    else
                        fragment = new PhotoFragment();

                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();


                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack
                    transaction.replace(R.id.fragment_container, fragment);
                    transaction.addToBackStack(null);

                    // Commit the transaction
                    transaction.commit();
                }
            }
        });
    }



}
