package com.example.boxifylogistics.Indexation;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.boxifylogistics.Localisation.ItemLocalisationActivity;
import com.example.boxifylogistics.ItemsSheets.ItemsSheetsActivity;
import com.example.boxifylogistics.Palletization.PalletizationActivity;
import com.example.boxifylogistics.R;

public class StartIndexationFragment extends Fragment {

    private LinearLayout startIndexation,startItemInfos, startPalettisation,findItems;


    public static StartIndexationFragment newInstance() {
        return (new StartIndexationFragment());
    }

    /**
     * Création de la vue
     * @param savedInstanceState
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Mise en place de la vue "Liste des RDV"
        View rootView = inflater.inflate(R.layout.fragment_start_indexation,container,false);

       /* GifImageView gifImageView = rootView.findViewById(R.id.start_indexation_gif);
        gifImageView.setGifImageResource(R.drawable.start_indexation);*/

        startIndexation = rootView.findViewById(R.id.start_indexation_layout);
        startItemInfos = rootView.findViewById(R.id.start_item_infos_layout);
        startPalettisation = rootView.findViewById(R.id.start_palettisation_layout);
        findItems = rootView.findViewById(R.id.find_item_layout);

        startIndexation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(getActivity(), IndexationActivity.class);
                startActivity(it);
            }
        });
        startItemInfos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(getActivity(), ItemsSheetsActivity.class);
                startActivity(it);
            }
        });
        startPalettisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(getActivity(), PalletizationActivity.class);
                startActivity(it);
            }
        });
        findItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(getActivity(), ItemLocalisationActivity.class);
                startActivity(it);
            }
        });

        return rootView;
    }

}
