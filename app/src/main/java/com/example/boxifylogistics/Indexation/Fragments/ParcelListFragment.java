package com.example.boxifylogistics.Indexation.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.Indexation.IndexationActivity;
import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.ParcelOut.Delivery.DeliveryActivity;
import com.example.boxifylogistics.ParcelOut.DropOff.DropOffActivity;
import com.example.boxifylogistics.ParcelOut.ParcelAdapter;
import com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.model.Appointement;
import com.example.boxifylogistics.model.Parcel;
import com.example.boxifylogistics.ViewModel.ParcelViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ParcelListFragment extends Fragment implements ParcelAdapter.onParcelClickListener{

    public static ParcelListFragment newInstance() {
        return (new ParcelListFragment());
    }

    String TAG = "PickUpLists";
    private ParcelViewModel viewModel;
    private RecyclerView rv;
    private View v;
    private final ParcelAdapter pa = new ParcelAdapter();
    private ImageView calendarIcon;
    private ImageView refreshIcon;

    private CalendarView cv;

    private TextView date;
    private TextView right;
    private TextView left;
    private TextView noAppointment;

    private ProgressBar pb;

    private CardView calendarCardView;
    final Calendar c = Calendar.getInstance();
    final SimpleDateFormat affiche = new SimpleDateFormat("EEE,dd MMM yyyy");
    final SimpleDateFormat serveur = new SimpleDateFormat("yyyy/MM/dd");


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_parcel_list,container,false);


        calendarCardView = rootView.findViewById(R.id.calendar_cv);
        cv = rootView.findViewById(R.id.calendar);
        noAppointment = rootView.findViewById(R.id.no_appointments);
        pb = rootView.findViewById(R.id.pb);
        right = rootView.findViewById(R.id.right);
        left = rootView.findViewById(R.id.left);
        pb.setVisibility(View.VISIBLE);
        date =rootView.findViewById(R.id.date);
        date.setVisibility(View.VISIBLE);
        right.setVisibility(View.VISIBLE);
        left.setVisibility(View.VISIBLE);
        cv.setVisibility(View.GONE);
        calendarIcon = rootView.findViewById(R.id.calendar_icon);
        refreshIcon = rootView.findViewById(R.id.refresh_icon);


        viewModel = ViewModelProviders.of(this).get(ParcelViewModel.class);

        String actualDate = affiche.format(cv.getDate());
        date.setText(actualDate);

        Bundle bundle = getActivity().getIntent().getExtras();
        if (bundle != null) {
            ArrayList<String> dt = bundle.getStringArrayList("ymd");
            if (dt != null && dt.size() >= 3) {
                int year = Integer.parseInt(dt.get(0));
                int month = Integer.parseInt(dt.get(1));
                int day = Integer.parseInt(dt.get(2));
                c.set(year, month - 1, day);
                Date d = c.getTime();
                cv.setDate(d.getTime(), true, true);
                date.setText(affiche.format(c.getTime()));
            }
        }


        cv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                pb.setVisibility(View.VISIBLE);
                c.set(year, month, dayOfMonth);
                String selectedDate = affiche.format(c.getTime());
                date.setText(selectedDate);
                pb.setVisibility(View.VISIBLE);
                getAllpackage(serveur.format(c.getTime()));
            }
        });


        //Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        rv = rootView.findViewById(R.id.recycler_parcel);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setHasFixedSize(true);
        rv.setAdapter(pa);

        pa.setOnPickUpClickListener(this);
        pb.setVisibility(View.VISIBLE);
        getAllpackage(serveur.format(c.getTime()));
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                c.add(Calendar.DAY_OF_MONTH, 1);
                Date d1 = c.getTime();
                cv.setDate(d1.getTime(), true, true);
                date.setText(affiche.format(c.getTime()));
                noAppointment.setVisibility(View.GONE);
                pb.setVisibility(View.VISIBLE);
                getAllpackage(serveur.format(c.getTime()));
            }
        });
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                c.add(Calendar.DAY_OF_MONTH, -1);
                Date d2 = c.getTime();
                cv.setDate(d2.getTime(), true, true);
                date.setText(affiche.format(c.getTime()));
                noAppointment.setVisibility(View.GONE);
                pb.setVisibility(View.VISIBLE);
                getAllpackage(serveur.format(c.getTime()));
            }
        });

        /* Gestion du bouton calendrier */
        calendarIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cv.getVisibility() == View.VISIBLE) {
                    date.setVisibility(View.VISIBLE);
                    right.setVisibility(View.VISIBLE);
                    left.setVisibility(View.VISIBLE);
                    cv.setVisibility(View.GONE);
                } else {
                    date.setVisibility(View.GONE);
                    right.setVisibility(View.GONE);
                    left.setVisibility(View.GONE);
                    cv.setVisibility(View.VISIBLE);
                }
            }
        });

        /* Gestion du bouton refresh */
        refreshIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllpackage(serveur.format(c.getTime()));
            }
        });

        return rootView;
    }

    public void getAllpackage(String d) {
        viewModel.getAllPackages(d).observe(getActivity(), new Observer<ArrayList<Parcel>>() {
            @Override
            public void onChanged(ArrayList<Parcel> parcels) {

                if (parcels == null) {
                    pb.setVisibility(View.GONE);
                    noAppointment.setVisibility(View.VISIBLE);
                    noAppointment.setText("Un probleme est survenu avec la connexion serveur");
                    Toast.makeText(getContext(), "La connexion au serveur a échouée, Veuillez verifier votre connexion", Toast.LENGTH_SHORT).show();
                } else {
                    pb.setVisibility(View.GONE);
                    if (!parcels.isEmpty()) {
                        rv.setVisibility(View.VISIBLE);
                        noAppointment.setVisibility(View.GONE);
                        pa.setParcels(parcels);
                    } else {
                        rv.setVisibility(View.GONE);
                        noAppointment.setVisibility(View.VISIBLE);
                        noAppointment.setText("Pas de RDV");
                    }
                }


            }
        });

    }

    @Override
    public void onPackageClick(int position, final int parcel_id, Appointement.TypeAppointment type, String statusAppointement) {
        Log.e(TAG, String.valueOf(parcel_id));
        SimpleDateFormat parcel_year = new SimpleDateFormat("yyyy");
        SimpleDateFormat parcel_month = new SimpleDateFormat("MM");
        SimpleDateFormat parcel_day = new SimpleDateFormat("dd");
        final ArrayList<String> date = new ArrayList<>();
        date.add(parcel_year.format(c.getTime()));
        date.add(parcel_month.format(c.getTime()));
        date.add(parcel_day.format(c.getTime()));
        if (type == Appointement.TypeAppointment.PICK_UP) {
            if(statusAppointement.equals("COMPLETED")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(true)
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Bundle bundle = new Bundle();
                                bundle.putInt("value", parcel_id);
                                bundle.putStringArrayList("ymd", date);
                                bundle.putString("status","COMPLETED");
                                Intent intent = new Intent(getContext(), PickUpActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Ajouter des objets au Pickup ?");
                alert.show();
            }else{
                Bundle bundle = new Bundle();
                bundle.putInt("value", parcel_id);
                bundle.putStringArrayList("ymd", date);
                bundle.putString("status","CREATED");
                Intent intent = new Intent(getContext(), PickUpActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }
        if (type == Appointement.TypeAppointment.DELIVERY) {
            Bundle bundle = new Bundle();
            bundle.putInt("value", parcel_id);
            bundle.putStringArrayList("ymd", date);
            Intent intent = new Intent(getContext(), DeliveryActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        if(type == Appointement.TypeAppointment.DROP_OFF) {
            Bundle bundle = new Bundle();
            bundle.putInt("value", parcel_id);
            bundle.putStringArrayList("ymd", date);
            Intent intent = new Intent(getContext(), DropOffActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }

    }

    @Override
    public void onNavButtonClick(int position) {

    }

    @Override
    public void onProceedToLoading(int position) {

    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.calendar_menu, menu);
        return super.onCreateOptionsMenu(getContext().menu);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                if (cv.getVisibility() == View.VISIBLE) {
                    date.setVisibility(View.VISIBLE);
                    right.setVisibility(View.VISIBLE);
                    left.setVisibility(View.VISIBLE);
                    cv.setVisibility(View.GONE);
                } else {
                    date.setVisibility(View.GONE);
                    right.setVisibility(View.GONE);
                    left.setVisibility(View.GONE);
                    cv.setVisibility(View.VISIBLE);
                }
                return true;
            case R.id.home:
                Intent intent = new Intent(getContext(), MainActivity.class);
                getActivity().finish();
                startActivity(intent);
                return true;
            case R.id.refresh:
                getAllpackage(serveur.format(c.getTime()));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG,"Je suis de retour !");
        getAllpackage(serveur.format(c.getTime()));
    }
}
