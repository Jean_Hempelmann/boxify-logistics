package com.example.boxifylogistics.Indexation.Fragments;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.IndexedItemResponse;
import com.example.boxifylogistics.ViewModel.IndexViewModel;
import com.example.boxifylogistics.model.IndexedItem;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity.extractQrCode;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScanFragment extends Fragment {

    private Handler handler;
    public static final String TAG = "ScanActivity";
    private ScanDevice sm = new ScanDevice();
    private String barcodeStr;
    private final static String SCAN_ACTION = "scan.rcv.message";
    private BroadcastReceiver br;

    AtomicBoolean isScanning;

    private IndexViewModel indexationViewModel;

    private Button closeButton;
    private ImageButton numPad;
    private AlertDialog.Builder builder;

    private boolean fromEdit;
    Activity mActivity;

    /**
     * Constructor
     */
    public ScanFragment() {
        // Required empty public constructor
    }

    /**
     * Méthode appelée à la création de la vue
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        // Mise en place de la vue "Liste des RDV"
        View rootView = inflater.inflate(R.layout.fragment_scan,container,false);

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent intent = new Intent(mActivity, MainActivity.class);
                mActivity.finish();
                startActivity(intent);
            }
        });


        return rootView;
    }

    /**
     * Méthode appelée une fois que la vue est créée (après onCreateView)
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        indexationViewModel = ViewModelProviders.of(requireActivity()).get(IndexViewModel.class);

        //Initialisation de l'Alert Dialog
        builder = new AlertDialog.Builder(getActivity());

        // ImageButton pour le numpad
        numPad = getActivity().findViewById(R.id.numpad);

        isScanning = new AtomicBoolean(false);

        // Tout ce qui est lié au SDK du PDA
        initializeHandlerThread();

        if(getArguments()!=null)
            fromEdit=getArguments().getBoolean("fromEdit");

        numPad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                        getActivity());

                final EditText saySomething = new EditText(getActivity());
                saySomething.setInputType(InputType.TYPE_CLASS_NUMBER);
                sayWindows.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String mString = saySomething.getText().toString();
                                if (!mString.matches("^[0-9]+$"))
                                    Toast.makeText(getActivity(), "Only numbers", Toast.LENGTH_SHORT).show();
                                else {
                                    try {
                                        validateQrCode(saySomething.getText().toString());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                // Your checkin() method
                            }
                        });

                sayWindows.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i("TAG", "Cancel");
                            }
                        });

                sayWindows.setView(saySomething);
                sayWindows.create().show();
            }
        });
    }

    /**
     * Gestion du scan
     */
    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] barocode = intent.getByteArrayExtra("barocode");
            int barocodelen = intent.getIntExtra("length", 0);
            byte temp = intent.getByteExtra("barcodeType", (byte) 0);
            byte[] aimid = intent.getByteArrayExtra("aimid");
            barcodeStr = new String(barocode, 0, barocodelen);
            Log.e(TAG,barcodeStr);
            try {
                validateQrCode(extractQrCode(barcodeStr));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e(TAG,extractQrCode(barcodeStr));
            sm.stopScan();
        }
    };


    private void initializeHandlerThread(){
        HandlerThread handlerThread = new HandlerThread("");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    /**
     * Récupère juste le numéro du QR Code
     * @param url
     * @return
     */
    private String extractQrCode(String url){
        return url.substring(url.lastIndexOf("/") + 1);
    }

    /**
     * Gestion de la valeur du QR Code
     * @param qr
     */
    private void validateQrCode(final String qr) throws IOException {

        indexationViewModel.loadIndexedItem(qr).observe(this, new Observer<IndexedItemResponse>() {
            @Override
            public void onChanged(IndexedItemResponse indexedItemResponse) {
                if(indexedItemResponse==null) {
                    Toast.makeText(getContext() , "QR"+qr+" pas associé à un object", Toast.LENGTH_SHORT).show();
                }
                else{
                    checkIndexationStatus(indexedItemResponse.getIndexedItem() ,qr);
                    indexationViewModel.getPhotoJPG(indexedItemResponse.getIndexedItem());
                }
            }
        });
    }

    /**
     * Vérification du status de l'indexation
     * @param indexedItem
     * @param qr
     */
    private void checkIndexationStatus(IndexedItem indexedItem, String qr){
        /** Déjà indexé**/
        Log.e("TAG", "checkIndexationStatus: "+indexedItem );
        if(indexedItem.getStatus_admin().equals("IN_STORAGE") ) {
            //Appel api pour recuperer les donnees du client
            indexationViewModel.setIndexedItem(indexedItem);
            indexationViewModel.fetchItemType(indexedItem.getCalculator_item_id());
            if(indexationViewModel.isEditing()==true){
                loadFragment();
            }
            else{
                indexationViewModel.setEditing(true);
                buildAlertDialogForEdit();
            }

        }/** Pas indexable**/
        else if(!indexedItem.getStatus_admin().equals("BEING_PICKED_UP") && !indexedItem.getStatus_admin().equals("IN_STORAGE")){
            Toast.makeText(getContext() , "Indexation impossible (Status)", Toast.LENGTH_SHORT).show();
        }
        else { /** Lancement de l'indéxation**/

            indexationViewModel.setIndexedItem(indexedItem);
            indexationViewModel.setEditing(false);
            loadFragment();

        }

    }

    /**
     * Gestion des dialogue d'alerte
     */
    private void buildAlertDialogForEdit(){

        //Setting message manually and performing action on button click
        builder.setMessage("Indexation déjà éffectuée pour cet article. Voulez vous la modifier ?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        loadFragment();

                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Modifier l'indexation");
        alert.show();

    }

    /**
     * Gestion des dialogue d'alerte d'erreur
     */
    private void buildAlertDialogForError(String msg){
        //Setting message manually and performing action on button click
        builder.setMessage(msg)
                .setCancelable(false)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();

                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Erreur");
        alert.show();

    }

    /**
     * Chargement du prochain fragment
     */
    private void loadFragment(){
        Fragment fragment;
        //if(fromEdit==true || indexationViewModel.isEditing())
            fragment=new EditFragment();
        //else
          //  fragment=new ObjectTypeFragment();

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof Activity){
            mActivity = (Activity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.unregisterReceiver(br);
    }

    /**
     * Le fragment commence à interragir avec l'utilisateur
     */
    @Override
    public void onResume() {
        super.onResume();
        sm.setOutScanMode(0);
        sm.openScan();
        setupScan();
    }

    private void setupScan()
    {
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte[] barocode = intent.getByteArrayExtra("barocode");
                int barocodelen = intent.getIntExtra("length", 0);
                byte temp = intent.getByteExtra("barcodeType", (byte) 0);
                byte[] aimid = intent.getByteArrayExtra("aimid");
                barcodeStr = new String(barocode, 0, barocodelen);
                Log.e(TAG,barcodeStr);
                try {
                    validateQrCode(extractQrCode(barcodeStr));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e(TAG,extractQrCode(barcodeStr));
                sm.stopScan();
            }
        };
        mActivity.registerReceiver(br, new IntentFilter(SCAN_ACTION));
    }

}
