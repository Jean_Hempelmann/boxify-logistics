package com.example.boxifylogistics.Indexation.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.boxifylogistics.R;
import com.example.boxifylogistics.ViewModel.IndexViewModel;


public class IndexationFragment extends Fragment {

    public static IndexationFragment newInstance() {
        return (new IndexationFragment());
    }

    private IndexViewModel indexationViewModel;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.activity_indexation,container,false);





        //Création d'une nouvelle instance du IndéxationViewModel , cela doit se faire dans l'activity containeur et doit etre appellé dans les fragments.
        indexationViewModel = new ViewModelProvider(this, new ViewModelProvider.Factory() {
            @SuppressWarnings("unchecked") @Override public <T extends ViewModel> T create(final Class<T> modelClass) {
                if (modelClass.equals(IndexViewModel.class)) {
                    return (T) new IndexViewModel();
                } else {
                    return null;
                }
            }
        }).get(IndexViewModel.class);

        loadScanFragment();


        return rootView;
    }

    /*@Override
    protected void onStop() {
        super.onStop();

    }*/


    private void loadScanFragment(){


        // Create new fragment and transaction
        ScanFragment scanFragment = new ScanFragment();


        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, scanFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();


    }
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.calendar_menu, menu);
        menu.getItem(1).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.item1) {
            item.setVisible(false);
        }
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(IndexationFragment.this, MainActivity.class);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/





}


