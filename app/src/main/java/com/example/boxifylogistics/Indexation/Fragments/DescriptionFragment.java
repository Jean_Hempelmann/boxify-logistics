package com.example.boxifylogistics.Indexation.Fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Utils.Utils;
import com.example.boxifylogistics.databinding.FragmentDescriptionBinding;
import com.example.boxifylogistics.ViewModel.IndexViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class DescriptionFragment extends Fragment {


    EditText et_input_desc;
    EditText et_input_name;
    ImageButton button;
    CheckBox chb_fragile;
    IndexViewModel indexationViewModel;
    EditText et_input_volume;
    CheckBox chb_two_persons;
    CheckBox chb_a_paletiser;
    private boolean fromEdit;
    private FragmentDescriptionBinding binding;
    //  FragmentDescriptionBinding binding;


    public DescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        binding = FragmentDescriptionBinding.inflate(inflater, container, false);

        View view = binding.getRoot();

        return view;

     //   return inflater.inflate(R.layout.fragment_description, container, false);


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getArguments()!=null)
            fromEdit=getArguments().getBoolean("fromEdit");



        et_input_desc=getActivity().findViewById(R.id.et_desc);
        button=getActivity().findViewById(R.id.btn_nom_desc_valider);
        chb_fragile=getActivity().findViewById(R.id.cb_fragile);
        et_input_name=getActivity().findViewById(R.id.et_nom);

        et_input_volume =getActivity().findViewById(R.id.et_volume);
        //button=getActivity().findViewById(R.id.btn_volume_valider);
        chb_two_persons =getActivity().findViewById(R.id.cb_two_persons);
        chb_a_paletiser=getActivity().findViewById(R.id.cb_palettisation);

        btnListener();

        indexationViewModel = ViewModelProviders.of(requireActivity()).get(IndexViewModel.class);
       // binding.etDescription.setText(indexationViewModel.getIndexedItem().getDescription());


        //Pour précocher le checbox "fragile" selon la valeur par défaut de l'itemType choisi
        if(indexationViewModel.getItemType()!=null)
            chb_fragile.setChecked(indexationViewModel.getItemType().getIs_fragile()==1);

        bindViews();

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                loadFragment();
                Toast.makeText(getContext() , "Vous n'avez pas validé les informations", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void bindViews(){
        binding.etDesc.setText(indexationViewModel.getIndexedItem().getDescription());
        binding.etNom.setText(indexationViewModel.getIndexedItem().getName());
        binding.cbFragile.setChecked(indexationViewModel.getIndexedItem().isIs_fragile()==1);

        binding.etVolume.setText( indexationViewModel.getIndexedItem().getVolume().toString());
        binding.cbPalettisation.setChecked(indexationViewModel.getIndexedItem().isOn_pallet()==1);
        binding.cbTwoPersons.setChecked(indexationViewModel.getIndexedItem().isIs_large()==1);
    }


    private void btnListener(){
        button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                Log.e("TAG", "onClick: ok" );
                    String desc=et_input_desc.getText().toString();
                    String name=et_input_name.getText().toString();

                    boolean validationsOk=true;

                    if(!indexationViewModel.setDescription(desc)) {
                        Toast.makeText(getContext() , "La description doit avoir au minimum 5 caractères", Toast.LENGTH_SHORT).show();
                        validationsOk=false;
                    }

                    if(!indexationViewModel.setName(name)){
                        Toast.makeText(getContext() , "Le nom doit avoir au minimum 3 caractères", Toast.LENGTH_SHORT).show();
                        validationsOk=false;
                    }

                    //On met isChecked en int pck l api prend des int a la place des booleans
                    int isChecked=0;
                    if(chb_fragile.isChecked())
                        isChecked=1;

                    indexationViewModel.setIsFragile(isChecked);

                    if(!indexationViewModel.setVolume(Double.parseDouble(et_input_volume.getText().toString()))){
                        Toast.makeText(getContext() , "Le volume doit être supérieur à 0", Toast.LENGTH_SHORT).show();
                        validationsOk=false;
                    }

                    //conversion du boolean en int pr envoyer a l api
                    int isLarge=0;
                    int aPaletiser=0;
                    if(chb_two_persons.isChecked())
                        isLarge=1;
                    if(chb_a_paletiser.isChecked())
                        aPaletiser=1;

                    indexationViewModel.setIsLarge(isLarge);
                    indexationViewModel.setOnPallet(aPaletiser);

                    if(validationsOk) {
                        loadFragment();
                    }
                }


        });
    }

    private void loadFragment() {
        Fragment fragment = new EditFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }
}
