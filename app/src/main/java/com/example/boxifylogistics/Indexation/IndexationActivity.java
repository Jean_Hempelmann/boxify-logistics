package com.example.boxifylogistics.Indexation;

import android.content.Intent;
import android.device.ScanDevice;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.boxifylogistics.Indexation.Fragments.EditFragment;
import com.example.boxifylogistics.Indexation.Fragments.ObjectTypeFragment;
import com.example.boxifylogistics.Indexation.Fragments.PhotoFragment;
import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Indexation.Fragments.ScanFragment;
import com.example.boxifylogistics.ViewModel.IndexViewModel;


public class IndexationActivity extends AppCompatActivity {


    private IndexViewModel indexationViewModel;
    public static ScanDevice sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indexation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView logoToolBar = toolbar.findViewById(R.id.logoXmarks);
        logoToolBar.setImageResource(R.drawable.ic_title_indexation);
        setSupportActionBar(toolbar);



        //Création d'une nouvelle instance du IndéxationViewModel , cela doit se faire dans l'activity containeur et doit etre appellé dans les fragments.
        indexationViewModel = new ViewModelProvider(this, new ViewModelProvider.Factory() {
            @SuppressWarnings("unchecked") @Override public <T extends ViewModel> T create(final Class<T> modelClass) {
                if (modelClass.equals(IndexViewModel.class)) {
                    return (T) new IndexViewModel();
                } else {
                    return null;
                }
            }
        }).get(IndexViewModel.class);

        loadScanFragment();
        // Mise en place du scan
        sm = new ScanDevice();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }


    /**
     * On charge ici le fragment permattent le scanner
     */
    private void loadScanFragment(){
        // Create new fragment and transaction
        ScanFragment scanFragment = new ScanFragment();


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, scanFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    /**
     * Méthode appelée à la création du menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Méthode permettant de gérer l'appui sur les différents items du menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(IndexationActivity.this, MainActivity.class);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sm != null) {
            sm.stopScan();
            sm.setScanLaserMode(8);
            sm.closeScan();
        }
    }

}


