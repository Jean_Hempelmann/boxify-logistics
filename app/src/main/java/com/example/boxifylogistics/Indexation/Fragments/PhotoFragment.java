package com.example.boxifylogistics.Indexation.Fragments;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.boxifylogistics.Indexation.IndexationActivity;
import com.example.boxifylogistics.LoginActivity;
import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.PictureResponse;
import com.example.boxifylogistics.Response.Response;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.Utils.Utils;
import com.example.boxifylogistics.model.IndexedItem;
import com.example.boxifylogistics.ViewModel.IndexViewModel;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoFragment extends Fragment {

    private static final String TAG = "PhotoFragment";
    static final int REQUEST_IMAGE_CAPTURE = 1;

    static final int RESULT_OK = -1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 5;

    private ImageView imageShare;

    Drawable.ConstantState addState;

    Drawable.ConstantState removeState;

    Drawable.ConstantState stateOfEditBtn;

    ImageButton btnEdit;

    ImageButton btnRotate;

    //ProgressBar progressBar;

    Intent takePictureIntent;

    IndexViewModel indexationViewModel;
    IndexedItem indexedItem;

    ImageButton btnValidate;
    private String photoBase64 = "null";
    private String photoName ="";
    private File pictureFile;
    private boolean fromEdit = false;

    private Bitmap photoBitmap;

    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);


    public PhotoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_photo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        indexationViewModel = ViewModelProviders.of(requireActivity()).get(IndexViewModel.class);
        indexedItem = indexationViewModel.getIndexedItem();


        takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        initializeViews();


        // Inflate the layout for this fragment
        //progressBar = getActivity().findViewById(R.id.progressBar2);
        //progressBar.setVisibility(View.INVISIBLE);


        //imageShare.setImageResource(R.drawable.empty);


        addState = getResources().getDrawable(R.drawable.ic_camera).getConstantState();

        removeState = getResources().getDrawable(R.drawable.ic_picture_delete).getConstantState();
        setBtnListeners();

        if (getArguments() != null)
            fromEdit = getArguments().getBoolean("fromEdit");

        //Pour qu'il dirige pas vers l'appareil photo quand on vient de Edit , pour donner un aperçu de la photo en plus grand que sur le fragment Edit
        //et ainsi pour décider si'il faut la changer cette photo ou pas.
        if (!fromEdit)
            onEditPhotoClick();

        loadPhotoIfExists();

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                loadFragment();
                Toast.makeText(getContext() , "Vous n'avez pas validé la photo", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initializeViews() {
        btnValidate = getActivity().findViewById(R.id.btn_validate);
        btnRotate = getActivity().findViewById(R.id.btn_rotate);
        imageShare = getActivity().findViewById(R.id.id_share_image);
        btnEdit = getActivity().findViewById(R.id.btn_camera);
    }

    private void loadPhotoIfExists() {
        if (indexationViewModel.getIndexedItem().getPhotoString() != null) {
            Glide.with(getContext()).load(indexationViewModel.getIndexedItem().getPhotoString()).apply(RequestOptions.skipMemoryCacheOf(true)).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)).into(imageShare);
            //photoBitmap =indexationViewModel.getIndexedItem().photoBitmap();
            btnEdit.setImageResource(R.drawable.ic_camera);
        }
        else {
            imageShare.setImageResource(R.drawable.ic_no_picture);
        }
    }

    private void loadImageRotate() {
        if (photoBitmap != null) {

            photoBitmap = Utils.rotateBitmap(photoBitmap);
            Glide
                    .with(getContext())
                    .load(photoBitmap)
                    .into(imageShare);
        } else {
            Toast.makeText(getContext() , "Prenez d'abord la photo ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.topbar_validate_btn, menu);

    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.topbar_validate_btn, menu);
        return super.onCreateOptionsMenu(menu);
    } */


    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.validate_btn) {
            //progressBar.setVisibility(View.VISIBLE);
            Log.i("topbarBtn ", "ok");
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isStateOfBtnRemove() {
        stateOfEditBtn = btnEdit.getDrawable().getConstantState();
        Log.i("stateOfEditBtnIsRemove ", String.valueOf(stateOfEditBtn == removeState));
        return stateOfEditBtn == removeState;


    }

    //retourne true si l etat du btn nescessite un rafraichissement des images par defaut
    private boolean refreshDefaultImagesAndBtnState() {
        if (isStateOfBtnRemove()) {
            imageShare.setImageResource(R.drawable.ic_no_picture);
            btnEdit.setImageResource(R.drawable.ic_camera);
            return true;
        } else
            return false;
    }

    public void onEditPhotoClick() {
        //THIS TEST WORKS ONLY AT 24 API LEVEL AND ABOVE
        if (!refreshDefaultImagesAndBtnState()) {
            dispatchTakePictureIntent();
        } else {
            photoBitmap = null;
            photoBase64 = "null";
            photoName = "empty.jpg";
        }
    }


    private void askPermissionForCamera() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {


            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);

            // MY_PERMISSIONS_REQUEST_CAMERA is an
            // app-defined int constant. The callback method gets the
            // result of the request.

        } else {
            // Permission has already been granted
            //Vu que la paermission a été accordé aupravant , lancer la camera
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    //Réponses a la demande de permission
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // camera-related task you need to do.
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    showAlertDialogPermissionExplanation();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void showAlertDialogPermissionExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //Setting message manually and performing action on button click
        builder.setMessage("Pour prendre une photo , il faut accorder à BoxifyApp la permission d'utiliser la caméra. Accorder la permission?")
                .setCancelable(false)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        askPermissionForCamera();
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // showAlertDialogPermissionExplanation();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Permission appareil photo");
        alert.show();
    }

    private void dispatchTakePictureIntent() {
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permmisions pas accordés auparavant , demandés la permission d 'utiliser la caméra
                askPermissionForCamera();
            } else {
                //Si les permissions ont été accordés auparavant , déclenche la caméra
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("oActivityResult ", "photo received");

        Log.i("oActivityResult ", Integer.toString(requestCode) + " == " + Integer.toString(requestCode));
        photoName = indexationViewModel.getIndexedItem().getPhotoString();

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Log.i("oActivityResult ", "if passed");

            Bundle extras = data.getExtras();
            photoBitmap = (Bitmap) extras.get("data");
            photoName="";

            // photoBase64=bitmapToBase64(photoBitmap);
            imageShare.setImageBitmap(photoBitmap);

            btnEdit.setImageResource(R.drawable.ic_picture_delete);
        }

    }

    private byte[] bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        Log.e("TAG", "bitmapToBase64: " + bitmap);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return byteArray;

    }

    private File bitmapToFile(Bitmap bitmap) throws IOException {
        File f = new File(getActivity().getCacheDir(), "photo.jpg");
        f.createNewFile();
        //indexationViewModel.getIndexedItem().setPhoto("photo.jpg");


        //Convert bitmap to byte array
        Bitmap bmp = bitmap;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100/*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    private void setBtnListeners() {

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditPhotoClick();
            }
        });

        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (photoBitmap != null) {
                    try {
                        pictureFile = bitmapToFile(photoBitmap);
                        sendPicture(indexationViewModel.getIndexedItem().getBoxify_id(),pictureFile);
                        indexedItem.setPhoto("http://51.91.98.11/storage/files/items/19/"+indexedItem.getBoxify_id()+".jpg");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(getContext() , "Prenez une photo pour valider", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImageRotate();
            }
        });
    }

    private void sendPicture(int qr, File file) {
        RequestBody qrCode = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(qr));

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("photo", file.getName(), requestFile);

        Log.e(TAG, String.valueOf(body.body().contentType()));

        apiRequest.postItemPicture((GetAndSetToken.getBearerWithToken()),qrCode, body).enqueue(new retrofit2.Callback<PictureResponse>() {

            @Override
            public void onResponse(Call<PictureResponse> call, retrofit2.Response<PictureResponse> response) {

                Log.e(TAG, "Code : " + response.code());
                Log.e(TAG,response.body().message);
                Log.e(TAG, String.valueOf(response.body()));

                // Si on reçoit un "OK", on revient sur EditFragment
                if (response.code() == 200) {
                    Log.e(TAG,response.message());
                    Log.e(TAG, String.valueOf(response.raw()));
                    Log.e(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    Log.e(TAG, "MESSAGE : "+response.body().message);
                    Log.e(TAG, "EXT : "+response.body().extension);
                    loadFragment();
                }
                else {
                    Log.e(TAG, "Statut de l'erreur : " + response.raw());
                    Toast.makeText(getContext(), "La photo n'a pas été chargée", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<PictureResponse> call, Throwable t) {
                Log.e(TAG,""+t.getMessage());

                Toast.makeText(getContext(), "Vérifiez votre connexion internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadFragment() {
        Fragment fragment = new EditFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }


}
