package com.example.boxifylogistics.Indexation.Fragments;


import android.content.Intent;
import android.device.ScanDevice;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.boxifylogistics.Indexation.IndexationActivity;
import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.databinding.FragmentEditBinding;
import com.example.boxifylogistics.model.IndexedItem;
import com.example.boxifylogistics.ViewModel.IndexViewModel;
import com.bumptech.glide.Glide;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditFragment extends Fragment  {

    private static final String TAG = "EditFragment";
    private ScanDevice sm = new ScanDevice();
    private ImageView ivType;
    private ImageView ivPhoto;
    private TextView changerObjet;
    private TextView tvQrCode;
    private TextView tvCalculatorItem;
    private TextView tvEditTypeName;

    private TextView changerPhoto;
    private TextView changerInfos;
    private LinearLayout llDescription;
    private LinearLayout llVolume;
    private CheckBox chbFragile;
    private CheckBox chbLarge;
    private CheckBox chbPallet;

    private ImageButton validate;

    LinearLayout layoutItemType;
    LinearLayout layoutItemPicture;
    LinearLayout layoutItemInfos;

    private boolean isVisible = false;





    private ObjectTypeFragment objectTypeFragment;
    private PhotoFragment photoFragment;
    private DescriptionFragment descriptionFragment;
    private VolumeFragment volumeFragment;
    private ScanFragment scanFragment;


    private FragmentEditBinding binding;

    private IndexViewModel indexationViewModel;
    private IndexedItem indexedItem;

    private Button buttonChargerImage;

    public EditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        indexationViewModel = ViewModelProviders.of(requireActivity()).get(IndexViewModel.class);
        indexedItem = indexationViewModel.getIndexedItem();

        binding = FragmentEditBinding.inflate(inflater, container, false);

        View view = binding.getRoot();

        buttonChargerImage = view.findViewById(R.id.charger_image);

        buttonChargerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadImages();
            }
        });

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Log.e("TAG","Bouton retour appuyé");
                Intent intent = new Intent(getContext(), IndexationActivity.class);
                startActivity(intent);
                Toast.makeText(getContext() , "Indexation non prise en compte", Toast.LENGTH_SHORT).show();
                getActivity().getViewModelStore().clear();
            }
        });

    }

    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
        //getActivity().getViewModelStore().clear();
    }

    @Override
    public void onStop() {
        super.onStop();
        isVisible = false;
    }

    private void startFragment(Fragment fragment){
        if(fragment == photoFragment){
            Log.e(TAG,"On lance le Fragement photo");
        }
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    private void bindViews(){
        Log.e("TAG", "bindViews: "+indexationViewModel.getItemType() );
        binding.tvEditDesc.setText(indexationViewModel.getIndexedItem().getDescription());
        binding.tvEditNom.setText(indexationViewModel.getIndexedItem().getName());
        binding.tvEditVolume.setText(indexationViewModel.getIndexedItem().getVolume().toString()+"m³");
        binding.chbEditFragile.setChecked(indexationViewModel.getIndexedItem().isIs_fragile()==1);
        binding.chbEditDeuxPrsns.setChecked(indexationViewModel.getIndexedItem().isIs_large()==1);
        binding.chbEditPalet.setChecked(indexationViewModel.getIndexedItem().isOn_pallet()==1);
        binding.tvQrCode.setText(String.valueOf(indexationViewModel.getIndexedItem().getBoxify_id()));
    }

    private void viewInitialization(){
        changerObjet=getActivity().findViewById(R.id.tv_changer_type_item);
        changerPhoto=getActivity().findViewById(R.id.tv_changer_photo);
        changerInfos=getActivity().findViewById(R.id.tv_changer_infos);
        tvQrCode =getActivity().findViewById(R.id.tv_qr_code);
        llDescription=getActivity().findViewById(R.id.ll_description);
        llVolume=getActivity().findViewById(R.id.ll_volume);
        chbFragile=llDescription.findViewById(R.id.chb_edit_fragile);
        chbPallet=llVolume.findViewById(R.id.chb_edit_palet);
        chbLarge=llVolume.findViewById(R.id.chb_edit_deux_prsns);
        ivType=getActivity().findViewById(R.id.iv_obj_type);
        ivPhoto=getActivity().findViewById(R.id.iv_photo);
        //tvEditTypeName=getActivity().findViewById(R.id.tv_edit_type_name);
        validate = getActivity().findViewById(R.id.btn_valider_index);
        layoutItemType = getActivity().findViewById(R.id.layout_item_type);
        layoutItemPicture = getActivity().findViewById(R.id.layout_item_picture);
        layoutItemInfos = getActivity().findViewById(R.id.layout_item_infos);
        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexationViewModel.postIndexation().observe(getActivity(), new Observer<IndexedItem>() {
                    @Override
                    public void onChanged(IndexedItem indexedItem) {
                        if(indexedItem != null){
                            getActivity().getViewModelStore().clear();
                            Toast.makeText(getActivity(), "Indexation terminée", Toast.LENGTH_SHORT).show();
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack
                            transaction.replace(R.id.fragment_container, scanFragment);
                            transaction.addToBackStack(null);

                            // Commit the transaction
                            transaction.commit();
                        }
                        else{
                            Toast.makeText(getActivity(), "Indexation incorrecte", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        layoutItemType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Clic sur layout_item_type");
                startFragment(objectTypeFragment);
            }
        });

        layoutItemPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Clic sur layout_item_type");
                startFragment(photoFragment);
            }
        });

        layoutItemInfos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Clic sur layout_item_type");
                startFragment(descriptionFragment);
            }
        });

    }
    private void fragmentInitialization(){
        Bundle bundle=new Bundle();
        bundle.putBoolean("fromEdit",true);

        objectTypeFragment=new ObjectTypeFragment();
        photoFragment =new PhotoFragment();
        descriptionFragment=new DescriptionFragment();
        volumeFragment=new VolumeFragment();
        scanFragment=new ScanFragment();



        objectTypeFragment.setArguments(bundle);
        photoFragment.setArguments(bundle);
        descriptionFragment.setArguments(bundle);
        volumeFragment.setArguments(bundle);
        scanFragment.setArguments(bundle);

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewInitialization();
        fragmentInitialization();
    }

    private void loadImages(){

        if(indexationViewModel.getItemType()!=null) {
            Log.e(TAG,indexationViewModel.getItemType().getImage());
            Glide.with(getContext()).load(indexationViewModel.getItemType().getImage()).apply(RequestOptions.skipMemoryCacheOf(true)).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)).into(ivType);
        }else{
            changerObjet.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.BoxiGrey,null));
            layoutItemType.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_corner_grey));
        }

        //Glide.with(getContext()).load(indexedItem.getPhotoString()).into(ivPhoto);
        /*Log.e(TAG,""+indexationViewModel.getItemType());
        Log.e(TAG,indexedItem.getPhotoString());
        String photoName[] = indexedItem.getPhotoString().split("/");
        for(int i = 0 ; i<photoName.length ; i++){
            Log.e(TAG, i + photoName[i]+"\n");
        }*/


        Glide.with(getContext()).load(indexedItem.getPhotoString()).apply(RequestOptions.skipMemoryCacheOf(true)).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)).into(ivPhoto);
        Log.e(TAG,indexedItem.getPhotoString());
        if(indexedItem.getPhotoString().contains("empty")){
            changerPhoto.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.BoxiGrey,null));
            layoutItemPicture.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_corner_grey));
        }else{
            Glide.with(getContext()).load(indexedItem.getPhotoString()).apply(RequestOptions.skipMemoryCacheOf(true)).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)).into(ivPhoto);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        bindViews();
        loadImages();
        sm.closeScan();
        isVisible = true;
        checkIfInfosOk();
    }

    private void checkIfInfosOk() {
        if(indexationViewModel.getIndexedItem().getDescription() == null || indexationViewModel.getIndexedItem().getName() == null){
            changerInfos.setBackgroundTintList(getActivity().getResources().getColorStateList(R.color.BoxiGrey,null));
            layoutItemInfos.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_corner_grey));
        }
    }
}
