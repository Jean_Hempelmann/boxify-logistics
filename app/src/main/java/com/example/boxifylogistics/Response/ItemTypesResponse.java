package com.example.boxifylogistics.Response;

import com.example.boxifylogistics.model.ItemType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemTypesResponse {



    @SerializedName("success")
    @Expose
    private boolean success;
    /*
    @SerializedName("totalResults")
    @Expose
    private Integer totalResults;
     */
    @SerializedName("data")
    @Expose
    private List<ItemType> items = null;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<ItemType> getItems() {
        return items;
    }

    public void setItems(List<ItemType> items) {
        this.items = items;
    }


}
