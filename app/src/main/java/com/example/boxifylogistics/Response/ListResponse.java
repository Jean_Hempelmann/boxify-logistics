package com.example.boxifylogistics.Response;

import java.util.List;

public class ListResponse<T> {

    private List<T> data;
    private Boolean success;
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "ListResponse{" +
                "data=" + data +
                ", success=" + success +
                '}';
    }
}
