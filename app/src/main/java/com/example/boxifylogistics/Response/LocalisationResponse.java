package com.example.boxifylogistics.Response;

import com.example.boxifylogistics.model.User;
import com.google.gson.annotations.SerializedName;

public class LocalisationResponse {

    // Statut de la réponse
    @SerializedName("success")
    public boolean success;

    // ID de
    @SerializedName("warehouse_position_id")
    public int warehousePositionId;

    // ID de
    @SerializedName("pallet_id")
    public String palletId;

    // Type de
    @SerializedName("type")
    public String type;

    //
    @SerializedName("rack")
    public String rack;

    @SerializedName("cell")
    public String cell;

    @SerializedName("position")
    public String position;

    @SerializedName("level")
    public String level;

    @SerializedName("custom_position")
    public String customPosition;

    @SerializedName("created_at")
    public String createdAt;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("address_string")
    public String addressString;

    @SerializedName("is_pallet")
    public String isPallet;
}