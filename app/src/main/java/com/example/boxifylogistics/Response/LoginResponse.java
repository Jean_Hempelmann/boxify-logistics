package com.example.boxifylogistics.Response;

import com.example.boxifylogistics.model.User;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("success")
    public boolean statusCode;

    @SerializedName("token")
    public String authToken;

    @SerializedName("user")
    public User full_name;


}
