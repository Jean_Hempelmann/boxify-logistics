package com.example.boxifylogistics.Response;

import com.example.boxifylogistics.model.IndexedItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class IndexedItemResponse {



    @SerializedName("success")
    @Expose
    private boolean success;



    @SerializedName("data")
    @Expose
    private IndexedItem item ;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public IndexedItem getIndexedItem() {
        return item;
    }

    public void setItem(IndexedItem item) {
        this.item = item;
    }


}
