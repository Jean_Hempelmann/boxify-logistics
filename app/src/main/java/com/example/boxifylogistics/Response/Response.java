package com.example.boxifylogistics.Response;

public class Response<T> {

    private T data;
    private Boolean success;
    private String message;

    public T getData() {
        return data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
