package com.example.boxifylogistics.Response;

import com.example.boxifylogistics.model.User;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class PictureResponse {
    @SerializedName("message")
    public String message;
    @SerializedName("ext")
    public String extension;
}