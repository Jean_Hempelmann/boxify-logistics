package com.example.boxifylogistics.ItemsSheets.Fragments;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.boxifylogistics.Indexation.IndexationActivity;
import com.example.boxifylogistics.ItemsSheets.SheetAdapter;
import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.IndexedItemResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.ViewModel.IndexViewModel;
import com.example.boxifylogistics.model.IndexedItem;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScanFragmentItemsSheets extends Fragment {

    private Handler handler;
    public static final String TAG = "ScanFragmentItemsSheets";
    private ScanDevice sm = new ScanDevice();
    private String barcodeStr;
    private final static String SCAN_ACTION = "scan.rcv.message";
    private BroadcastReceiver br;
    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    private IndexedItem indexedItem = new IndexedItem();

    AtomicBoolean isScanning;

    private IndexViewModel indexationViewModel;

    private Button closeButton;
    private ImageButton numPad;
    private AlertDialog.Builder builder;

    private boolean fromEdit;
    Activity mActivity;

    private String userName = "";

    private SheetAdapter itemAdapter;

    /**
     * Constructor
     */
    public ScanFragmentItemsSheets() {
        // Required empty public constructor
    }

    /**
     * Méthode appelée à la création de la vue
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        // Mise en place de la vue "Liste des RDV"
        View rootView = inflater.inflate(R.layout.fragment_scan,container,false);

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent intent = new Intent(mActivity, MainActivity.class);
                mActivity.finish();
                startActivity(intent);
            }
        });
        return rootView;
    }

    /**
     * Méthode appelée une fois que la vue est créée (après onCreateView)
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        indexationViewModel = ViewModelProviders.of(requireActivity()).get(IndexViewModel.class);

        //Initialisation de l'Alert Dialog
        builder = new AlertDialog.Builder(getActivity());

        // ImageButton pour le numpad
        numPad = getActivity().findViewById(R.id.numpad);

        isScanning = new AtomicBoolean(false);

        // Tout ce qui est lié au SDK du PDA
        initializeHandlerThread();

        if(getArguments()!=null)
            fromEdit=getArguments().getBoolean("fromEdit");

        numPad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                        getActivity());

                final EditText saySomething = new EditText(getActivity());
                saySomething.setInputType(InputType.TYPE_CLASS_NUMBER);
                sayWindows.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String mString = saySomething.getText().toString();
                                if (!mString.matches("^[0-9]+$"))
                                    Toast.makeText(getActivity(), "Only numbers", Toast.LENGTH_SHORT).show();
                                else {
                                    try {
                                        validateQrCode(saySomething.getText().toString());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                // Your checkin() method
                            }
                        });
                sayWindows.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i("TAG", "Cancel");
                            }
                        });

                sayWindows.setView(saySomething);
                sayWindows.create().show();
            }
        });
    }

    /**
     * Gestion du scan
     */
    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] barocode = intent.getByteArrayExtra("barocode");
            int barocodelen = intent.getIntExtra("length", 0);
            byte temp = intent.getByteExtra("barcodeType", (byte) 0);
            byte[] aimid = intent.getByteArrayExtra("aimid");
            barcodeStr = new String(barocode, 0, barocodelen);
            Log.e(TAG,barcodeStr);
            try {
                validateQrCode(extractQrCode(barcodeStr));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e(TAG,extractQrCode(barcodeStr));
            sm.stopScan();
        }
    };


    private void initializeHandlerThread(){
        HandlerThread handlerThread = new HandlerThread("");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    /**
     * Récupère juste le numéro du QR Code
     * @param url
     * @return
     */
    private String extractQrCode(String url){
        return url.substring(url.lastIndexOf("/") + 1);
    }

    /**
     * Gestion de la valeur du QR Code
     * @param qr
     */
    private void validateQrCode(final String qr) throws IOException {

        if(qr.startsWith("P")){
            showPopupPallette(qr);
        }else{
            indexationViewModel.loadIndexedItem(qr).observe(this, new Observer<IndexedItemResponse>() {
                @Override
                public void onChanged(IndexedItemResponse indexedItemResponse) {
                    if(indexedItemResponse==null) {
                        Toast.makeText(getContext() , "QR"+qr+" pas associé à un object", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        userName = getUser(qr);
                        showPopup(qr);
                    }
                }
            });
        }
    }

    private void showPopupPallette(final String qr) {
        final View popupView = LayoutInflater.from(getContext()).inflate(R.layout.popup_items_pallet_sheets, null);
        final TextView popupQRCode = popupView.findViewById(R.id.items_sheets_qrcode);
        final NestedScrollView popupNestedScrollView = popupView.findViewById(R.id.scrv_item_list);
        final RecyclerView popupItemList = popupView.findViewById(R.id.item_list);

        popupItemList.setLayoutManager(new LinearLayoutManager(getActivity()));
        popupItemList.setHasFixedSize(true);
        popupItemList.setAdapter(itemAdapter);

        apiRequest.getItemOnPallet((GetAndSetToken.getBearerWithToken()),qr).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                Log.e(TAG, String.valueOf(response));
                if(response.code() == 200){
                        popupQRCode.setText("QR n°"+qr);
                        JsonArray data = response.body().getAsJsonArray("data");
                        for(JsonElement jsonElement : data){
                            Log.e(TAG, String.valueOf(jsonElement.getAsJsonObject().get("item_id")));
                        }

                        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                        ImageView popupIcClose = popupView.findViewById(R.id.items_sheets_ic_close);
                        popupIcClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popupWindow.dismiss();
                            }
                        });
                        popupWindow.showAsDropDown(popupView, 0, 0);
                }
                else{
                    Toast.makeText(getActivity(), "Impossible de récupérer la pallette", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(), "Vérifiez la connexion internet", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void showPopup(final String qr) {
        final View popupView = LayoutInflater.from(getContext()).inflate(R.layout.popup_items_sheets, null);
        final TextView popupQRCode = popupView.findViewById(R.id.items_sheets_qrcode);
        final ImageView popupPicture = popupView.findViewById(R.id.items_sheets_picture);
        final TextView popupName = popupView.findViewById(R.id.items_sheets_name);
        final TextView popupDesc = popupView.findViewById(R.id.items_sheets_description);
        final TextView popupVolume = popupView.findViewById(R.id.items_sheets_volume);
        final TextView popupUniqueID = popupView.findViewById(R.id.items_sheets_unique_id);

        final TextView popupCreationDate = popupView.findViewById(R.id.items_sheets_creation_date);
        final TextView popupUpdateDate = popupView.findViewById(R.id.items_sheets_update_date);
        final TextView popupCustomer = popupView.findViewById(R.id.items_sheets_customer);

        Log.e(TAG, "getItemByQrCode: "+qr );

        apiRequest.getItemByQrCode((GetAndSetToken.getBearerWithToken()),qr).enqueue(new Callback<IndexedItemResponse>() {
            @Override
            public void onResponse(Call<IndexedItemResponse> call, Response<IndexedItemResponse> response) {

                Log.e(TAG, String.valueOf(response.code()));
                if(response.code() == 200){
                    Log.e(TAG, String.valueOf(response.body().getIndexedItem()));
                    indexedItem = response.body().getIndexedItem();

                    if (indexedItem.getName() != null){
                        popupQRCode.setText("QR n°"+qr);
                        Glide.with(getContext()).load(indexedItem.getPhotoString()).apply(RequestOptions.skipMemoryCacheOf(true)).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)).into(popupPicture);
                        popupName.setText(""+indexedItem.getName());
                        popupDesc.setText(indexedItem.getDescription());
                        popupVolume.setText(indexedItem.getVolume()+" m3");
                        popupUniqueID.setText(""+indexedItem.getItem_id());


                        String creationDate[] =  indexedItem.getCreated_at().split(":");
                        popupCreationDate.setText(creationDate[0]+"h"+creationDate[1]);
                        String updateDate[] =  indexedItem.getUpdated_at().split(":");
                        popupUpdateDate.setText(updateDate[0]+"h"+updateDate[1]);

                        String proprietaire = "";
                        proprietaire = proprietaire+"("+indexedItem.getCustomer_id()+")";
                        //userName = getUser(qr);
                        proprietaire = proprietaire+" "+userName;
                        popupCustomer.setText(proprietaire);

                        //popupPicture.setImageResource(indexedItem.getPhoto());

                        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                        ImageView popupIcClose = (ImageView) popupView.findViewById(R.id.items_sheets_ic_close);
                        popupIcClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popupWindow.dismiss();
                            }
                        });
                        popupWindow.showAsDropDown(popupView, 0, 0);
                    } else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setMessage("Voulez-vous indexer l'objet ?")
                                .setCancelable(false)
                                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //  Action for 'NO' Button
                                        dialog.cancel();
                                    }
                                })
                                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(getActivity(), IndexationActivity.class);
                                        getActivity().finish();
                                        startActivity(intent);
                                    }
                                })
                        ;
                        //Creating dialog box
                        AlertDialog alert = builder.create();
                        //Setting the title manually
                        alert.setTitle("L'objet n'est pas encore indexé");
                        alert.show();
                    }

                }
                else{
                    Toast.makeText(getActivity(), "Impossible de récupérer le QR Code", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<IndexedItemResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Vérifiez la connexion internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getUser(String qr){
        apiRequest.getUser((GetAndSetToken.getBearerWithToken()),qr).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if(response.code() == 200){
                    JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                    userName = data.get("full_name").getAsString();
                }else {
                    Log.e(TAG, "200 : "+response.raw());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "200 : "+t.getMessage());
            }
        });

        return userName;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof Activity){
            mActivity = (Activity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.unregisterReceiver(br);
    }

    /**
     * Le fragment commence à interragir avec l'utilisateur
     */
    @Override
    public void onResume() {
        super.onResume();
        sm.setOutScanMode(0);
        sm.openScan();
        setupScan();
    }

    private void setupScan()
    {
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte[] barocode = intent.getByteArrayExtra("barocode");
                int barocodelen = intent.getIntExtra("length", 0);
                byte temp = intent.getByteExtra("barcodeType", (byte) 0);
                byte[] aimid = intent.getByteArrayExtra("aimid");
                barcodeStr = new String(barocode, 0, barocodelen);
                Log.e(TAG,barcodeStr);
                try {
                    validateQrCode(extractQrCode(barcodeStr));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e(TAG,extractQrCode(barcodeStr));
                sm.stopScan();
            }
        };
        mActivity.registerReceiver(br, new IntentFilter(SCAN_ACTION));
    }
}
