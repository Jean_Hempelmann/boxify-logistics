package com.example.boxifylogistics.ItemsSheets.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;

/**
 * Created by Jean Hempelmann on 22/07/2020.
 * Boxify
 * #StagiaireVie
 */
public class ItemsSheetsFragment extends Fragment {
    /**
     * Méthode appelée à la création de la vue
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        // Mise en place de la vue "Liste des RDV"
        View rootView = inflater.inflate(R.layout.popup_items_sheets,container,false);

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }
}
