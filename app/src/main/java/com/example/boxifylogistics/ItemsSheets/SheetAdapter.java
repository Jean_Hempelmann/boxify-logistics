package com.example.boxifylogistics.ItemsSheets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.R;
import com.example.boxifylogistics.model.Item;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SheetAdapter extends RecyclerView.Adapter<SheetAdapter.ItemViewHolder> {

    String TAG ="SheetAdapter";
    private List<Item> items = new ArrayList<>();

    View itemViewDeOuf;
    Context context;

    public SheetAdapter(Context context) {
        this.context = context;
    }

    public void setItems(List<Item> items){
        if(items != null && items.size() >= 2){
            items.sort(new Comparator<Item>() {
                @Override
                public int compare(Item o1, Item o2) {
                    return o1.getBoxify_id()-o2.getBoxify_id();
                }
            });
        }
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        itemViewDeOuf = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pellet_details, parent, false);

        return new ItemViewHolder(itemViewDeOuf);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Item item = items.get(position);
        holder.display(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView name,qrCode;

        public ItemViewHolder(@NonNull final View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_name);
            qrCode = itemView.findViewById(R.id.qrcode_item);
        }

        /**
         * Affichage d'un item
         * @param it
         */
        @SuppressLint("SetTextI18n")
        public void display(Item it){
            if(it != null){
                Log.e(TAG, "display: "+it );
                name.setText(it.getName());
                qrCode.setText("x"+it.getBoxify_id());
            }
        }
    }
}
