package com.example.boxifylogistics.Palletization;

import android.content.Intent;
import android.device.ScanDevice;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.Palletization.Fragments.ChoicePalletizationFragment;
import com.example.boxifylogistics.Palletization.Fragments.ScanPalletizationItemToPalletFragment;
import com.example.boxifylogistics.R;

/**
 * Created by Jean Hempelmann on 22/07/2020.
 * Boxify
 * #StagiaireVie
 */
public class PalletizationActivity extends AppCompatActivity {

    public static ScanDevice sm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palletization);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView logoToolBar = toolbar.findViewById(R.id.logoXmarks);
        logoToolBar.setImageResource(R.drawable.ic_title_paletization);
        setSupportActionBar(toolbar);

        loadChoicePalletizationFragment();
        // Mise en place du scan
        sm = new ScanDevice();
    }

    /**
     * On charge ici le fragment permattent le scanner
     */
    private void loadChoicePalletizationFragment(){
        // Create new fragment and transaction
        ChoicePalletizationFragment choicePalletizationFragment = new ChoicePalletizationFragment();


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container, choicePalletizationFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    /**
     * Méthode appelée à la création du menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Méthode permettant de gérer l'appui sur les différents items du menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(PalletizationActivity.this, MainActivity.class);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
