package com.example.boxifylogistics.Palletization.Fragments;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.boxifylogistics.Indexation.Fragments.EditFragment;
import com.example.boxifylogistics.Indexation.Fragments.ObjectTypeFragment;
import com.example.boxifylogistics.Indexation.IndexationActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.IndexedItemResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.ViewModel.IndexViewModel;
import com.example.boxifylogistics.model.IndexedItem;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScanPalletizationPalletToShelfFragment extends Fragment {

    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    public static final String TAG = "ScanFragmentPalletization";
    private ScanDevice sm = new ScanDevice();
    private String barcodeStr;
    private final static String SCAN_ACTION = "scan.rcv.message";
    private BroadcastReceiver br;
    private ImageButton btnScan,btnValidate;
    private Button btnValidateQR;
    private RelativeLayout rlPalletShadow,rlShelfShadow;
    private TextView tvPalletQRCode,tvShelfQRCode;
    private ArrayList<String> qrCodesToPalletize = new ArrayList<>();
    private boolean palletIsValidated;

    private IndexViewModel indexationViewModel;

    private ImageButton numPad;

    private boolean fromEdit;
    Activity mActivity;

    /**
     * Constructor
     */
    public ScanPalletizationPalletToShelfFragment() {
        // Required empty public constructor
    }

    /**
     * Méthode appelée à la création de la vue
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        // Mise en place de la vue "Liste des RDV"
        View rootView = inflater.inflate(R.layout.fragment_scan_palletization_pallet_shelf, container, false);

        numPad = rootView.findViewById(R.id.numpad);
        btnScan = rootView.findViewById(R.id.btn_scan);
        btnValidate = rootView.findViewById(R.id.btn_validate);
        btnValidateQR = rootView.findViewById(R.id.btn_validate_pallet);
        rlPalletShadow = rootView.findViewById(R.id.rl_pallet_shadow);
        rlShelfShadow = rootView.findViewById(R.id.rl_shelf_shadow);;
        tvPalletQRCode = rootView.findViewById(R.id.tv_pallet_qrCode);
        tvShelfQRCode = rootView.findViewById(R.id.tv_shelf_qrCode);

        setupListeners();

        palletIsValidated = false;

        return rootView;
    }


    /**
     * Mise en place des listeners
     */
    private void setupListeners() {
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sm.startScan();
            }
        });
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(palletIsValidated){
                    if(!tvShelfQRCode.getText().equals("")){
                        placePallet((String) tvPalletQRCode.getText(),(String) tvShelfQRCode.getText());
                    }else{
                        Toast.makeText(getActivity(), "Scanner l'étagère", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Scanner d'abord une pallette ou un objet", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnValidateQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvPalletQRCode.getText().equals("")) {
                    rlPalletShadow.setVisibility(View.VISIBLE);
                    rlShelfShadow.setVisibility(View.GONE);
                    btnValidateQR.setClickable(false);
                    btnValidateQR.setBackgroundTintList(getResources().getColorStateList(R.color.BoxiGrey,null));
                    palletIsValidated = true;
                }else{
                    Toast.makeText(getActivity(), "Scanner une pallette ou un objet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void placePallet(String qrPalette, String qrShelf) {
        apiRequest.placePallet(GetAndSetToken.getBearerWithToken(),qrPalette,qrShelf).enqueue(new Callback<JsonObject>(){
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.code() == 200){
                    Toast.makeText(getActivity(), "Pallette/Article mis sur étagère", Toast.LENGTH_LONG).show();
                    // TODO : Appeler route pour placer en étagère
                    getActivity().onBackPressed();
                }
                else{
                    Toast.makeText(getActivity(), "Impossible de mettre sur étagère", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(), "Vérifiez la connexion internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Méthode appelée une fois que la vue est créée (après onCreateView)
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        indexationViewModel = ViewModelProviders.of(requireActivity()).get(IndexViewModel.class);

        if(getArguments()!=null)
            fromEdit=getArguments().getBoolean("fromEdit");

        /*numPad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                        getActivity());

                final EditText saySomething = new EditText(getActivity());
                saySomething.setInputType(InputType.TYPE_CLASS_NUMBER);
                sayWindows.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String mString = saySomething.getText().toString();
                                if (!mString.matches("^[0-9]+$"))
                                    Toast.makeText(getActivity(), "Only numbers", Toast.LENGTH_SHORT).show();
                                else {
                                    try {
                                        validateQrCode(saySomething.getText().toString());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                // Your checkin() method
                            }
                        });

                sayWindows.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i("TAG", "Cancel");
                            }
                        });

                sayWindows.setView(saySomething);
                sayWindows.create().show();
            }
        });*/
    }

    /**
     * Gestion du scan
     */

    /**
     * Récupère juste le numéro du QR Code
     * @param url
     * @return
     */
    private String extractQrCode(String url){
        return url.substring(url.lastIndexOf("/") + 1);
    }

    /**
     * Gestion de la valeur du QR Code
     * @param qr
     */
    private void validateQrCode(String qr) {
        qr = qr.toUpperCase();
        if(!palletIsValidated) {
            if(Character.isDigit(qr.charAt(0))) {
                final String finalQr = qr;
                indexationViewModel.loadIndexedItem(qr).observe(this, new Observer<IndexedItemResponse>() {
                    @Override
                    public void onChanged(IndexedItemResponse indexedItemResponse) {
                        if (indexedItemResponse == null) {
                            Toast.makeText(getContext(), "QR" + finalQr + " pas associé à un article", Toast.LENGTH_SHORT).show();
                        } else {
                            checkIndexationStatus(indexedItemResponse.getIndexedItem(), finalQr);
                            indexationViewModel.getPhotoJPG(indexedItemResponse.getIndexedItem());
                        }
                    }
                });
            } else if(qr.startsWith("P")){
                tvPalletQRCode.setText(qr);
            } else{
                Toast.makeText(getContext(), "Le QR"+qr+" n'est ni une pallette, ni un article", Toast.LENGTH_SHORT).show();
            }
        }else{
            if(qr.startsWith("W") || qr.startsWith("A") || qr.startsWith("R")){
                tvShelfQRCode.setText(qr);
            }else{
                Toast.makeText(getContext(),"Ce QR Code ne correspond pas à un emplacement",Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Vérification du status de l'indexation
     * @param indexedItem
     * @param qr
     */
    private void checkIndexationStatus(IndexedItem indexedItem, String qr){
        Log.e("TAG", "checkIndexationStatus: "+indexedItem );
        if(indexedItem.getStatus_admin().equals("IN_STORAGE") ) {
            tvPalletQRCode.setText(qr);
        }
        else if(indexedItem.getStatus_admin().equals("BEING_PICKED_UP")){
            startIndexationActivity();
        }
        else {
            Toast.makeText(getContext() , "L'article n'est pas actuellement stocké", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Chargement de l'activité d'indexation
     */
    private void startIndexationActivity(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true)
                .setMessage("Voulez-vous l'indexer ?")
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(getContext(), IndexationActivity.class));
                    }
                })
        ;
        builder.setTitle("Article pas encore indexé");
        builder.create().show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof Activity){
            mActivity = (Activity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.unregisterReceiver(br);
    }

    /**
     * Le fragment commence à interragir avec l'utilisateur
     */
    @Override
    public void onResume() {
        super.onResume();
        sm.setOutScanMode(0);
        sm.openScan();
        setupScan();
    }

    private void setupScan()
    {
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte[] barocode = intent.getByteArrayExtra("barocode");
                int barocodelen = intent.getIntExtra("length", 0);
                byte temp = intent.getByteExtra("barcodeType", (byte) 0);
                byte[] aimid = intent.getByteArrayExtra("aimid");
                barcodeStr = new String(barocode, 0, barocodelen);
                Log.e(TAG,barcodeStr);
                validateQrCode(extractQrCode(barcodeStr));
                Log.e(TAG,extractQrCode(barcodeStr));
                sm.stopScan();
            }
        };
        mActivity.registerReceiver(br, new IntentFilter(SCAN_ACTION));
    }

}