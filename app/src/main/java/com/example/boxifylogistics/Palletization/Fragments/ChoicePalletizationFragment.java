package com.example.boxifylogistics.Palletization.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;

/**
 * Created by Jean Hempelmann on 24/07/2020.
 * Boxify
 * #StagiaireVie
 */
public class ChoicePalletizationFragment extends Fragment {

    Activity mActivity;
    private LinearLayout palettisationItemPallet, palettisationPalletShelf;

    /**
     * Constructor
     */
    public ChoicePalletizationFragment() {
        // Required empty public constructor
    }

    /**
     * Méthode appelée à la création de la vue
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        // Mise en place de la vue "Liste des RDV"
        View rootView = inflater.inflate(R.layout.fragment_choice_palletization,container,false);

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent intent = new Intent(mActivity, MainActivity.class);
                mActivity.finish();
                startActivity(intent);
            }
        });

        palettisationItemPallet = rootView.findViewById(R.id.ll_palettisation_item_pallet);
        palettisationPalletShelf = rootView.findViewById(R.id.ll_palettisation_pallet_shelf);

        listenersSetup();


        return rootView;
    }


    private void listenersSetup() {
        palettisationItemPallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadScanPalletizationItemToPalletFragment();
            }
        });
        palettisationPalletShelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadScanPalletizationPalletToShelfFragment();
            }
        });
    }

    /**
     * On charge ici le fragment permettant de palletiser les objets
     */
    private void loadScanPalletizationItemToPalletFragment(){
        // Create new fragment and transaction
        ScanPalletizationItemToPalletFragment scanPalletizationItemToPalletFragment = new ScanPalletizationItemToPalletFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container,scanPalletizationItemToPalletFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    /**
     * On charge ici le fragment permettant de placer les palettes dans les étagères
     */
    private void loadScanPalletizationPalletToShelfFragment(){
        // Create new fragment and transaction
        ScanPalletizationPalletToShelfFragment scanPalletizationPalletToShelfFragment = new ScanPalletizationPalletToShelfFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(R.id.fragment_container,scanPalletizationPalletToShelfFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof Activity){
            mActivity = (Activity) context;
        }
    }
}
