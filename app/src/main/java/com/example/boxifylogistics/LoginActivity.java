package com.example.boxifylogistics;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.boxifylogistics.Response.LoginResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.Utils.StorageUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private EditText emailInput, passwordInput;
    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    WifiManager wifiManager;
    ConnectivityManager connectivityManager;
    NetworkInfo activeNetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_screen);

        ((GetAndSetToken) this.getApplication()).setToken(this.readFromStorage());

        /*
            Gestion de la connectivité
         */
        connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = connectivityManager.getActiveNetworkInfo();
        if(activeNetwork == null){
            Toast.makeText(LoginActivity.this , "Vérifiez votre connexion internet", Toast.LENGTH_LONG).show();
        }else{
            // Essai de récupération du profil du dernier livreur
            // = test du token
            if(GetAndSetToken.getToken()!=null){
                tryToReconnectUser();
            }else{
                setContentView(R.layout.activity_login);
                init();
            }
        }
    }



    /**
     * Initialisation de toutes les vues
     */
    void init(){
        emailInput = findViewById(R.id.email_input);
        passwordInput = findViewById(R.id.password_input);

        // Gestion du clic sur le bouton "S'enregistrer"
        findViewById(R.id.login_register_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activeNetwork == null){
                    Toast.makeText(LoginActivity.this , "Vérifiez la connexion internet", Toast.LENGTH_LONG).show();
                }else{
                    userLogin();
                }
            }
        });
    }

    /**
     * Méthode permettant de gérer le login du logsticien
     */
    private void userLogin() {
        //first getting the values
        final String email = emailInput.getText().toString();
        final String password = passwordInput.getText().toString();

        // Validation des inputs
        if (TextUtils.isEmpty(email)) {
            emailInput.setError("Entrez votre email");
            emailInput.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            passwordInput.setError("Entrez votre mot de passe");
            passwordInput.requestFocus();
            return;
        }

        /*
            Si les inputs sont remplis, on lancer le POST login
         */
        apiRequest.logisticianLogin(email,password).enqueue(new Callback<LoginResponse>(){
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                // Si on reçoit un "OK"
                if(response.code() == 200){
                    Log.e(TAG,response.body().authToken);

                    writeOnInternalStorage(response.body().authToken);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                } else{
                    Toast.makeText(LoginActivity.this , "Mauvais indentifiants", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(TAG,t.getMessage());
                Toast.makeText(LoginActivity.this , "Vérifiez la connexion internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * On utilise le token pour essayer de récupérer la profil du logisticien
     */
    private void tryToReconnectUser() {
        apiRequest.logisticianRelogin((GetAndSetToken.getBearerWithToken())).enqueue(new Callback<LoginResponse>(){
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                // Si on reçoit un "OK", on passe l'activité de login et on passe à la première page
                if(response.code() == 200){
                    Log.e(TAG,"Satut de la requête : "+response.body().statusCode);
                    Log.e(TAG,"Satut de la requête : "+response.body().statusCode);
                    Log.e(TAG,"Satut de la requête : "+response.raw());
                    Log.e(TAG,"Satut de la requête : "+response.body().full_name.getFirst_name());

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("logisticianFirstname", response.body().full_name.getFirst_name());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                // sinon, on lance l'activité de login
                else{
                    Log.e(TAG,"Satut de l'erreur : "+response.raw());
                    setContentView(R.layout.activity_login);
                    init();
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                setContentView(R.layout.activity_login);
                init();
            }
        });
    }

    /**
    * Lecture du contenu du fichier, récupération du token
    */
    private String readFromStorage(){
        return StorageUtils.getTextFromStorage(getCacheDir(), this);
    }

    /**
     *  Ecriture dans le fichier, stockage du token
     */
    private void writeOnInternalStorage(String textToSave){
        ((GetAndSetToken) this.getApplication()).setToken(textToSave);
        StorageUtils.setTextInStorage(getCacheDir(), this, textToSave);
    }

    /**
     * Si on appuie sur le bouton retour, il ne se passe rien
     */
    @Override
    public void onBackPressed() {
    }
}