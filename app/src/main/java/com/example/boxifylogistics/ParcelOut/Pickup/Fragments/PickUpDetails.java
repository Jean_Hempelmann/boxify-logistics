package com.example.boxifylogistics.ParcelOut.Pickup.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.device.ScanDevice;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.example.boxifylogistics.LoginActivity;
import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.LoginResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.Utils.Utils;
import com.example.boxifylogistics.model.PickUp;
import com.example.boxifylogistics.ViewModel.PickUpViewModel;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickUpDetails extends Fragment {

    String TAG="PickUpDetails2";

    private View v;
    private PickUpViewModel viewModel;
    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    //Headers
    private TextView deliveryDate;
    private TextView deliveryHour;
    private TextView type;

    //Customer informations
    private TextView arrow;
    private TextView customerIdentity;
    private TextView addressStreet;
    private TextView addressCityZipcode;
    private TextView addressCountry;
    private TextView phoneNumber;
    private TextView hasParking;
    private TextView hasLift;
    private TextView volume;
    private RelativeLayout rl;
    private CardView deliveryInformations;
    private ScanDevice sm = new ScanDevice();

    //Navigation
    private Button beginNavigation;

    private TextView carCheck;

    private GridLayout carGrid;

    private TextView matCheck;

    private GridLayout matGrid;

    //Deliver
    private Button toFirstLastScan;
    Activity mActivity;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.pickup_details_2, container, false);
        deliveryDate = v.findViewById(R.id.parcel_date);
        deliveryHour = v.findViewById(R.id.parcel_hour);
        type = v.findViewById(R.id.type);

        PickUpActivity activity = (PickUpActivity) getActivity();
        String status = activity.getStatus();
        Log.e(TAG,status);
        if(status.equals("COMPLETED")){
            NavHostFragment.findNavController(this).navigate(R.id.pickup_all_scan);
        }

        bindCardViews();
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {

                Intent intent = new Intent(mActivity, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("ymd",viewModel.getDate());
                intent.putExtras(bundle);
                mActivity.finish();
                startActivity(intent);
            }
        });

        toFirstLastScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Navigation.findNavController(v).navigate(R.id.pickup_first_last_scan);
                sendCheckedCheckbox();

            }
        });

        return v;
    }

    public void sendCheckedCheckbox(){
        //Log.e(TAG, String.valueOf(viewModel.getCarCheck().size()));
        //viewModel.getCheckupChecked();
        for (final String d : viewModel.getCheckupChecked()) {
            Log.e(TAG, d);
        }
        JSONArray jsCheckedArray = new JSONArray(viewModel.getCheckupChecked());
        apiRequest.sendCheckedCheckbox(GetAndSetToken.getBearerWithToken(),jsCheckedArray).enqueue(new Callback<JsonObject>(){
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e(TAG, String.valueOf(response.raw()));
                // Si on reçoit un "OK"
                if(response.code() == 200){
                    Navigation.findNavController(v).navigate(R.id.pickup_first_last_scan);
                }
                else{
                    Toast.makeText(getActivity(),"Les vérifications ont échouées",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(),"Les vérifications ont échouées\nVérifier la connexion internet",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel = ViewModelProviders.of(getActivity()).get(PickUpViewModel.class);

        navigationListeners();
        populateGridLayouts();

        viewModel.getPickupDetails(null).observe(getActivity(), new Observer<PickUp>() {
            @Override
            public void onChanged(PickUp pickUp) {
                CardViewDeliveryInformation(pickUp);
            }
        });
        viewModel.canNavigate().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    beginNavigation.setEnabled(true);
                    beginNavigation.setBackgroundResource(R.color.BoxyBBRed);
                    beginNavigation.setTextColor(Color.WHITE);
                } else {
                    beginNavigation.setEnabled(false);
                    beginNavigation.setBackgroundResource(R.color.BoxiGrey);
                    beginNavigation.setTextColor(Color.BLACK);
                }
            }
        });
    }
    public void bindCardViews(){
        bindCardViewDeliveryInformation();
        bindCardViewNavigation();
        toFirstLastScan = v.findViewById(R.id.begin_delivery);

    }
    public void bindCardViewDeliveryInformation() {

        customerIdentity = v.findViewById(R.id.customer_identity);
        addressStreet = v.findViewById(R.id.address_street);
        addressCityZipcode = v.findViewById(R.id.address_city_zipcode);
        addressCountry = v.findViewById(R.id.address_country);
        phoneNumber = v.findViewById(R.id.customer_phonenbr);
        rl = v.findViewById(R.id.complementary_informations_body);
        deliveryInformations = v.findViewById(R.id.delivery_first_informations);
        hasParking = v.findViewById(R.id.has_parking);
        hasLift = v.findViewById(R.id.has_lift);
        volume = v.findViewById(R.id.volume);
        arrow = v.findViewById(R.id.expander);

        v.findViewById(R.id.complementary_informations).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl.getVisibility() == View.GONE) {
                    rl.setVisibility(View.VISIBLE);
                    arrow.setBackgroundResource(R.drawable.ic_expand_less_black_24dp);
                } else {
                    rl.setVisibility(View.GONE);
                    arrow.setBackgroundResource(R.drawable.ic_expand_more_black_24dp);
                }
            }
        });
    }

    public void CardViewDeliveryInformation(PickUp d) {
        String identity = d.getUser().getFull_name() + " (" + d.getUser().getLanguage() + ")";
        String street = d.getAddress().getNumber() + " " + Utils.capitalizeWord(d.getAddress().getStreet());
        String cipCode = d.getAddress().getZipcode() + " " + d.getAddress().getCity();
        String phonnbr = d.getUser().getPhone() == null ? d.getUser().getPhone() : "NC";

        customerIdentity.setText(identity);
        addressStreet.setText(street);
        addressCityZipcode.setText(cipCode);
        addressCountry.setText("Belgique");
        phoneNumber.setText(phonnbr);
        hasParking.setText(d.getAppointment().isHas_parking() ? "Oui" : "Non");
        hasLift.setText(d.getAppointment().isHas_lift() ? "Oui" : "Non");
        volume.setText(d.getAppointment().getVolume() + "m³");

        deliveryHour.setText(d.getAppointment().getHourAppoint());
        deliveryDate.setText(d.getAppointment().getDayAppoint());
        type.setText("Pick Up");
    }

    public void bindCardViewNavigation() {
        beginNavigation = v.findViewById(R.id.btn_begin_nav);

        carGrid = v.findViewById(R.id.car_grid);
        matGrid = v.findViewById(R.id.mat_grid);

        matCheck = v.findViewById(R.id.mat_check);
        carCheck = v.findViewById(R.id.car_check);


    }
    public void navigationListeners() {
        beginNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.setGpsTracker(getActivity());
                /* MODIFICATION DU WEEKEND */
                Log.e(TAG,viewModel.getUrlForNavigation());
                if (!viewModel.getIsAllChecked()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setMessage("Vous n'avez pas tout coché, êtes vous sur de vouloir continuer ?")
                            .setCancelable(false)
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    carGrid.setVisibility(View.GONE);
                                    matGrid.setVisibility(View.GONE);
                                    String url = viewModel.getUrlForNavigation();
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                    startActivity(intent);
                                }
                            })
                    ;
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("Attention");
                    alert.show();
                } else {
                String url = viewModel.getUrlForNavigation();

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                }
            }
        });
    }



    public void populateGridLayouts() {
        String carCheckArray[];
        carGrid.setColumnCount(1);
        for (final String d : viewModel.getCarCheck()) {
            final CheckBox a = new CheckBox(getActivity());
            a.setText(d);
            if (d == "Permis de conduire" || d == "Validité assurance") {
                a.setTextColor(Color.parseColor("#cc003d"));
            }
            a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        if (a.getText() == "Permis de conduire" || a.getText() == "Validité assurance" || a.getText() == "Rouleau de QrCodes") {
                            a.setTextColor(Color.parseColor("#00cc33"));
                        }
                        viewModel.addCheck(buttonView.getText().toString());
                    }

                    else{
                        if (a.getText() == "Permis de conduire" || a.getText() == "Validité assurance" || a.getText() == "Rouleau de QrCodes") {
                            a.setTextColor(Color.parseColor("#cc003d"));
                        }
                        viewModel.removeCheck(buttonView.getText().toString());
                    }

                }
            });
            carGrid.addView(a);
        }
        matGrid.setColumnCount(1);
        for (String d : viewModel.getMaterialCheck()) {
            final CheckBox a = new CheckBox(getActivity());
            a.setText(d);
            if (d == "Rouleau de QrCodes") {
                a.setTextColor(Color.parseColor("#cc003d"));
            }
            a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        if (a.getText() == "Rouleau de QrCodes") {
                            a.setTextColor(Color.parseColor("#00cc33"));
                        }
                        viewModel.addCheck(buttonView.getText().toString());
                    }
                    else{
                        if (a.getText() == "Rouleau de QrCodes") {
                            a.setTextColor(Color.parseColor("#cc003d"));
                        }
                        viewModel.removeCheck(buttonView.getText().toString());
                    }

                }
            });
            matGrid.addView(a);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof Activity)
            mActivity = (Activity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        sm.closeScan();
    }
}
