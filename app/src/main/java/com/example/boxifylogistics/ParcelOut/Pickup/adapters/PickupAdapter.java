package com.example.boxifylogistics.ParcelOut.Pickup.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.R;
import com.example.boxifylogistics.model.Appointement;
import com.example.boxifylogistics.model.PickUp;

import java.util.ArrayList;
import java.util.List;

public class PickupAdapter extends RecyclerView.Adapter<PickupAdapter.PickupViewHolder> {
    private static final String TAG = "jaja";
    private List<PickUp> pickUps = new ArrayList<>();
    private int arrivedPickup  = -1 ;

    private OnPickUpClickListener listener;

    public void setPickUp(List<PickUp> pickUp) {
        this.pickUps = pickUp;
        notifyDataSetChanged();
    }
    public void setArrivedPickupId(int id){
        arrivedPickup = id;
    }

    public interface OnPickUpClickListener {
        void onPickUpClick(int position);
        void onNavButtonClick(int position);
        void onProceedToLoading(int position);

    }

    @NonNull
    @Override
    public PickupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pickup_details, parent, false);

        return new PickupViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PickupViewHolder holder, int position) {
        PickUp pickUp = pickUps.get(position);
        holder.display(pickUp);
//        if(arrivedPickup == position)
//            holder.setArrivedPickup();
//        else
//            holder.setNotArrivedPickup();
    }

    @Override
    public int getItemCount() {
        return pickUps.size();
    }

    public class PickupViewHolder extends RecyclerView.ViewHolder {
        CardView pickupView;
        TextView pickupHour;
        TextView pickupType;
        TextView pickupCustomerFullName;
        TextView pickupAddress;
        TextView pickupCusCiv;
        Button button;

        View.OnClickListener buttonListener;

        public PickupViewHolder(@NonNull View itemView) {
            super(itemView);
            pickupHour = itemView.findViewById(R.id.pickup_hour);
            pickupType = itemView.findViewById(R.id.pickup_type);
            pickupAddress = itemView.findViewById(R.id.pickup_address);
            pickupCustomerFullName = itemView.findViewById(R.id.pickup_customer_fullname);
            pickupCusCiv = itemView.findViewById(R.id.pickup_customer_civ);
            button = itemView.findViewById(R.id.begin_navigation);
            pickupView = itemView.findViewById(R.id.cv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPickUpClick(getAdapterPosition());
                }
            });
            button.setOnClickListener(buttonListener);
        }

        public void display(PickUp pu) {
            if(pu.getAppointment().getType() == Appointement.TypeAppointment.PICK_UP){
                button.setText("Commencer navigation");
                buttonListener  = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onNavButtonClick(getAdapterPosition());
                    }
                };
            }
            if(pu.getAppointment().getType() == Appointement.TypeAppointment.DELIVERY){
                button.setText("Proceder Au chargement");
                buttonListener  = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onProceedToLoading(getAdapterPosition());
                    }
                };
            }

            pickupType.setText(pu.getAppointment().getTypeString());
            pickupHour.setText(pu.getAppointment().getHourAppoint());

            pickupCustomerFullName.setText(pu.getUser().getFull_name());
            pickupCusCiv.setText(pu.getUser().getCivility());
            pickupAddress.setText(pu.getAddress().getCompletedAddress());


        }
//        public void setArrivedPickup(){
//            pickupView.setCardBackgroundColor(Color.GREEN);
//            beginNav.setBackgroundColor(Color.GREEN);
//            beginNav.setEnabled(false);
//        }
//        public void setNotArrivedPickup(){
//            pickupView.setCardBackgroundColor(Color.GRAY);
//            beginNav.setBackgroundColor(Color.GRAY);
//            beginNav.setEnabled(true);
//        }
    }


    public void setOnPickUpClickListener(OnPickUpClickListener listener) {
        this.listener = listener;
    }
}
