package com.example.boxifylogistics.ParcelOut.Delivery.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.boxifylogistics.Localisation.ItemLocalisationActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.IndexedItemResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.model.IndexedItem;
import com.example.boxifylogistics.model.Item;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    String TAG ="Item Adapter";
    private List<Item> items = new ArrayList<>();
    private boolean isAll = false ;
    private List<Item> red = new ArrayList<>();
    private List<Item> blue = new ArrayList<>();
    private List<Item> green = new ArrayList<>();
    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);;

    private IndexedItem indexedItem = new IndexedItem();
    View itemViewDeOuf;
    Context context;

    public ItemAdapter(Context context) {
        this.context = context;
    }

    public void setItems(List<Item> items){
        if(items != null && items.size() >= 2){
            items.sort(new Comparator<Item>() {
                @Override
                public int compare(Item o1, Item o2) {
                    return o1.getBoxify_id()-o2.getBoxify_id();
                }
            });
        }
        this.items = items;
        notifyDataSetChanged();
    }

    public void setRed(List<Item> red) {
        this.red = red;
        notifyDataSetChanged();
    }

    public void setBlue(List<Item> blue) {
        this.blue = blue;
        notifyDataSetChanged();
    }

    public void setGreen(List<Item> green) {
        this.green = green;
        notifyDataSetChanged();
    }


    public void isAll(){
        isAll = true;
    }
    public void isNotAll(){
        isAll = false;
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        itemViewDeOuf = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_details, parent, false);

        return new ItemViewHolder(itemViewDeOuf);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Item item = items.get(position);
        holder.display(item);
        if(red.contains(item))
            holder.setRed();
        if(blue.contains(item))
            holder.setBlue();
        if(green.contains(item))
            holder.setGreen();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView qrCode;
        TextView name;
        TextView volume;
        TextView Lfe;
        CardView cardView;

        public ItemViewHolder(@NonNull final View itemView) {
            super(itemView);

            qrCode = itemView.findViewById(R.id.qrcode_item);
            name = itemView.findViewById(R.id.itemname);
            volume = itemView.findViewById(R.id.volume_item);
            cardView = itemView.findViewById(R.id.cv);
            Lfe = itemView.findViewById(R.id.lfe);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopup((String) qrCode.getText());
                }
            });
        }

        public void showPopup(String qr) {
            final View popupView = LayoutInflater.from(context).inflate(R.layout.item_popup_window, null);
            final ImageView popupPicture = popupView.findViewById(R.id.popup_picture);
            final TextView popupQRCode = popupView.findViewById(R.id.popup_QRCode);
            final TextView popupName = popupView.findViewById(R.id.popup_name);
            final TextView popupDesc = popupView.findViewById(R.id.popup_desc);
            final TextView popupVolume = popupView.findViewById(R.id.popup_volume);
            final LinearLayout popupLFE = popupView.findViewById(R.id.popup_layout_lfe);
            final TextView popupTextViewLFE = popupView.findViewById(R.id.popup_tv_lfe);
            final TextView popupPosition = popupView.findViewById(R.id.popup_position);

            Log.e(TAG, "getItemByQrCode: "+qr );

            apiRequest.getItemByQrCode((GetAndSetToken.getBearerWithToken()),qr).enqueue(new Callback<IndexedItemResponse>() {
                @Override
                public void onResponse(Call<IndexedItemResponse> call, Response<IndexedItemResponse> response) {

                    if(response.code() == 200){
                        Log.e(TAG, String.valueOf(response.body().getIndexedItem().getBoxify_id()));
                        String lfe = "";
                        indexedItem = response.body().getIndexedItem();
                        popupQRCode.setText("QR n°"+indexedItem.getBoxify_id());
                        popupName.setText(""+indexedItem.getName());
                        popupDesc.setText("Description : "+indexedItem.getDescription());
                        popupVolume.setText("Volume : "+indexedItem.getVolume()+"m3");
                        Log.e("Base 64 : ",""+indexedItem.getPhotoBase64());
                        Log.e("String: ",""+indexedItem.getPhotoString());


                        Glide.with(context).load(indexedItem.getPhotoString()).apply(RequestOptions.skipMemoryCacheOf(true)).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)).into(popupPicture);

                        //popupPicture.setImageBitmap(indexedItem.photoBitmap());

                        if(indexedItem.isIs_fragile()==1 && indexedItem.isIs_large()==1){
                            popupLFE.setVisibility(View.VISIBLE);
                            lfe = lfe+"Encombrant et fragile";
                        }else if(indexedItem.isIs_fragile()==1){
                            lfe = lfe+"Fragile";
                            popupLFE.setVisibility(View.VISIBLE);
                        }else if(indexedItem.isIs_large()==1){
                            lfe = lfe+"Encombrant";
                            popupLFE.setVisibility(View.VISIBLE);
                        }
                        popupTextViewLFE.setText(lfe);

                        //popupPicture.setImageResource(indexedItem.getPhoto());

                        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

                        ImageView popupIcClose = (ImageView) popupView.findViewById(R.id.popup_ic_close);
                        popupIcClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                popupWindow.dismiss();
                            }
                        });
                        popupWindow.showAsDropDown(popupView, 0, 0);

                    }
                    else{
                        Log.e(TAG,"Impossible de récupérer le QR Code");
                    }
                }

                @Override
                public void onFailure(Call<IndexedItemResponse> call, Throwable t) {
                    Log.e(TAG,"Impossible de récupérer le QR Code");
                }
            });
            apiRequest.getItemLocalisation(GetAndSetToken.getBearerWithToken(),qr).enqueue(new Callback<JsonObject>(){
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {


                    Log.e(TAG, String.valueOf(response.body()));

                    // Si on reçoit un "OK"
                    if(response.code() == 200){
                        //position = ""+response.body().addressString;
                        JsonObject data = response.body().getAsJsonObject("data").deepCopy();
                        if(data.get("address_string").getAsString() == null){
                            popupPosition.setText("");
                        }else{
                            popupPosition.setText(data.get("address_string").getAsString());
                        }
                    } else if(response.code() == 404){
                        popupPosition.setText("");
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e(TAG,t.getMessage());
                    popupPosition.setText("");
                }
            });
        }

        /**
         * Affichage d'un item
         * @param it
         */
        public void display(Item it){
            if(it != null){
                Log.e(TAG, "display: "+it );
                qrCode.setText(Integer.toString(it.getBoxify_id()));
                name.setText(it.getName());
                volume.setText(it.getVolume()+"m³");
                String f = it.is_fragile()? "F":"";
                String e = it.is_large()? "E":"";
                Lfe.setText(f+e);
                Lfe.setTextColor(Color.WHITE);
            }
        }

        /**
         * Permet de modifier le background color sur un objet
         * Ici : rouge -> Pas encore scanné
         */
        public void setRed(){
            cardView.setBackgroundResource(R.color.BoxyRed);
            qrCode.setTextColor(Color.WHITE);
            name.setTextColor(Color.WHITE);
            volume.setTextColor(Color.WHITE);
        }
        /**
         * Permet de modifier le background color sur un objet
         * Ici : bleu -> Scanné
         */
        public void setBlue(){
            cardView.setBackgroundResource(R.color.blueclair);
            qrCode.setTextColor(Color.parseColor("#2c2e35"));
            name.setTextColor(Color.parseColor("#2c2e35"));
            volume.setTextColor(Color.parseColor("#2c2e35"));
        }
        /**
         * Permet de modifier le background color sur un objet
         * Ici : vert ->
         */
        public void setGreen(){
            cardView.setBackgroundResource(R.color.green);
            qrCode.setTextColor(Color.WHITE);
            name.setTextColor(Color.WHITE);
            volume.setTextColor(Color.WHITE);
        }
    }

    /*public IndexedItem getItemInfosByQrCode(String qr)  {
        Log.e(TAG, "getItemByQrCode: "+qr );
        apiRequest.getItemByQrCode((GetAndSetToken.getBearerWithToken()),qr).enqueue(new Callback<IndexedItemResponse>() {
            @Override
            public void onResponse(Call<IndexedItemResponse> call, Response<IndexedItemResponse> response) {
                if(response.code() == 200){
                    Log.e(TAG, String.valueOf(response.body().getIndexedItem().getBoxify_id()));
                    indexedItem = response.body().getIndexedItem();
                    Log.e(TAG,indexedItem.getName() +""+indexedItem.getDescription());
                }
                else{
                    Log.e(TAG,"Impossible de récupérer le QR Code");
                }
            }

            @Override
            public void onFailure(Call<IndexedItemResponse> call, Throwable t) {
                Log.e(TAG,"Impossible de récupérer le QR Code");
            }
        });
        return indexedItem;
    }*/

}
