package com.example.boxifylogistics.ParcelOut.Pickup.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.R;

import java.util.ArrayList;
import java.util.Comparator;

public class QrCodeAdapter extends RecyclerView.Adapter<QrCodeAdapter.QrCodeViewHolder> {
    private ArrayList<String> qrCodes = new ArrayList<>();
    private ArrayList<String> redQrCodes = new ArrayList<>();
    private ArrayList<String> greenQrCodes = new ArrayList<>();
    private onQrCodeClick listener ;


    public void setqrCodes(ArrayList<String> QrCodes){
        if(!qrCodes.isEmpty()){
            qrCodes.sort(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.compareTo(o2);
                }
            });
        }
        qrCodes = QrCodes;
        notifyDataSetChanged();
    }
    public void setRedQrCodes(ArrayList<String> redQrCodes){
        if(redQrCodes != null)
            this.redQrCodes = redQrCodes;
        else
            this.redQrCodes = new ArrayList<>();
    }
    public void setGreenQrCodes(ArrayList<String> greenQrCodes){
        if(greenQrCodes != null)
            this.greenQrCodes = greenQrCodes;
        else
            this.greenQrCodes = new ArrayList<>();
    }

    public interface onQrCodeClick {
        void onQrCodeClick(int position);
    }

    public void setOnQrCodeClickListener(onQrCodeClick listener) {
        this.listener = listener;
    }
    @NonNull
    @Override
    public QrCodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.qrcode,parent,false);

        return new QrCodeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull QrCodeViewHolder holder, int position) {
        String qrcode = qrCodes.get(position);
        holder.display(qrcode);
        if(redQrCodes.contains(qrcode)){
            holder.paintItRed();
        }
        if(greenQrCodes.contains(qrcode)){
            holder.paintItGreen();
        }
    }

    @Override
    public int getItemCount() {
        if(qrCodes.isEmpty())
            return 0;

        return qrCodes.size();
    }


    public class QrCodeViewHolder extends RecyclerView.ViewHolder{
        private TextView qrcode;
        public QrCodeViewHolder(@NonNull View itemView) {
            super(itemView);
            qrcode = itemView.findViewById(R.id.qrcode);

        }
        protected void display(String a){
            qrcode.setText(a);
        }
        protected  void paintItRed(){
            qrcode.setTextColor(Color.RED);
        }
        protected  void paintItGreen(){
            qrcode.setTextColor(Color.GREEN);
        }
    }
}
