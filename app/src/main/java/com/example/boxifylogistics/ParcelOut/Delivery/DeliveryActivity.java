package com.example.boxifylogistics.ParcelOut.Delivery;

import android.content.Intent;
import android.device.ScanDevice;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Repository.DeliveryRepository;
import com.example.boxifylogistics.ViewModel.DeliveryViewModel;
//import com.senter.support.openapi.StKeyManager;

public class DeliveryActivity extends AppCompatActivity {

    String TAG = "DeliveryActivity";
    DeliveryViewModel viewModel;
    DeliveryRepository rpstry;
    public static ScanDevice sm;

    //private StKeyManager.ShortcutKeyMonitor scanHandleKeyMonitor;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set de la view delivery_activity (qui contient juste delivery_navigation)
        setContentView(R.layout.delivery_activity);
       // scanHandleKeyMonitor = StKeyManager.ShortcutKeyMonitor.isShortcutKeyAvailable(StKeyManager.ShortcutKey.Scan_Handle) ?
       //         StKeyManager.getInstanceOfShortcutKeyMonitor(StKeyManager.ShortcutKey.Scan_Handle) : null;
        // Récupération et mise en place de la toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView logoToolBar = toolbar.findViewById(R.id.logoXmarks);
        logoToolBar.setImageResource(R.drawable.ic_title_delivery);
        setSupportActionBar(toolbar);

        rpstry = new DeliveryRepository();
        viewModel = ViewModelProviders.of(this).get(DeliveryViewModel.class);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int value = bundle.getInt("value");
            viewModel.setDeliveryId(value);
            viewModel.setDate(bundle.getStringArrayList("ymd"));
        }
    }

    /**
     * Désactivation du scanner

    public void desactiveScan() {
        if (scanHandleKeyMonitor != null && scanHandleKeyMonitor.isMonitoring()) {
            scanHandleKeyMonitor.stopMonitor();
            scanHandleKeyMonitor = null;
        }
    }  */

    /**
     * Mise en place du menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Gestion de la sélection de l'item du menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(DeliveryActivity.this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("ymd",viewModel.getDate());
                intent.putExtras(bundle);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Une fois que l'activité est détruite
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sm != null) {
            sm.stopScan();
            sm.setScanLaserMode(8);
            sm.closeScan();
        }
    }
}
