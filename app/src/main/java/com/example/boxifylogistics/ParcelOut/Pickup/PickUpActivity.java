package com.example.boxifylogistics.ParcelOut.Pickup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.ParcelOut.Pickup.Fragments.PickUpDetails;
import com.example.boxifylogistics.ParcelOut.Pickup.Fragments.PickUpFirstLastScan;
import com.example.boxifylogistics.ParcelOut.Pickup.Fragments.PickupSignature;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.ViewModel.PickUpViewModel;
import com.example.boxifylogistics.model.PickUp;

public class PickUpActivity extends AppCompatActivity {

    private static final String TAG = "PickUpActivity";
    //private static BroadcastReceiver mScanReceiver;
    private PickUpViewModel viewModel;
    public static ScanDevice sm;
    private String status="";

    /**
     * Méthode appelée à la création de l'activité
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pickup_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView logoToolBar = toolbar.findViewById(R.id.logoXmarks);
        logoToolBar.setImageResource(R.drawable.ic_title_pickup);
        setSupportActionBar(toolbar);

        viewModel = ViewModelProviders.of(this).get(PickUpViewModel.class);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int value = bundle.getInt("value");
            status = bundle.getString("status");
            viewModel.setPickupId(value);
            viewModel.setDate(bundle.getStringArrayList("ymd"));
        }


    }

    public String getStatus() {
        return status;
    }

    /**
     * Méthode appelée dès que les options du menu sont crées
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Méthode de gestion de l'appui sur le bouton Home
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(PickUpActivity.this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("ymd",viewModel.getDate());
                intent.putExtras(bundle);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Récupère juste le numéro du QR Code
     * @param url
     * @return
     */
    public static String extractQrCode(String url){
        return url.substring(url.lastIndexOf("/") + 1);
    }

    /**
     * Une fois que l'activité est détruite
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (sm != null) {
            sm.stopScan();
            sm.setScanLaserMode(8);
            sm.closeScan();
        }
    }

}
