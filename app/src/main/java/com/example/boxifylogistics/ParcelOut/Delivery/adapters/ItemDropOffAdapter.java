package com.example.boxifylogistics.ParcelOut.Delivery.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.IndexedItemResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.model.IndexedItem;
import com.example.boxifylogistics.model.Item;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDropOffAdapter extends RecyclerView.Adapter<ItemDropOffAdapter.ItemViewHolder> {

    String TAG ="ItemDropOffAdapter";
    private List<Item> items = new ArrayList<>();

    View itemViewDeOuf;
    Context context;

    public ItemDropOffAdapter(Context context) {
        this.context = context;
    }

    public void setItems(List<Item> items){
        if(items != null && items.size() >= 2){
            items.sort(new Comparator<Item>() {
                @Override
                public int compare(Item o1, Item o2) {
                    return o1.getBoxify_id()-o2.getBoxify_id();
                }
            });
        }
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        itemViewDeOuf = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dropoff_details, parent, false);

        return new ItemViewHolder(itemViewDeOuf);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Item item = items.get(position);
        holder.display(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView name,quantity;

        public ItemViewHolder(@NonNull final View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.itemnamedropoff);
            quantity = itemView.findViewById(R.id.item_quantity_dropoff);
        }

        /**
         * Affichage d'un item
         * @param it
         */
        @SuppressLint("SetTextI18n")
        public void display(Item it){
            if(it != null){
                Log.e(TAG, "display: "+it );
                name.setText(it.getName());
                quantity.setText("x"+it.getQuantity());
                if(Integer.parseInt(it.getQuantity())>1){
                    quantity.setTypeface(Typeface.DEFAULT_BOLD);
                }else{
                    quantity.setTypeface(null,Typeface.NORMAL);
                }
            }
        }
    }
}
