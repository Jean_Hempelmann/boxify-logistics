package com.example.boxifylogistics.ParcelOut.Pickup.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity;
import com.example.boxifylogistics.ParcelOut.Pickup.adapters.QrCodeAdapter;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.ViewModel.PickUpViewModel;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;

import static com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity.extractQrCode;

public class PickupQrScans extends Fragment {

    String TAG = "PickUpAllScan";
    String NO_QR_SCANNED = "Aucun Qr n'a été scanné";
    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    private ScanDevice sm = new ScanDevice();
    private String barcodeStr;
    final static String SCAN_ACTION = "scan.rcv.message";
    private BroadcastReceiver br;

    private PickUpViewModel viewModel;
    private View v;
    //Scanner
    private Button scannerManually;

    private AtomicBoolean isScanning;
    private Handler handler;

    private TextView scQrCodeLabel;
    private TextView numPad;

    //Items Lists
    private NestedScrollView nestedScrollView;
    private RecyclerView qrListView;
    private QrCodeAdapter qrAdapter = new QrCodeAdapter();
    private TabLayout tabLayout;
    private TextView collexp;
    private TextView nbLoaded;

    //Deliver
    private Button toSignature;

    Activity mActivity;
    Thread tr;

    private String tabShowed;

    /**
     * Méthode appelée à la création de la vue
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.pickup_all_scans, container, false);

        cardViewBinding();
        listener();

        initHandlerThread();
        isScanning = new AtomicBoolean(false);
        PickUpActivity activity = (PickUpActivity) getActivity();
        String status = activity.getStatus();
        if(status.equals("COMPLETED")){
            requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
                @Override
                public void handleOnBackPressed() {
                    Intent intent = new Intent(mActivity, MainActivity.class);
                    mActivity.finish();
                    startActivity(intent);
                }
            });
        }
        return v;
    }

    public void initHandlerThread() {
        HandlerThread handlerThread = new HandlerThread("pakpak");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    /**
     * Mise en place des vues de la page
     */
    public void cardViewBinding() {
        scannerBinding();
        qrCodeListBinding();
        toSignature = v.findViewById(R.id.sign_btn);
    }

    /**
     * Mise en place des vues concernant le sca,
     */
    public void scannerBinding() {
        nbLoaded = v.findViewById(R.id.nb_loaded);
        scQrCodeLabel = v.findViewById(R.id.scitemname_label);
        scannerManually = v.findViewById(R.id.btn_scan);
        numPad = v.findViewById(R.id.numpad);
    }

    /**
     * Mise en place des vues concernant la liste de QR Code
     */
    public void qrCodeListBinding() {
        tabLayout = v.findViewById(R.id.tabLayout);
        collexp = v.findViewById(R.id.expcoll);
        qrListView = v.findViewById(R.id.qr_list);
        nestedScrollView = v.findViewById(R.id.scrv_item_list);
    }

    /**
     * Mise en place des listeners sur l'Activity
     * Ici : Bouton de lancement de la signature
     */
    public void listener() {
        scannerListener();
        qrCodeListListener();
        toSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if(viewModel.getNbQrNotScanned().getValue() != 0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setMessage("Il vous manque " + viewModel.getNbQrNotScanned().getValue() + " objets a scanner\nÊtes vous sur de vouloir continuer?")
                            .setCancelable(false)
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();

                                }
                            })
                            .setPositiveButton("oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (tr != null && tr.isAlive()) {
                                        tr.interrupt();
                                    }
                                    sm.stopScan();
                                    Navigation.findNavController(v).navigate(R.id.pickup_signature);
                                    dialog.cancel();
                                }
                            })
                    ;
                    //Creating dialog box

                    AlertDialog alert = builder.create();
                    //Setting the title manually

                    alert.setTitle("Attention");
                    alert.show();
                }
                else{
                    if(tr != null && tr.isAlive())
                        tr.interrupt();
                    sm.stopScan();
                    Navigation.findNavController(v).navigate(R.id.pickup_signature);
                }
            }
        });
    }

    /**
     * Gestion des listener des boutons :
     * Scanner manuellement + NumPad
     */
    public void scannerListener(){
        scannerManually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sm.startScan();
            }
        });
        numPad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                        mActivity);

                final EditText saySomething = new EditText(mActivity);
                saySomething.setInputType(InputType.TYPE_CLASS_NUMBER);
                sayWindows.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "OK");
                                String mString = saySomething.getText().toString();
                                if(!mString.matches("^[0-9]+$"))
                                    Toast.makeText(mActivity, "Only numbers", Toast.LENGTH_SHORT).show();
                                else
                                    qrScanned(saySomething.getText().toString());
                                // Your checkin() method
                            }
                        });

                sayWindows.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "Cancel");
                            }
                        });

                sayWindows.setView(saySomething);
                sayWindows.create().show();
            }
        });
    }

    /**
     * Gestion des listener des boutons :
     * Etendre/Reduire + Onglets du TabLayout
     */
    public void qrCodeListListener() {
        collexp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final float scale = getResources().getDisplayMetrics().density;
                if (collexp.getText() == "Étendre") {

                    nestedScrollView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    collexp.setText("Réduire");

                } else {

                    nestedScrollView.getLayoutParams().height = (int) (250 * scale);
                    collexp.setText("Étendre");
                }

            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        tabShowed = "Tous";
                        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(viewModel.getQrNotScanned().getValue());
                                qrAdapter.setGreenQrCodes(viewModel.getQrScanned().getValue());
                            }
                        });
                        break;
                    case "Scannés":
                        tabShowed = "Scannés";
                        viewModel.getQrScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setGreenQrCodes(strings);
                            }
                        });
                        break;
                    case "À scanner":
                        tabShowed = "À scanner";
                        viewModel.getQrNotScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(strings);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":

                        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                return;
                            }
                        });
                        break;
                    case "Scannés":

                        viewModel.getQrScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                return;
                            }
                        });
                        break;
                    case "À scanner":
                        viewModel.getQrNotScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                return;
                            }
                        });
                        break;
                }

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        tabShowed = "Tous";
                        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(viewModel.getQrNotScanned().getValue());
                                qrAdapter.setGreenQrCodes(viewModel.getQrScanned().getValue());
                            }
                        });
                        break;
                    case "Scannés":
                        tabShowed = "Scannés";
                        viewModel.getQrScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setGreenQrCodes(strings);
                            }
                        });
                        break;
                    case "À scanner":
                        tabShowed = "À scanner";
                        viewModel.getQrNotScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(strings);
                            }
                        });
                        break;
                }

            }
        });
    }

    /**
     * Méthode appelée dès que l'activité est créée
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel = ViewModelProviders.of(getActivity()).get(PickUpViewModel.class);
        mActivity = getActivity();
        qrListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        qrListView.setHasFixedSize(true);
        qrListView.setAdapter(qrAdapter);
        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (tr != null && tr.isAlive()) {
                    tr.interrupt();
                }
                Navigation.findNavController(v).popBackStack();
                sm.stopScan();
            }
        });
        tabShowed = "Tous";
        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(ArrayList<String> strings) {
                qrAdapter.setqrCodes(strings);
                qrAdapter.setRedQrCodes(viewModel.getQrNotScanned().getValue());
                qrAdapter.setGreenQrCodes(viewModel.getQrScanned().getValue());
            }
        });
        viewModel.getNbQrScanned().observe(getActivity(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(viewModel.getNbQrNotScanned().getValue() != 0)
                    nbLoaded.setText(integer+"/"+viewModel.getNbTotQr().getValue());
                else
                    nbLoaded.setText(viewModel.getNbTotQr().getValue()+"/"+viewModel.getNbTotQr().getValue());
            }
        });

    }



    /**
     * Affichage des erreurs
     * @param Errors
     */
    public void throwErrors(String Errors) {
        Toast.makeText(mActivity, Errors, Toast.LENGTH_SHORT).show();
        viewModel.clearError();
    }

    /**
     * Méthode appelée dès que le fragment s'attache à la vue
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    /**
     * Le fragment commence à interragir avec l'utilisateur
     */
    @Override
    public void onResume() {
        super.onResume();
        sm.setOutScanMode(0);
        sm.openScan();
        setupScan();
    }

    private void setupScan()
    {
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte[] barocode = intent.getByteArrayExtra("barocode");
                int barocodelen = intent.getIntExtra("length", 0);
                byte temp = intent.getByteExtra("barcodeType", (byte) 0);
                byte[] aimid = intent.getByteArrayExtra("aimid");
                barcodeStr = new String(barocode, 0, barocodelen);
                Log.e(TAG,barcodeStr);
                qrScanned(extractQrCode(barcodeStr));
                Log.e(TAG,extractQrCode(barcodeStr));
                sm.stopScan();
            }
        };
        mActivity.registerReceiver(br, new IntentFilter(SCAN_ACTION));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //sm.closeScan();
        mActivity.unregisterReceiver(br);
    }

    /**
     * Traitement de la valeur du QR Code
     * @param qr
     */
    private void qrScanned(final String qr){
        Log.e(TAG,"Methode de gestio du QR code qrScanned");


        if(qr ==NO_QR_SCANNED){
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.wrong);
            mp.start();
            throwErrors(NO_QR_SCANNED);
            return;
        }
        viewModel.qrScanned(qr);
        if(viewModel.getErrors().equals("Le QR scanné ne fait pas partie des QR à scanner")){
            Log.e(TAG, "Le QR ne fait pas partie de la liste");
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setMessage("Voulez-vous l'ajouter à la liste ?")
                    .setCancelable(false)
                    .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            scQrCodeLabel.setText(qr);
                            scQrCodeLabel.setTextColor(Color.GREEN);
                            addQROutsideList(qr);
                        }
                    });
            //Creating dialog box
            AlertDialog alert = builder.create();
            //Setting the title manually
            alert.setTitle("Le QR scanné ne fait pas partie des QR à scanner");
            alert.show();
        }
        else if(viewModel.getErrors() != ""){
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.wrong);
            mp.start();
            scQrCodeLabel.setText(qr);
            scQrCodeLabel.setTextColor(Color.RED);
            throwErrors(viewModel.getErrors());
            return;
        }
        else{
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.cool);
            mp.start();
            scQrCodeLabel.setText(qr);
            scQrCodeLabel.setTextColor(Color.GREEN);
        }

        if(tabShowed.equals("Tous")){
            Log.e(TAG,"On scan et on est dans l'onglet Tous");
            viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
                @Override
                public void onChanged(ArrayList<String> strings) {
                    qrAdapter.setqrCodes(strings);
                    qrAdapter.setRedQrCodes(viewModel.getQrNotScanned().getValue());
                    qrAdapter.setGreenQrCodes(viewModel.getQrScanned().getValue());
                }
            });
        }else if(tabShowed.equals("Scannés")){
            Log.e(TAG,"On scan et on est dans l'onglet Scannés");
            viewModel.getQrScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                @Override
                public void onChanged(ArrayList<String> strings) {
                    qrAdapter.setqrCodes(strings);
                    qrAdapter.setGreenQrCodes(strings);
                }
            });
        }else if(tabShowed.equals("À scanner")){
            Log.e(TAG,"On scan et on est dans l'onglet A scanner");
            viewModel.getQrNotScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                @Override
                public void onChanged(ArrayList<String> strings) {
                    qrAdapter.setqrCodes(strings);
                    qrAdapter.setRedQrCodes(strings);
                }
            });
        }



    }

    public void addQROutsideList(final String qr){
        // TODO route back pour ajouter QR code qui n'est pas dans la liste
        apiRequest.addQROutsideList((GetAndSetToken.getBearerWithToken()),qr, viewModel.getPickupId()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, String.valueOf(response.raw()));
                if(response.code()==200){
                    Toast.makeText(getContext(),"QR code ajouté",Toast.LENGTH_SHORT).show();
                    viewModel.addQROutsideList(qr);
                    if(tabShowed.equals("Tous")){
                        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(viewModel.getQrNotScanned().getValue());
                                qrAdapter.setGreenQrCodes(viewModel.getQrScanned().getValue());
                            }
                        });
                    }else if(tabShowed.equals("Scannés")){
                        viewModel.getQrScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setGreenQrCodes(strings);
                            }
                        });
                    }else if(tabShowed.equals("À scanner")){
                        viewModel.getQrNotScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(strings);
                            }
                        });
                    }
                }else{
                    Toast.makeText(getContext(),"Impossible d'ajouter le QR code",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getContext(),"Vérifiez la connexion internet",Toast.LENGTH_SHORT).show();
            }
        });

    }


}
