package com.example.boxifylogistics.ParcelOut.DropOff.Fragments;

import android.content.Intent;
import android.device.ScanDevice;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Utils.GPSTracker;
import com.example.boxifylogistics.ViewModel.DropOffViewModel;
import com.kyanogen.signatureview.SignatureView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class DropOffSignature extends Fragment {

    private static final String TAG = "DropOffSignature";
    SignatureView signatureView;
    private Bitmap bm;
    private LinearLayout vln;

    private Button validate;
    private Button clear;
    private TextView modify;


    private View v;
    private DropOffViewModel viewModel;

    private Button finishDropOff;

    private ScanDevice sm = new ScanDevice();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.dropoff_signature, container, false);
        signatureCardView();

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(DropOffViewModel.class);
        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(v).popBackStack();
            }
        });

    }

    public void signatureCardView(){
        finishDropOff = v.findViewById(R.id.finish_dropoff);
        signatureView = v.findViewById(R.id.signature_view_dropoff);
        validate = v.findViewById(R.id.btn_validate_dropoff);
        clear = v.findViewById(R.id.btn_clear_dropoff);
        modify = v.findViewById(R.id.modif_signature_dropoff);
        signatureListeners();
    }
    public void signatureListeners(){
        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate.getVisibility() == View.GONE){
//                    signatureView.setVisibility(View.VISIBLE);
                    signatureView.setEnableSignature(true);
                    validate.setVisibility(View.VISIBLE);
                    clear.setVisibility(View.VISIBLE);
                    modify.setVisibility(View.GONE);

                }
            }
        });
        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bm = signatureView.getSignatureBitmap();
                validate.setVisibility(View.GONE);
                clear.setVisibility(View.GONE);
                signatureView.setEnableSignature(false);
                modify.setVisibility(View.VISIBLE);
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clearCanvas();
                bm = null;
            }

        });

        finishDropOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bm != null){
                    try {
                        viewModel.endDropOff(bitmapToFile(bm));
                        GPSTracker gpsTracker = new GPSTracker(getActivity());
                        Log.e(TAG,gpsTracker.getLocation().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Intent it = new Intent(getActivity(), MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("ymd",viewModel.getDate());
                    it.putExtras(bundle);
                    startActivity(it);
                }
                else
                    Toast.makeText(getActivity(), "Il n'y a pas de signature", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private File bitmapToFile(Bitmap bitmap) throws IOException {
        File f = new File(getActivity().getCacheDir(), "signature.jpg");
        f.createNewFile();
        //indexationViewModel.getIndexedItem().setPhoto("photo.jpg");

        //Convert bitmap to byte array
        Bitmap bmp = bitmap;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100/*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    @Override
    public void onResume() {
        super.onResume();
        sm.closeScan();
    }
}