package com.example.boxifylogistics.ParcelOut.Pickup.Fragments;

import android.content.Intent;
import android.device.ScanDevice;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity;
import com.example.boxifylogistics.ParcelOut.Pickup.adapters.QrCodeAdapter;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.ViewModel.PickUpViewModel;
import com.google.android.material.tabs.TabLayout;
import com.kyanogen.signatureview.SignatureView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PickupSignature extends Fragment {

    private static final String TAG = "PickUpSignature";
    SignatureView signatureView;
    private Bitmap bm;
    private LinearLayout vln;

    private TextView nbItemsDelivered;

    private Button validate;
    private Button clear;
    private TextView modify;


    private View v;
    private PickUpViewModel viewModel;

    private Button finishDelivery;

    //Items Lists
    private NestedScrollView nestedScrollView;
    private RecyclerView qrListView;
    private QrCodeAdapter qrAdapter = new QrCodeAdapter();
    private TabLayout tabLayout;
    private TextView nbLoaded;

    private ScanDevice sm = new ScanDevice();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.pickup_signature, container, false);
        vln = v.findViewById(R.id.vtextview_lists);
        qrCodeListBinding();
        qrCodeListListener();

        nbItemsDelivered = v.findViewById(R.id.nb_it_delivered);
        signatureCardView();
        finishDelivery = v.findViewById(R.id.finish_delivery);



        return v;
    }
    public void qrCodeListBinding() {
        nbLoaded = v.findViewById(R.id.nb_loaded);
        tabLayout = v.findViewById(R.id.tabLayout);
        qrListView = v.findViewById(R.id.qr_list);
        nestedScrollView = v.findViewById(R.id.scrv_item_list);
        finishDelivery = v.findViewById(R.id.finish_pickup);

    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(PickUpViewModel.class);
        qrListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        qrListView.setHasFixedSize(true);
        qrListView.setAdapter(qrAdapter);
        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {

                Navigation.findNavController(v).popBackStack();

            }
        });

        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(ArrayList<String> strings) {
                qrAdapter.setqrCodes(strings);
                qrAdapter.setRedQrCodes(viewModel.getQrNotScanned().getValue());
                qrAdapter.setGreenQrCodes(viewModel.getQrScanned().getValue());
            }
        });
        viewModel.getNbQrScanned().observe(getActivity(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(viewModel.getNbQrNotScanned().getValue() != 0)
                    nbLoaded.setText(integer+"/"+viewModel.getNbTotQr().getValue());
                else
                    nbLoaded.setText(viewModel.getNbTotQr().getValue()+"/"+viewModel.getNbTotQr().getValue());
            }
        });
    }

    public void signatureCardView(){
        signatureView = v.findViewById(R.id.signature_view);
        validate = v.findViewById(R.id.btn_validate);
        clear = v.findViewById(R.id.btn_clear);
        modify = v.findViewById(R.id.modif_signature);

        signatureListeners();
    }
    public void signatureListeners(){
        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate.getVisibility() == View.GONE){
//                    signatureView.setVisibility(View.VISIBLE);
                    signatureView.setEnableSignature(true);
                    validate.setVisibility(View.VISIBLE);
                    clear.setVisibility(View.VISIBLE);
                    modify.setVisibility(View.GONE);

                }
            }
        });
        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bm = signatureView.getSignatureBitmap();
                validate.setVisibility(View.GONE);
                clear.setVisibility(View.GONE);
                signatureView.setEnableSignature(false);
                modify.setVisibility(View.VISIBLE);


            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clearCanvas();
                bm = null;
            }

        });
    }
    public void qrCodeListListener() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(viewModel.getQrNotScanned().getValue());
                                qrAdapter.setGreenQrCodes(viewModel.getQrScanned().getValue());
                            }
                        });
                        break;
                    case "Scannés":
                        viewModel.getQrScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setGreenQrCodes(strings);
                            }
                        });
                        break;
                    case "À scanner":
                        viewModel.getQrNotScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(strings);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                return;
                            }
                        });
                        break;
                    case "Scannés":
                        viewModel.getQrScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                return;
                            }
                        });
                        break;
                    case "À scanner":
                        viewModel.getQrNotScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                return;
                            }
                        });
                        break;
                }

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        viewModel.getAllQr().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(viewModel.getQrNotScanned().getValue());
                                qrAdapter.setGreenQrCodes(viewModel.getQrScanned().getValue());
                            }
                        });
                        break;
                    case "Scannés":
                        viewModel.getQrScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setGreenQrCodes(strings);
                            }
                        });
                        break;
                    case "À scanner":
                        viewModel.getQrNotScanned().observe(getActivity(), new Observer<ArrayList<String>>() {
                            @Override
                            public void onChanged(ArrayList<String> strings) {
                                qrAdapter.setqrCodes(strings);
                                qrAdapter.setRedQrCodes(strings);
                            }
                        });
                        break;
                }

            }
        });
        finishDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bm != null){
                    try {
                        viewModel.finishPickup(bitmapToFile(bm));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Intent it = new Intent(getActivity(), MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("ymd",viewModel.getDate());
                    it.putExtras(bundle);
                    startActivity(it);
                }
                else
                    Toast.makeText(getActivity(), "Il n'y a pas de signature", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private File bitmapToFile(Bitmap bitmap) throws IOException {
        File f = new File(getActivity().getCacheDir(), "signature.jpg");
        f.createNewFile();
        //indexationViewModel.getIndexedItem().setPhoto("photo.jpg");

        //Convert bitmap to byte array
        Bitmap bmp = bitmap;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100/*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    @Override
    public void onResume() {
        super.onResume();
        sm.closeScan();
    }
}