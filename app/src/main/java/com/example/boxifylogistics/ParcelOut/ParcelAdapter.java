package com.example.boxifylogistics.ParcelOut;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.R;
import com.example.boxifylogistics.model.Appointement;
import com.example.boxifylogistics.model.Parcel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.lang.Integer.parseInt;

public class ParcelAdapter extends RecyclerView.Adapter<ParcelAdapter.PackageViewHolder> {
    private static final String TAG = "ParcelAdapter";
    private List<Parcel> parcels = new ArrayList<>();
    private int arrivedPackage  = -1 ;
    private View itemView;
    private Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    private String date = simpleDateFormat.format(calendar.getTime());
    private String statusAppointement;


    private onParcelClickListener listener;

    public void setArrivedPickupId(int id){
        arrivedPackage = id;
    }

    public void setParcels(List<Parcel> parcels) {
        this.parcels = parcels;
        Log.e(TAG, "setParcels: "+parcels );
        notifyDataSetChanged();
    }

    /**
     * Listener du click sur les package
     */
    public interface onParcelClickListener {
        void onPackageClick(int position, int parcel_id, Appointement.TypeAppointment type, String statusAppointement);
        void onNavButtonClick(int position);
        void onProceedToLoading(int position);
    }
    public void setOnPickUpClickListener(onParcelClickListener listener) {
        this.listener = listener;
    }

    /**
     * A la création de la vue
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public PackageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.parcel_details, parent, false);
        return new PackageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageViewHolder holder, int position) {
        Parcel aParcel = parcels.get(position);
        holder.display(aParcel);
    }

    /**
     * Envoie le nombre de parcel dans l'array
     * @return
     */
    @Override
    public int getItemCount() {
        return parcels.size();
    }

    /**
     * Holder d'un parcel
     */
    public class PackageViewHolder extends RecyclerView.ViewHolder{

        CardView parcelView;
        TextView parcelHour;
        TextView parcelType;
        TextView parcelCustomerFullname;
        TextView parcelAddress;
        TextView parcelCusCiv;
        Appointement.TypeAppointment type;
        RelativeLayout parcelTitleLayout;
        View parcelBottomView;
        ImageView parcelProfilIcon;
        ImageView parcelPlaceIcon;
        LinearLayout layoutFinish;
        LinearLayout layoutItemType;
        LinearLayout layoutItemPicture;
        LinearLayout layoutItemInfos;
        int id;

        /**
         * Mise en place de toutes les view
         * @param itemView
         */
        public PackageViewHolder(@NonNull View itemView) {
            super(itemView);
            parcelHour = itemView.findViewById(R.id.parcel_hour);
            parcelType = itemView.findViewById(R.id.parcel_type);
            parcelAddress = itemView.findViewById(R.id.parcel_address);
            parcelCustomerFullname = itemView.findViewById(R.id.parcel_customer_fullname);
            parcelCusCiv = itemView.findViewById(R.id.parcel_customer_civ);
            parcelView = itemView.findViewById(R.id.cv);
            parcelTitleLayout = itemView.findViewById(R.id.parcel_title_layout);
            parcelBottomView = itemView.findViewById(R.id.parcel_bottom_view);
            parcelProfilIcon = itemView.findViewById(R.id.parcel_profil_icon);
            parcelPlaceIcon = itemView.findViewById(R.id.parcel_place_icon);
            layoutFinish = itemView.findViewById(R.id.layout_finish);
            layoutItemType = itemView.findViewById(R.id.layout_item_type);
            layoutItemPicture = itemView.findViewById(R.id.layout_item_picture);
            layoutItemInfos = itemView.findViewById(R.id.layout_item_infos);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPackageClick(getAdapterPosition(),id,type,statusAppointement);
                }
            });
        }

        /**
         * Affichage du parcel
         * @param aParcel
         */
        public void display(Parcel aParcel){
            type = aParcel.getType();
            id = aParcel.getId();
            statusAppointement = aParcel.getAppointment().getStatus();
            parcelType.setText(aParcel.getAppointment().getTypeString());
            parcelHour.setText(aParcel.getAppointment().getHourAppoint());
            parcelCustomerFullname.setText(aParcel.getUser().getFull_name());
            parcelCusCiv.setText(String.valueOf(aParcel.getUser().getCustomer_id()));
            parcelAddress.setText(aParcel.getAddress().getCompletedAddress());


            Log.e(TAG,aParcel.getTypeString());
            Log.e(TAG,aParcel.getAppointment().getTypeString());




            if(aParcel.getAppointment().getTypeString().equals("Pickup")){
                parcelTitleLayout.setBackgroundColor(Color.parseColor("#fec200"));
                parcelBottomView.setBackgroundColor(Color.parseColor("#fec200"));
                parcelProfilIcon.setImageResource(R.drawable.ic_person_yellow_24dp);
                parcelPlaceIcon.setImageResource(R.drawable.ic_place_yellow_24dp);
            }else if(aParcel.getAppointment().getTypeString().equals("Delivery")){
                parcelTitleLayout.setBackgroundColor(Color.parseColor("#007398"));
                parcelBottomView.setBackgroundColor(Color.parseColor("#007398"));
                parcelProfilIcon.setImageResource(R.drawable.ic_person_blue_24dp);
                parcelPlaceIcon.setImageResource(R.drawable.ic_place_blue_24dp);
            }else{
                parcelTitleLayout.setBackgroundColor(Color.parseColor("#CC003D"));
                parcelBottomView.setBackgroundColor(Color.parseColor("#CC003D"));
                parcelProfilIcon.setImageResource(R.drawable.ic_person_red_24dp);
                parcelPlaceIcon.setImageResource(R.drawable.ic_place_red_24dp);
            }
            // Changement de la couleur si le RDV est terminé
            // Status possibles : NO_STATUS | CREATED | PENDING | CANCELLED | COMPLETED
            if(aParcel.getAppointment().getStatus().equals("COMPLETED")){
                layoutFinish.setVisibility(View.VISIBLE);
                String[] appointmentDay = aParcel.getAppointment().getDate().split("T");
                appointmentDay[0] = appointmentDay[0].replace("-","");
                Log.e(TAG+"Date de ouf", parseInt(appointmentDay[0])+" < "+parseInt(date));
                if(parseInt(appointmentDay[0]) < parseInt(date)){
                    itemView.setClickable(false);
                }else{
                    itemView.setClickable(true);
                }
            } else if(aParcel.getAppointment().getStatus().equals("CREATED")){
                layoutFinish.setVisibility(View.GONE);
                itemView.setClickable(true);
                String[] appointmentDay = aParcel.getAppointment().getDate().split("T");
                appointmentDay[0] = appointmentDay[0].replace("-","");
                if(parseInt(appointmentDay[0]) < parseInt(date)){
                    parcelTitleLayout.setBackgroundColor(Color.parseColor("#cc003d"));
                }else{
                    itemView.setClickable(true);
                }
                // Changement de couleur en fonction du type de RDV
            }
        }
    }
}
