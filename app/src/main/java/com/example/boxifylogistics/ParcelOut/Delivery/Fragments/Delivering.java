package com.example.boxifylogistics.ParcelOut.Delivery.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.ParcelOut.Delivery.adapters.ItemAdapter;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.model.Item;
import com.example.boxifylogistics.ViewModel.DeliveryViewModel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class Delivering extends Fragment {

    String TAG = "Delivering";
    private final String NO_QR_SCANNED = "Pas de QR code scanné";
    private DeliveryViewModel viewModel;
    private View v;
    private ScanDevice sm = new ScanDevice();
    private String barcodeStr;
    final static String SCAN_ACTION = "scan.rcv.message";
    private BroadcastReceiver br;

    //Scanner
    private TextView numPad;
    private TextView nbLoaded;
    private Button scannerManually;

    //private StKeyManager.ShortcutKeyMonitor.ShortcutKeyListener scanHandleKeyListener;
    //private StKeyManager.ShortcutKeyMonitor scanHandleKeyMonitor;
    private AtomicBoolean isScanning;
    private Handler handler;

    private TextView scQrCodeLabel;
    private TextView scItemNameLabel;
    private TextView scVolumeLabel;

    //Items Lists
    private NestedScrollView nestedScrollView;
    private RecyclerView itemsListView;
    private ItemAdapter itemAdapter;
    private TextView collexp;
    private TabLayout tabLayout;

    private String tabShowed;

    //Deliver
    private Button toSignature;

    Activity mActivity;
    Thread handlerThread;

    /**
     * Méthode appelée à la création de la View (= après onAttach)
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.delivering, container, false);

        itemAdapter = new ItemAdapter(getContext());

        // On mets en place toutes les vues
        bindCardViews();
        // Mise en place des listeners sur l'Activity (Ici : bouton "Passer à la signature")
        listeners();
        // Initiation du handler (tâche de fond)
        initHandlerThread();
        isScanning = new AtomicBoolean(false);

        return v;
    }

    /**
     * Récupère juste le numéro du QR Code
     * @param url
     * @return
     */
    private String extractQrCode(String url){
        return url.substring(url.lastIndexOf("/") + 1);
    }

    /**
     * Méthode appelée une fois l'activité créée (= après onCreateView)
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(DeliveryViewModel.class);

        tabLayout = new TabLayout(getActivity());
        itemsListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        itemsListView.setHasFixedSize(true);
        mActivity = getActivity();
        itemsListView.setAdapter(itemAdapter);

        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setItems(items);
                itemAdapter.isAll();
            }
        });
        viewModel.getItemDelivered().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setGreen(items);
            }
        });
        viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setBlue(items);
            }
        });
        viewModel.getItemToLoad().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setRed(items);
            }
        });

        tabShowed = "Tous";
        viewModel.getNbDeliveredItems().observe(getActivity(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (viewModel.getNbLoadedItems().getValue() != 0 || integer != 0){
                    int total =  viewModel.getNbLoadedItems().getValue() + integer;
                    nbLoaded.setText(integer + "/" + total);
                }
                else if(viewModel.getNbLoadedItems().getValue() == 0 && integer==0 ){
                    nbLoaded.setText("no Deliveries");
                }
            }
        });
    }

    /**
     * Mise en place des vues
     */
    public void bindCardViews() {
        bindCardViewScanner();
        bindCardViewItemsList();
        toSignature = v.findViewById(R.id.to_signature);
    }

    /**
     * Mise en place de la vue "Scanner + Entrer QR à la main + QR venant d'être scanné"
     */
    public void bindCardViewScanner() {
        numPad = v.findViewById(R.id.numpad);
        scannerManually = v.findViewById(R.id.btn_scan);
        scQrCodeLabel = v.findViewById(R.id.scqrcode_label);
        scItemNameLabel = v.findViewById(R.id.scitemname_label);
        scVolumeLabel = v.findViewById(R.id.scvolume_label);

    }

    /**
     * Mise en place de la NestedScrollView contenant la RecyclerView de la liste des items
     */
    public void bindCardViewItemsList() {
        nbLoaded = v.findViewById(R.id.nb_loaded);
        itemsListView = v.findViewById(R.id.item_list);
        tabLayout = v.findViewById(R.id.tabLayout);
        collexp = v.findViewById(R.id.expcoll);
        nestedScrollView = v.findViewById(R.id.scrv_item_list);
    }

    /**
     * Mise en place des listeners sur l'Activity
     * Ici : Bouton de lancement de la signature
     */
    public void listeners() {
        itemListListener();
//        switchListener();
        manualScanListener();
        toSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if(!viewModel.isAllDelivered()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setMessage(viewModel.getNbNotLoadedItems().getValue() + " article(s) sont toujours chez Boxify\nIl vous manque "+viewModel.getItemLoaded().getValue()+" article(s) qui sont dans le camion \nVous avez livré "+viewModel.getNbDeliveredItems().getValue()+" article(s)\nÊtes vous sur de vouloir continuer ?")
                            .setCancelable(false)
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (handlerThread != null && handlerThread.isAlive()) {
                                        handlerThread.interrupt();
                                    }
                                    //deactivateScan();
                                    // Lancement de la vue suivante "delivering"
                                    Navigation.findNavController(v).navigate(R.id.delivery_signature);
                                    dialog.cancel();
                                }
                            })
                    ;
                    //Creating dialog box

                    AlertDialog alert = builder.create();
                    //Setting the title manually

                    alert.setTitle("Attention");
                    alert.show();
                }
                else{
                    //deactivateScan();
                    // Lancement de la vue suivante "delivery_signature"
                    Navigation.findNavController(v).navigate(R.id.delivery_signature);
                }


            }
        });
    }

    public void initHandlerThread() {
        HandlerThread handlerThread = new HandlerThread("");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    /**
     * Gestion du scan manuel (sans gachette, ou entré à la main)
     */
    public void manualScanListener() {
        scannerManually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sm.startScan();
            }
        });

        numPad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                        mActivity);

                final EditText saySomething = new EditText(mActivity);
                saySomething.setInputType(InputType.TYPE_CLASS_NUMBER);
                sayWindows.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String mString = saySomething.getText().toString();
                                if (!mString.matches("^[0-9]+$"))
                                    Toast.makeText(mActivity, "N'entrez que des nombres", Toast.LENGTH_SHORT).show();
                                else
                                    qrScanned(saySomething.getText().toString());
                                // Your checkin() method
                            }
                        });

                sayWindows.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "Cancel");
                            }
                        });

                sayWindows.setView(saySomething);
                sayWindows.create().show();
            }
        });
    }

    /**
     * Gestion de l'affichage de la liste d'items
     */
    public void itemListListener() {
        collexp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final float scale = getResources().getDisplayMetrics().density;
                if (collexp.getText() == "Étendre") {
                    nestedScrollView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    collexp.setText("Réduire");
                } else {
                    nestedScrollView.getLayoutParams().height = (int) (150 * scale);
                    collexp.setText("Étendre");
                }

            }
        });
        tabListeners();
    }

    /**
     * Gestion de la partie onglet de la liste d'Item
     */
    //A modifier pour alimenter la liste des delivers
    public void tabListeners() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        tabShowed = "Tous";
                        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setGreen(viewModel.getItemDelivered().getValue());
                                itemAdapter.setRed(viewModel.getItemToLoad().getValue());
                                itemAdapter.setBlue(viewModel.getItemLoaded().getValue());
                                itemAdapter.isAll();
                            }
                        });
                        break;
                    case "Livrés":
                        tabShowed = "Livrés";
                        viewModel.getItemDelivered().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setGreen(items);
                                itemAdapter.isNotAll();
                            }
                        });
                        break;
                    case "À livrer":
                        tabShowed = "À livrer";
                        viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setBlue(items);
                                itemAdapter.isNotAll();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(new ArrayList<Item>());
                                return;
                            }
                        });
                        break;
                    case "Livrés":
                        viewModel.getItemDelivered().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(new ArrayList<Item>());
                                return;
                            }
                        });
                        break;
                    case "À livrer":
                        viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(new ArrayList<Item>());
                                return;
                            }
                        });
                        break;
                }

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        tabShowed = "Tous";
                        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setGreen(viewModel.getItemDelivered().getValue());
                                itemAdapter.setRed(viewModel.getItemToLoad().getValue());
                                itemAdapter.setBlue(viewModel.getItemLoaded().getValue());
                                itemAdapter.isAll();
                            }
                        });
                        break;
                    case "Livrés":
                        tabShowed = "Livrés";
                        viewModel.getItemDelivered().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setGreen(items);
                                itemAdapter.isNotAll();
                            }
                        });
                        break;
                    case "À livrer":
                        tabShowed = "À livrer";
                        viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setBlue(items);

                                itemAdapter.isNotAll();
                            }
                        });
                        break;
                }

            }
        });
    }

    /**
     * Gestion du scan d'un QR code
     * @param qr
     */
    public void qrScanned(String qr) {
        Log.e(TAG, "qrScanned: " + qr);
        if (qr == NO_QR_SCANNED) {
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.wrong);
            mp.start();
            throwErrors(NO_QR_SCANNED);
            return;
        }
        Item it = viewModel.qrDelivered(qr);
        if (viewModel.getErrors() == "" && it != null) {
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.cool);
            mp.start();
            setItemScanned(it);
        }
        else{
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.wrong);
            mp.start();
            throwErrors(viewModel.getErrors());
        }

        if(tabShowed.equals("Tous")){
            viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
                @Override
                public void onChanged(ArrayList<Item> items) {
                    itemAdapter.setItems(items);
                    itemAdapter.setGreen(viewModel.getItemDelivered().getValue());
                    itemAdapter.setRed(viewModel.getItemToLoad().getValue());
                    itemAdapter.setBlue(viewModel.getItemLoaded().getValue());
                    itemAdapter.isAll();
                }
            });
        }else if(tabShowed.equals("Livrés")){
            viewModel.getItemDelivered().observe(getActivity(), new Observer<ArrayList<Item>>() {
                @Override
                public void onChanged(ArrayList<Item> items) {
                    itemAdapter.setItems(items);
                    itemAdapter.setGreen(items);
                    itemAdapter.isNotAll();
                }
            });
        }else if(tabShowed.equals("À livrer")){
            viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
                @Override
                public void onChanged(ArrayList<Item> items) {
                    itemAdapter.setItems(items);
                    itemAdapter.setBlue(items);

                    itemAdapter.isNotAll();
                }
            });
        }

    }

    /**
     * Gestion au scan d'un item (affichage)
     * @param it
     */
    public void setItemScanned(Item it) {
        scQrCodeLabel.setText(Integer.toString(it.getBoxify_id()));
        scQrCodeLabel.setTextColor(getResources().getColor(R.color.green));
        scItemNameLabel.setText(it.getName());
        scItemNameLabel.setTextColor(getResources().getColor(R.color.green));
        scVolumeLabel.setText(it.getVolume() + "m³");
        scVolumeLabel.setTextColor(getResources().getColor(R.color.green));
    }

    /**
     * Affichage dun toast d'erreur lors du scan
     * @param Errors
     */
    public void throwErrors(String Errors) {
        Toast.makeText(mActivity, Errors, Toast.LENGTH_SHORT).show();
        viewModel.clearErrors();
    }

    /**
     * Méthode appelée après que l'activité soit créée
     */
    @Override
    public void onStart() {
        super.onStart();

        //activateScan();
        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setItems(items);
                itemAdapter.isAll();
            }
        });
    }

    /**
     * Méthode appelée dès que le fragment s'attache à la vue
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity)
            mActivity = (Activity) context;
    }

    private void setupScan()
    {
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte[] barocode = intent.getByteArrayExtra("barocode");
                int barocodelen = intent.getIntExtra("length", 0);
                byte temp = intent.getByteExtra("barcodeType", (byte) 0);
                byte[] aimid = intent.getByteArrayExtra("aimid");
                barcodeStr = new String(barocode, 0, barocodelen);
                Log.e(TAG,barcodeStr);
                qrScanned(extractQrCode(barcodeStr));
                Log.e(TAG,extractQrCode(barcodeStr));
                sm.stopScan();
            }
        };
        mActivity.registerReceiver(br, new IntentFilter(SCAN_ACTION));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.unregisterReceiver(br);
    }

    /**
     * Le fragment commence à interragir avec l'utilisateur
     */
    @Override
    public void onResume() {
        super.onResume();
        sm.setOutScanMode(0);
        sm.openScan();
        setupScan();
    }

}
