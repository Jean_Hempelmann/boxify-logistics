package com.example.boxifylogistics.ParcelOut.Delivery.Fragments;

import android.content.Intent;
import android.device.ScanDevice;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.model.Item;
import com.example.boxifylogistics.ViewModel.DeliveryViewModel;
import com.kyanogen.signatureview.SignatureView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

public class DeliverySignature extends Fragment {

    private ScanDevice sm = new ScanDevice();
    SignatureView signatureView;
    private Bitmap bm;
    private LinearLayout vln;

    private TextView nbItemsDelivered;
    private TextView nbItemsInStorage;
    private TextView nbItemsInTruck;

    private Button validate;
    private Button clear;
    private TextView modify;

    private View v;
    private DeliveryViewModel viewModel;

    private Button finishDelivery;

    /**
     * Méthode appelée à la création de la View (= après onAttach)
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.delivery_signature, container, false);
        /* On mets en place toutes les vues */
        vln = v.findViewById(R.id.vtextview_lists);
        nbItemsDelivered = v.findViewById(R.id.nb_it_delivered);
        nbItemsInStorage = v.findViewById(R.id.nb_it_in_storage);
        nbItemsInTruck = v.findViewById(R.id.nb_it_in_truck);
        signatureCardView();
        finishDelivery = v.findViewById(R.id.finish_delivery);
        return v;
    }

    /**
     * Méthode appelée une fois l'activité créée (= après onCreateView)
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(DeliveryViewModel.class);
        viewModel.getItemDelivered().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                nbItemsDelivered.setText(items.size()+"/"+viewModel.getAllItems().getValue().size());
                nbItemsInStorage.setText(""+viewModel.getItemToLoad().getValue().size());
                nbItemsInTruck.setText(""+viewModel.getItemLoaded().getValue().size());
            }
        });
        // Gestion de l'appui sur le bouton de fin de livraison
        finishDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bm != null){
                    try {
                        viewModel.finishDelivery(bitmapToFile(bm));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Intent pour l'activité ParceListActivity
                    Intent it = new Intent(getActivity(), MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("ymd",viewModel.getDate());
                    it.putExtras(bundle);
                    startActivity(it);
                }
                else
                    Toast.makeText(getActivity(), "Il n'y a pas de signature", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Méthode appelée après que l'activité soit créée
     */
    @Override
    public void onStart() {
        super.onStart();
        viewModel.getItemDelivered().observe(this, new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {

                if(!items.isEmpty()){
                    items.sort(new Comparator<Item>() {
                        @Override
                        public int compare(Item o1, Item o2) {
                            return o1.getBoxify_id()-o2.getBoxify_id();
                        }
                    });

                    for (Item it : items) {
                        if(it!=null){
                            View ti = getLayoutInflater().inflate(R.layout.delivered_items, null);
                            TextView tr = ti.findViewById(R.id.Qr);
                            TextView tame = ti.findViewById(R.id.name);
                            tr.setText(Integer.toString(it.getBoxify_id()));
                            tame.setText(it.getName());
                            vln.addView(ti);
                        }
                    }
                }

            }
        });
    }

    /**
     * Mise en place de la vue "Signature"
     */
    public void signatureCardView(){
        signatureView = v.findViewById(R.id.signature_view);
        validate = v.findViewById(R.id.btn_validate);
        clear = v.findViewById(R.id.btn_clear);
        modify = v.findViewById(R.id.modif_signature);
        //Mise en place des listeners sur l'Activity (Ici : Modifier signature + Valider signature + Effacer signature)
        signatureListeners();
    }

    /**
     * Mise en place des listeners sur l'Activity
     * Ici : Modifier signature + Valider signature + Effacer signature
     */
    public void signatureListeners(){
        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate.getVisibility() == View.GONE){
//                    signatureView.setVisibility(View.VISIBLE);
                    signatureView.setEnableSignature(true);
                    validate.setVisibility(View.VISIBLE);
                    clear.setVisibility(View.VISIBLE);
                    modify.setVisibility(View.GONE);

                }
            }
        });
        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bm = signatureView.getSignatureBitmap();
                validate.setVisibility(View.GONE);
                clear.setVisibility(View.GONE);
                signatureView.setEnableSignature(false);
                modify.setVisibility(View.VISIBLE);


            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clearCanvas();
                bm = null;
            }

        });

    }

    private File bitmapToFile(Bitmap bitmap) throws IOException {
        File f = new File(getActivity().getCacheDir(), "signature.jpg");
        f.createNewFile();
        //indexationViewModel.getIndexedItem().setPhoto("photo.jpg");


        //Convert bitmap to byte array
        Bitmap bmp = bitmap;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100/*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    @Override
    public void onResume() {
        super.onResume();
        sm.closeScan();
    }
}
