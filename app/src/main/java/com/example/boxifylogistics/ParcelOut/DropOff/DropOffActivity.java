package com.example.boxifylogistics.ParcelOut.DropOff;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.ViewModel.DropOffViewModel;
import com.example.boxifylogistics.ViewModel.DropOffViewModel;
import com.example.boxifylogistics.model.DropOff;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jean Hempelmann on 29/07/2020.
 * Boxify
 * #StagiaireVie
 */
public class DropOffActivity extends AppCompatActivity {

    private static final String TAG = "DropOffActivity";
    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    private int dropoffId;
    private String status="";
    private ArrayList<String> date;
    private DropOffViewModel viewModel;

    /**
     * Méthode appelée à la création de l'activité
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropoff);

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView logoToolBar = toolbar.findViewById(R.id.logoXmarks);
        logoToolBar.setImageResource(R.drawable.ic_title_dropoff);
        setSupportActionBar(toolbar);

        viewModel = ViewModelProviders.of(this).get(DropOffViewModel.class);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int value = bundle.getInt("value");
            status = bundle.getString("status");
            viewModel.setDropOffId(value);
            viewModel.setDate(bundle.getStringArrayList("ymd"));
        }

    }

    /**
     * Méthode appelée dès que les options du menu sont crées
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Méthode de gestion de l'appui sur le bouton Home
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(DropOffActivity.this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("ymd",viewModel.getDate());
                intent.putExtras(bundle);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
