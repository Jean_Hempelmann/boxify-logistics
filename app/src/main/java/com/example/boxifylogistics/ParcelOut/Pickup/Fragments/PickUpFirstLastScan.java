package com.example.boxifylogistics.ParcelOut.Pickup.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.ViewModel.PickUpViewModel;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity.extractQrCode;

public class PickUpFirstLastScan extends Fragment {

    String TAG = "PickUpFirstLastScan";
    String NO_QR_SCANNED = "Aucun Qr n'a été scanné";
    private ScanDevice sm = new ScanDevice();
    private String barcodeStr;
    final static String SCAN_ACTION = "scan.rcv.message";
    private BroadcastReceiver br;

    private View v;
    private PickUpViewModel viewModel;

    private AtomicBoolean isScanning;
    private Handler handler;

    //Premierscan view
    private CardView firstScanView;
    private TextView firstScanTitle;
    private TextView firstQrCodeScan;
    private Button firstManualScan;
    private ImageView numPadFirstCode;

    //QrCodesCollés view
    private CardView checkQrStickedView;
    private TextView checkQrStickedTitle;
    private CheckBox checkQrSticked;


    //SecondScan View
    private CardView secondScanView;
    private TextView secondScanTitle;
    private TextView secondQrCodeScan;
    private Button secondManualScan;
    private ImageView numPadLastCode;


    private int activeView = 0;
    Thread handlerThread;

    private boolean isValid;

    private Button beginAllScan;

    Activity mActivity;

    /**
     * Méthode appelée à la création de la View (= après onAttach)
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.pickup_first_last_scan, container, false);

        cardViews();
        listeners();

        desactivateView(1);
        desactivateView(2);
        desactivateView(3);
        activateView(1);

        initHandlerThread();
        isScanning = new AtomicBoolean(false);


        return v;

    }



    /**
     * Méthode appelée une fois que la vue est créée (après onCreateView)
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Mise en place du scan

    }

    /**
     * Méthode appelée une fois l'activité créée (= après onCreateView)
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(PickUpViewModel.class);
        mActivity  = getActivity();
        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (handlerThread != null && handlerThread.isAlive()) {
                    handlerThread.interrupt();
                }
                Navigation.findNavController(v).popBackStack();
                //sm.stopScan();
            }
        });

        viewModel.getFirstQrCode().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {

                if (!viewModel.scannerHasBeenUsed()) {
                    if (!s.equals("")) {
                        firstQrCodeScan.setText(s);
                        firstManualScan.setVisibility(View.GONE);
                        approuveView(1);
                        activateView(2);
                    } else {
                        firstQrCodeScan.setText(null);
                        firstQrCodeScan.setHint("SCAN");
                        viewModel.clearFirstQr();
                    }
                }

            }
        });
        viewModel.getSecondQrCode().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!viewModel.scannerHasBeenUsed()) {
                    if (!s.equals("")) {
                        secondQrCodeScan.setText(s);
                        approuveAllViews();
                    } else {
                        secondQrCodeScan.setText(null);
                        secondQrCodeScan.setHint("SCAN");
                        viewModel.clearSecondQr();
                    }
                }
            }
        });

    }

    /**
     * Mise en place des vues
     */
    public void cardViews() {
        firstScanCardView();
        qrStickCardView();
        secondScanCardView();
        beginAllScan = v.findViewById(R.id.begin_all_qr_scan);
    }

    /**
     * Mise en place des vues de la partie FirstScan
     */
    public void firstScanCardView() {
        firstScanView = v.findViewById(R.id.firstScanView);
        firstScanTitle = v.findViewById(R.id.first_scan_label);
        firstQrCodeScan = v.findViewById(R.id.firstQrScanned);
        firstManualScan = v.findViewById(R.id.btn_fscan);
        numPadFirstCode = v.findViewById(R.id.numpad_firstcode);
    }

    /**
     * Mise en place des vues de la partie QR Collés
     */
    public void qrStickCardView() {
        checkQrStickedView = v.findViewById(R.id.checkAllStickedView);
        checkQrStickedTitle = v.findViewById(R.id.stickLabel);
        checkQrSticked = v.findViewById(R.id.checkAllSticked);
        checkQrStickedView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkQrSticked.setChecked(true);
            }
        });
    }

    /**
     * Mise en place des vues de la partie SecondScan
     */
    public void secondScanCardView() {
        secondScanView = v.findViewById(R.id.secondScanView);
        secondScanTitle = v.findViewById(R.id.secondscanLabel);
        secondQrCodeScan = v.findViewById(R.id.secondQrScanned);
        secondManualScan = v.findViewById(R.id.btn_scscan);
        numPadLastCode = v.findViewById(R.id.numpad_lastcode);
    }

    public void initHandlerThread() {
        HandlerThread handlerThread = new HandlerThread("pakpak");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    /**
     * Mise en place des listeners sur l'Activity
     * Ici : Bouton de lancement de la signature
     */
    public void listeners() {
        firstScanButtonListeners();
        checkQrStickedListeners();
        secondScanButtonListeners();

        /* MODIFICATION DU WEEKEND */
        beginAllScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (handlerThread != null && handlerThread.isAlive()) {
                    handlerThread.interrupt();
                }
                Navigation.findNavController(v).navigate(R.id.pickup_all_scan);

            }
        });
        /* MODIFICATION DU WEEKEND */
        beginAllScan.setClickable(false);

    }

    /**
     * Gestion des listenners de la partie FirstScan
     */
    public void firstScanButtonListeners() {
        firstManualScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sm.startScan();
            }
        });
        numPadFirstCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                        mActivity);

                final EditText saySomething = new EditText(mActivity);
                saySomething.setInputType(InputType.TYPE_CLASS_NUMBER);
                sayWindows.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "OK");
                                String mString = saySomething.getText().toString();
                                if(!mString.matches("^[0-9]+$"))
                                    Toast.makeText(mActivity, "N'entrez que des nombres", Toast.LENGTH_SHORT).show();
                                else
                                    qrScanned(saySomething.getText().toString());
                                // Your checkin() method
                            }
                        });

                sayWindows.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "Cancel");
                            }
                        });

                sayWindows.setView(saySomething);
                sayWindows.create().show();
            }
        });

    }

    /**
     * Gestion des listenners de la partie QR Collés
     */
    public void checkQrStickedListeners() {
        checkQrSticked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    approuveView(2);
                    activateView(3);
                }
            }
        });
    }

    /**
     * Gestion des listenners de la partie SecondScan
     */
    public void secondScanButtonListeners() {
        secondManualScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sm.startScan();
            }
        });
        numPadLastCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                        mActivity);

                final EditText saySomething = new EditText(mActivity);
                saySomething.setInputType(InputType.TYPE_CLASS_NUMBER);
                sayWindows.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "OK");
                                String mString = saySomething.getText().toString();
                                if(!mString.matches("^[0-9]+$"))
                                    Toast.makeText(mActivity, "N'entrez que des nombres", Toast.LENGTH_SHORT).show();
                                else
                                    qrScanned(saySomething.getText().toString());
                                // Your checkin() method
                            }
                        });

                sayWindows.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "Cancel");
                            }
                        });

                sayWindows.setView(saySomething);
                sayWindows.create().show();
            }
        });
    }

    /**
     * Méthode permettant d'activer les views
     * @param view
     */
    public void activateView(int view) {
        switch (view) {
            case 1: {
                firstScanView.setCardBackgroundColor(Color.WHITE);
                firstScanTitle.setTextColor(Color.WHITE);
                firstQrCodeScan.setTextColor(Color.LTGRAY);
                firstManualScan.setEnabled(true);
                firstManualScan.setBackgroundResource(R.color.BoxyBBRed);
                numPadFirstCode.setEnabled(true);
                numPadFirstCode.setColorFilter(Color.GREEN);

                activeView = view;
                break;
            }
            case 2: {
                checkQrStickedView.setCardBackgroundColor(Color.WHITE);
                checkQrStickedTitle.setTextColor(Color.WHITE);
                checkQrSticked.setTextColor(Color.BLACK);
                checkQrSticked.setEnabled(true);
                activeView = view;
                break;
            }
            case 3: {
                //activateScan();
                secondScanView.setCardBackgroundColor(Color.WHITE);
                secondScanTitle.setTextColor(Color.WHITE);
                secondQrCodeScan.setTextColor(Color.LTGRAY);
                secondManualScan.setEnabled(true);
                secondManualScan.setBackgroundResource(R.color.BoxyBBRed);
                numPadLastCode.setEnabled(true);
                numPadLastCode.setColorFilter(Color.GREEN);
                activeView = view;
                break;
            }
        }
        activeView = view;
    }

    /**
     * Méthode permettant de désactiver les views
     * @param view
     */
    public void desactivateView(int view) {
        switch (view) {
            case 1: {
                firstScanView.setCardBackgroundColor(Color.LTGRAY);
                firstScanTitle.setTextColor(Color.GRAY);
                firstQrCodeScan.setTextColor(Color.GRAY);
                firstManualScan.setEnabled(false);
                firstManualScan.setBackgroundResource(R.color.BoxiGrey);
                numPadFirstCode.setEnabled(false);
                numPadFirstCode.setColorFilter(Color.GRAY);
                if (sm != null) {
                    sm.stopScan();
                }
                break;
            }
            case 2: {
                checkQrStickedView.setCardBackgroundColor(Color.LTGRAY);
                checkQrStickedTitle.setTextColor(Color.GRAY);
                checkQrSticked.setTextColor(Color.GRAY);
                checkQrSticked.setEnabled(false);
                break;
            }
            case 3: {
                secondScanView.setCardBackgroundColor(Color.LTGRAY);
                secondScanTitle.setTextColor(Color.GRAY);
                secondQrCodeScan.setTextColor(Color.GRAY);
                secondManualScan.setEnabled(false);
                secondManualScan.setBackgroundResource(R.color.BoxiGrey);
                numPadLastCode.setEnabled(false);
                numPadLastCode.setColorFilter(Color.GRAY);
                if (sm != null) {
                    sm.stopScan();
                }
                break;
            }
        }
        if (activeView > 0)
            activeView = view - 1;
    }

    public void approuveView(int view) {
        switch (view) {
            case 1: {
                firstScanView.setCardBackgroundColor(Color.GREEN);
                firstScanTitle.setTextColor(Color.WHITE);
                firstQrCodeScan.setTextColor(Color.BLACK);
                firstManualScan.setVisibility(View.GONE);
                numPadFirstCode.setEnabled(false);
                numPadFirstCode.setColorFilter(Color.GRAY);
                if (sm != null) {
                    sm.stopScan();
                }
                break;
            }
            case 2: {
                checkQrStickedView.setCardBackgroundColor(Color.GREEN);
                checkQrStickedTitle.setTextColor(Color.WHITE);
                checkQrSticked.setTextColor(Color.BLACK);
                checkQrSticked.setEnabled(false);
                break;
            }
            case 3: {
                secondScanView.setCardBackgroundColor(Color.GREEN);
                secondScanTitle.setTextColor(Color.WHITE);
                secondQrCodeScan.setTextColor(Color.BLACK);
                secondManualScan.setVisibility(View.GONE);
                numPadLastCode.setEnabled(false);
                numPadLastCode.setColorFilter(Color.GRAY);
                if (sm != null) {
                    sm.stopScan();
                }
                /* MODIFICATION DU WEEKEND */
                beginAllScan.setClickable(true);
                break;
            }
        }
        if (activeView <= 3)
            activeView = view + 1;
    }

    public void approuveAllViews() {
        approuveView(1);
        checkQrSticked.setChecked(true);
        approuveView(2);
        approuveView(3);
    }

    /**
     * Affichage dun toast d'erreur lors du scan
     * @param Errors
     */
    public void throwErrors(String Errors) {
        Toast.makeText(mActivity, Errors, Toast.LENGTH_SHORT).show();
        viewModel.clearError();
    }

    /**
     * Méthode appelée dès que le fragment s'attache à la vue
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    private void setupScan()
    {
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte[] barocode = intent.getByteArrayExtra("barocode");
                int barocodelen = intent.getIntExtra("length", 0);
                byte temp = intent.getByteExtra("barcodeType", (byte) 0);
                byte[] aimid = intent.getByteArrayExtra("aimid");
                barcodeStr = new String(barocode, 0, barocodelen);
                Log.e(TAG,barcodeStr);
                qrScanned(extractQrCode(barcodeStr));
                Log.e(TAG,extractQrCode(barcodeStr));
                sm.stopScan();
            }
        };
        mActivity.registerReceiver(br, new IntentFilter(SCAN_ACTION));
    }

    /**
     * Gestion du scan d'un QR code
     * @param qr QR Code
     */
    private void qrScanned(String qr) {
        Log.e(TAG,"Methode de gestio du QR code FirstLast");
        isValid = false;
        viewModel.scanIsUsed();
        if (qr == NO_QR_SCANNED) {
            MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.wrong);
            mp.start();
            throwErrors(NO_QR_SCANNED);
            return;
        }
        if (activeView == 1) {
            viewModel.isFirstScanValid(qr);
        } else if (activeView == 3) {
            viewModel.isSecondScanValid(qr);
        } else {
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.wrong);
            mp.start();
            throwErrors("Vous n'avez pas beson de scanner pour cette partie");
        }
        if (viewModel.getErrors() != "") {
            isValid = false;
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.wrong);
            mp.start();
            if (activeView == 1) {
                firstQrCodeScan.setText(qr);
                firstQrCodeScan.setTextColor(Color.RED);
            } else if (activeView == 3) {
                secondQrCodeScan.setText(qr);
                secondQrCodeScan.setTextColor(Color.RED);
            }
            throwErrors(viewModel.getErrors());
        } else {
            isValid = true;
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.cool);
            mp.start();
            if (activeView == 1) {
                firstQrCodeScan.setText(qr);
                firstQrCodeScan.setTextColor(Color.GREEN);

                viewModel.pushFirstLastQrCode().observe(getActivity(), new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        Toast t = Toast.makeText(getActivity(), "Le QR Code existe déjà dans la DB", Toast.LENGTH_SHORT);
                        t.show();
                        if (aBoolean) {
                            t.cancel();
                            firstManualScan.setVisibility(View.GONE);
                            approuveView(1);
                            activateView(2);
                        } else {
                            firstQrCodeScan.setTextColor(Color.RED);
                            isValid = false;
                        }
                    }
                });
                viewModel.scanIsNotUsed();

            } else if (activeView == 3) {
                secondQrCodeScan.setText(qr);
                secondQrCodeScan.setTextColor(Color.GREEN);
                viewModel.pushFirstLastQrCode().observe(getActivity(), new Observer<Boolean>() {
                    @Override
                    public void onChanged(Boolean aBoolean) {
                        Toast t = Toast.makeText(getActivity(), "Le QR Code existe déjà dans la DB", Toast.LENGTH_SHORT);
                        t.show();
                        if (aBoolean) {
                            approuveView(3);
                            secondManualScan.setVisibility(View.GONE);
                            t.cancel();
                        } else {
                            secondQrCodeScan.setTextColor(Color.RED);
                            isValid = false;
                        }

                    }
                });
                viewModel.scanIsNotUsed();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.unregisterReceiver(br);
    }

    /**
     * Le fragment commence à interragir avec l'utilisateur
     */
    @Override
    public void onResume() {
        super.onResume();
        sm.setOutScanMode(0);
        sm.openScan();
        setupScan();
    }

}
