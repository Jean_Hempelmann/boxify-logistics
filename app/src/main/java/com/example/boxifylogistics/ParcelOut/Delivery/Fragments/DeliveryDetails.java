package com.example.boxifylogistics.ParcelOut.Delivery.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.boxifylogistics.MainActivity;
import com.example.boxifylogistics.ParcelOut.Delivery.adapters.ItemAdapter;
import com.example.boxifylogistics.R;
import com.example.boxifylogistics.Response.LoginResponse;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.example.boxifylogistics.Utils.Utils;
import com.example.boxifylogistics.model.Delivery;
import com.example.boxifylogistics.model.Item;
import com.example.boxifylogistics.ViewModel.DeliveryViewModel;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.boxifylogistics.ParcelOut.Pickup.PickUpActivity.extractQrCode;

public class DeliveryDetails extends Fragment {

    String TAG = "DeliveryDetails";
    private final String NO_QR_SCANNED = "Pas de QR code scanné";
    private DeliveryViewModel viewModel;
    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    private View v;
    private ScanDevice sm = new ScanDevice();
    private String barcodeStr;
    final static String SCAN_ACTION = "scan.rcv.message";
    private BroadcastReceiver br;

    //Headers
    private TextView deliveryDate;
    private TextView deliveryHour;
    private TextView type;

    //Customer informations
    private TextView arrow;
    private TextView customerIdentity;
    private TextView addressStreet;
    private TextView addressCityZipcode;
    private TextView addressCountry;
    private TextView phoneNumber;
    private TextView hasParking;
    private TextView hasLift;
    private TextView volume;
    private RelativeLayout rl;
    private CardView deliveryInformations;
    private TextView lfe;

    //Scanner
    private TextView numPad;
    private Button scannerManually;
    private AtomicBoolean isScanning;
    private Handler handler;

    private TextView scQrCodeLabel;
    private TextView scItemNameLabel;
    private TextView scVolumeLabel;


    //Items Lists
    private NestedScrollView nestedScrollView;
    private RecyclerView itemsListView;
    private ItemAdapter itemAdapter;
    private TextView collexp;
    private TextView nbLoaded;
    private TextView volumeVal;
    private TabLayout tabLayout;

    //Navigation
    private Button beginNavigation;

    private TextView carCheck;
    private GridLayout carGrid;

    private TextView matCheck;
    private GridLayout matGrid;

    //Deliver
    private Button toDelivering;

    private Activity mActivity;
    Thread handlerThread;

    private String tabShowed;

    /**
     * Méthode appelée dès que le Fragment s'attache à son conteneur
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity)
            mActivity = (Activity) context;
    }

    /**
     * Méthode appelée à la création de la View (= après onAttach)
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.delivery_details, container, false);
        itemAdapter = new ItemAdapter(getContext());
        // On mets en place toutes les vues
        bindCardViews();
        // Mise en place des listeners sur l'Activity (Ici : bouton "Lancer la delivery")
        listeners();
        // Gestion du pressage de la gachette du PDA
        initHandlerThread();
        // Initiation du handler (tâche de fond)
        isScanning = new AtomicBoolean(false);

        // Récupération de la date, de l'heure et du type (Ici : delivery)
        deliveryDate = v.findViewById(R.id.parcel_date);
        deliveryHour = v.findViewById(R.id.parcel_hour);
        type = v.findViewById(R.id.type);

        return v;
    }

    /**
     * Récupère juste le numéro du QR Code
     * @param url
     * @return
     */
    private String extractQrCode(String url){
        return url.substring(url.lastIndexOf("/") + 1);
    }

    /**
     * Méthode appelée après que l'activité soit créée
     */
    @Override
    public void onStart() {
        super.onStart();

        tabShowed="Tous";
        viewModel.getDelivery().observe(getActivity(), new Observer<Delivery>() {
            @Override
            public void onChanged(Delivery delivery) {
                CardViewDeliveryInformation(delivery);
                itemAdapter.setItems(delivery.getItems());
                itemAdapter.isAll();
            }
        });
    }

    /**
     * Mise en place des vues
     */
    public void bindCardViews() {
        // Mise en place de la vue "Information sur le client, adresse et infomrations complémentaires"
        bindCardViewDeliveryInformation();
        //Mise en place de la vue "Scanner + Entrer QR à la main + QR venant d'être scanné"
        bindCardViewScanner();
        // Mise en place de la NestedScrollView contenant la RecyclerView de la liste des items
        bindCardViewItemsList();
        // Mise en place de la vue "Check du véhicule/matériel + Lancement de la navigation
        bindCardViewNavigation();
        // Bouton de lancement de la delivery
        toDelivering = v.findViewById(R.id.begin_delivery);
    }

    /**
     * Mise en place de la vue "Information sur le client, adresse et informations complémentaires"
     */
    public void bindCardViewDeliveryInformation() {

        customerIdentity = v.findViewById(R.id.customer_identity);
        addressStreet = v.findViewById(R.id.address_street);
        addressCityZipcode = v.findViewById(R.id.address_city_zipcode);
        addressCountry = v.findViewById(R.id.address_country);
        phoneNumber = v.findViewById(R.id.customer_phonenbr);
        rl = v.findViewById(R.id.complementary_informations_body);
        lfe = v.findViewById(R.id.lfe);
        deliveryInformations = v.findViewById(R.id.delivery_first_informations);
        hasParking = v.findViewById(R.id.has_parking);
        hasLift = v.findViewById(R.id.has_lift);
        volume = v.findViewById(R.id.volume);

        arrow = v.findViewById(R.id.expander);
        v.findViewById(R.id.complementary_informations).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl.getVisibility() == View.GONE) {
                    rl.setVisibility(View.VISIBLE);
                    arrow.setBackgroundResource(R.drawable.ic_expand_less_black_24dp);
                } else {
                    rl.setVisibility(View.GONE);
                    arrow.setBackgroundResource(R.drawable.ic_expand_more_black_24dp);
                }
            }
        });

    }

    /**
     * Mise en place de la vue "Scanner + Entrer QR à la main + QR venant d'être scanné"
     */
    public void bindCardViewScanner() {
        numPad = v.findViewById(R.id.numpad);
        scannerManually = v.findViewById(R.id.btn_scan);
        scQrCodeLabel = v.findViewById(R.id.scqrcode_label);
        scItemNameLabel = v.findViewById(R.id.scitemname_label);
        scVolumeLabel = v.findViewById(R.id.scvolume_label);
    }

    /**
     * Mise en place de la NestedScrollView contenant la RecyclerView de la liste des items
     */
    public void bindCardViewItemsList() {
        itemsListView = v.findViewById(R.id.item_list);
        nbLoaded = v.findViewById(R.id.nb_loaded);
        tabLayout = v.findViewById(R.id.tabLayout);
        collexp = v.findViewById(R.id.expcoll);
        nestedScrollView = v.findViewById(R.id.scrv_item_list);
        volumeVal = v.findViewById(R.id.volume_val);
    }

    /**
     * Mise en place de la vue "Check du véhicule/matériel + Lancement de la navigation"
     */
    public void bindCardViewNavigation() {
        beginNavigation = v.findViewById(R.id.btn_begin_nav);

        carGrid = v.findViewById(R.id.car_grid);
        matGrid = v.findViewById(R.id.mat_grid);

        matCheck = v.findViewById(R.id.mat_check);
        carCheck = v.findViewById(R.id.car_check);

    }

    /**
     * Initiation du handler (tâche de fond)
     */
    public void initHandlerThread() {
        HandlerThread handlerThread = new HandlerThread("");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    /**
     * Méthode appelée une fois l'activité créée (= après onCreateView)
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(DeliveryViewModel.class);
        tabLayout = new TabLayout(getActivity());
        itemsListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        itemsListView.setHasFixedSize(true);
        itemsListView.setAdapter(itemAdapter);
        mActivity = getActivity();
        navigationListeners();
        populateGridLayouts();
        viewModel.getDelivery().observe(getActivity(), new Observer<Delivery>() {
            @Override
            public void onChanged(Delivery delivery) {
                CardViewDeliveryInformation(delivery);
            }
        });

        /* GESTION DES LISTES D'ITEMS */
        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setItems(items);
                itemAdapter.isAll();
            }
        });
        viewModel.getItemDelivered().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setGreen(items);
            }
        });
        viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setBlue(items);
            }
        });
        viewModel.getItemToLoad().observe(getActivity(), new Observer<ArrayList<Item>>() {
            @Override
            public void onChanged(ArrayList<Item> items) {
                itemAdapter.setRed(items);
            }
        });
        /* GESTION DU BOUTON DE LANCEMENT DE NAVIGATION */
        viewModel.canNavigate().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    beginNavigation.setEnabled(true);
                    beginNavigation.setBackgroundResource(R.color.BoxyBBRed);
                    beginNavigation.setTextColor(Color.WHITE);
                } else {
                    beginNavigation.setEnabled(false);
                    beginNavigation.setBackgroundResource(R.color.BoxiGrey);
                    beginNavigation.setTextColor(Color.BLACK);
                }
            }
        });
        /* GESTION DE L'AFFICHAGE DU NOMBRE D'OBJET CHARGES/RESTANT */
        viewModel.getNbLoadedItems().observe(getActivity(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (viewModel.getNbNotLoadedItems().getValue() != 0){
                    int total = viewModel.getNbLoadedItems().getValue()+viewModel.getNbNotLoadedItems().getValue();
                    nbLoaded.setText(integer + "/" + total);
                }
                else
                    nbLoaded.setText("Tous chargés !");
            }
        });
        /* GESTION DU BOUTON DE LANCEMENT DE NAVIGATION*/
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (handlerThread != null && handlerThread.isAlive()) {
                    handlerThread.interrupt();
                }
                Intent intent = new Intent(mActivity, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("ymd", viewModel.getDate());
                intent.putExtras(bundle);
                mActivity.finish();
                startActivity(intent);
                //deactivateScan();
            }
        });
    }

    /**
     * Méthode de population de la liste des vérifications
     */
    public void populateGridLayouts() {
        carGrid.setColumnCount(1);
        for (String d : viewModel.getCarCheck()) {
            final CheckBox a = new CheckBox(getActivity());
            a.setText(d);
            if (d == "Permis de conduire" || d == "Validité assurance") {
                a.setTextColor(Color.parseColor("#cc003d"));
            }
            a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if (a.getText() == "Permis de conduire" || a.getText() == "Validité assurance") {
                            a.setTextColor(Color.parseColor("#00cc33"));
                        }
                        viewModel.addCheck(buttonView.getText().toString());
                    } else {
                        if (a.getText() == "Permis de conduire" || a.getText() == "Validité assurance") {
                            a.setTextColor(Color.parseColor("#cc003d"));
                        }
                        viewModel.removeCheck(buttonView.getText().toString());
                    }
                }
            });
            carGrid.addView(a);
        }
        matGrid.setColumnCount(1);
        /* Listener sur les CheckBox */
        for (String d : viewModel.getMaterialCheck()) {
            CheckBox a = new CheckBox(getActivity());
            a.setText(d);
            a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                        viewModel.addCheck(buttonView.getText().toString());
                    else
                        viewModel.removeCheck(buttonView.getText().toString());
                }
            });
            matGrid.addView(a);
        }
    }

    /**
     * Remplissage de la CardView de la delivery
     * @param d
     */
    public void CardViewDeliveryInformation(Delivery d) {
        String identity = d.getUser().getCustomer_id() + " " + d.getUser().getFull_name() + "(" + d.getUser().getLanguage() + ")";
        String street = d.getAddress().getNumber() + " " + Utils.capitalizeWord(d.getAddress().getStreet());
        String cipCode = d.getAddress().getZipcode() + " " + d.getAddress().getCity();
        String phonnbr = d.getUser().getPhone() == null ? d.getUser().getPhone() : "NC";

        customerIdentity.setText(identity);
        addressStreet.setText(street);
        addressCityZipcode.setText(cipCode);
        addressCountry.setText("Belgique");
        phoneNumber.setText(phonnbr);
        hasParking.setText(d.getAppointment().isHas_parking() ? "Oui" : "Non");
        hasLift.setText(d.getAppointment().isHas_lift() ? "Oui" : "Non");
        volume.setText(d.getAppointment().getVolume() + "m³");
        volumeVal.setText(d.getAppointment().getVolume() + "m³");

        deliveryHour.setText(d.getAppointment().getHourAppoint());
        deliveryDate.setText(d.getAppointment().getDayAppoint());
        type.setText("Delivery");


    }

    /**
     * Mise en place des listeners sur l'Activity
     * Ici : bouton "Lancer la delivery"
     */
    public void listeners() {
        itemListListener();
        manualScanListener();
        toDelivering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (!viewModel.isAllLoaded()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setMessage("Il vous manque " + viewModel.getNbNotLoadedItems().getValue() + " objets a scanner\nÊtes vous sur de vouloir continuer ?")
                            .setCancelable(false)
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.cancel();

                                }
                            })
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (handlerThread != null && handlerThread.isAlive()) {
                                        handlerThread.interrupt();
                                    }
                                    sm.stopScan();
                                    sendCheckedCheckbox();

                                }
                            })
                    ;
                    //Creating dialog box

                    AlertDialog alert = builder.create();
                    //Setting the title manually

                    alert.setTitle("Attention");
                    alert.show();

                } else {
                    if (handlerThread != null && handlerThread.isAlive()) {
                        handlerThread.interrupt();
                    }
                    sm.stopScan();
                    sendCheckedCheckbox();
                }

            }
        });

    }

    /**
     * Gestion des listenners sur la View de navigation
     */
    public void navigationListeners() {
        beginNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean[] canbegin = {false};
                viewModel.setGpsTracker(getActivity());
                // Dans le cas où tous les objets sont chargés et que toutes les checkBox sont cochées
                if(viewModel.getNbNotLoadedItems().getValue() != 0 && !viewModel.getIsAllChecked()){
                    final String Message2 = "Il vous manque " + viewModel.getNbNotLoadedItems().getValue() + " objets a scanner\nÊtes-vous sûr de vouloir continuer?";
                    final String Title2 = "Attention !";
                    final DialogInterface.OnClickListener plk = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            carGrid.setVisibility(View.GONE);
                            matGrid.setVisibility(View.GONE);
                            String url = viewModel.getUrlForNavigation();
                            Log.e(TAG,url);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);
                        }
                    };
                    final String Message = "Vous n'avez pas tout coché \nÊtes-vous sûr de vouloir continuer ?";
                    final String Title = "Attention !";
                    DialogInterface.OnClickListener nc = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DialogShow(Title2,Message2,plk);
                        }
                    };
                    DialogShow(Title,Message,nc);
                }
                /* Si toutes les CheckBox n'ont pas été cochées */
                else if (!viewModel.getIsAllChecked()) {
                    String Message = "Vous n'avez pas tout cocher \n Êtes vous sur de voulour continuer ?";
                    String Title = "Attention";
                    DialogInterface.OnClickListener plk = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            carGrid.setVisibility(View.GONE);
                            matGrid.setVisibility(View.GONE);
                            String url = viewModel.getUrlForNavigation();
                            Log.e(TAG,url);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);
                        }
                    };
                    DialogShow(Title,Message,plk);
                }
                /* Si toutes les objets n'ont pas été chargés */
                else if (viewModel.getNbNotLoadedItems().getValue() != 0) {
                    String Message = "Il vous manque " + viewModel.getNbNotLoadedItems().getValue() + " objets a scanner\n Êtes vous sur de vouloir continuer?";
                    String Title = "Attention";
                    DialogInterface.OnClickListener plk = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            carGrid.setVisibility(View.GONE);
                            matGrid.setVisibility(View.GONE);
                            String url = viewModel.getUrlForNavigation();
                            Log.e(TAG,url);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);
                        }
                    };
                    DialogShow(Title,Message,plk);
                }
                else
                {
                    carGrid.setVisibility(View.GONE);
                    matGrid.setVisibility(View.GONE);
                    String url = viewModel.getUrlForNavigation();
                    Log.e(TAG,url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }



            }
        });
    }

    /**
     * Gestion de l'affichage de la liste d'items
     */
    public void itemListListener() {
        collexp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final float scale = getResources().getDisplayMetrics().density;
                if (collexp.getText() == "Étendre") {
                    nestedScrollView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    collexp.setText("Réduire");
                } else {
                    nestedScrollView.getLayoutParams().height = (int) (150 * scale);
                    collexp.setText("Étendre");
                }

            }
        });
        tabListeners();
    }

    /**
     * Gestion de la partie onglet de la liste d'Item
     */
    public void tabListeners() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        tabShowed = "Tous";
                        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setGreen(viewModel.getItemDelivered().getValue());
                                itemAdapter.setRed(viewModel.getItemToLoad().getValue());
                                itemAdapter.setBlue(viewModel.getItemLoaded().getValue());
                                itemAdapter.isAll();
                            }
                        });
                        break;
                    case "Scannés":
                        tabShowed = "Scannés";
                        viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setBlue(items);
                                itemAdapter.isNotAll();
                            }
                        });
                        break;
                    case "À scanner":
                        tabShowed = "À scanner";
                        viewModel.getItemToLoad().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setRed(items);
                                itemAdapter.isNotAll();
                            }
                        });
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(new ArrayList<Item>());
                                return;
                            }
                        });
                        break;
                    case "Scannés":
                        viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(new ArrayList<Item>());
                                return;
                            }
                        });
                        break;
                    case "À scanner":
                        viewModel.getItemToLoad().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(new ArrayList<Item>());
                                return;
                            }
                        });
                        break;
                }


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getText().toString()) {
                    case "Tous":
                        tabShowed = "Tous";
                        viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setGreen(viewModel.getItemDelivered().getValue());
                                itemAdapter.setRed(viewModel.getItemToLoad().getValue());
                                itemAdapter.setBlue(viewModel.getItemLoaded().getValue());
                                itemAdapter.isAll();
                            }
                        });
                        break;
                    case "Scannés":
                        tabShowed = "Scannés";
                        viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setBlue(items);
                                itemAdapter.isNotAll();
                            }
                        });
                        break;
                    case "À scanner":
                        tabShowed = "À scanner";
                        viewModel.getItemToLoad().observe(getActivity(), new Observer<ArrayList<Item>>() {
                            @Override
                            public void onChanged(ArrayList<Item> items) {
                                itemAdapter.setItems(items);
                                itemAdapter.setRed(items);
                                itemAdapter.isNotAll();
                            }
                        });
                        break;
                }

            }
        });
    }

    /**
     * Gestion du scan manuel (sans gachette, ou entré à la main)
     */
    public void manualScanListener() {
        /* Scanner sans gachette */
        scannerManually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sm.startScan();
            }
        });
        /* Entrer QRCode avec le pad */
        numPad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                        mActivity);

                final EditText saySomething = new EditText(mActivity);
                saySomething.setInputType(InputType.TYPE_CLASS_NUMBER);
                sayWindows.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "OK");
                                String mString = saySomething.getText().toString();
                                if (!mString.matches("^[0-9]+$"))
                                    Toast.makeText(mActivity, "Only numbers", Toast.LENGTH_SHORT).show();
                                else
                                    qrScanned(saySomething.getText().toString());
                                // Your checkin() method
                            }
                        });

                sayWindows.setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i(TAG, "Cancel");
                            }
                        });

                sayWindows.setView(saySomething);
                sayWindows.create().show();
            }
        });
    }

    /**
     * Gestion du scan d'un QR code
     * @param qr
     */
    public void qrScanned(String qr) {
        Log.e(TAG, "qrScanned: " + qr);
        if (qr == NO_QR_SCANNED) {
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.wrong);
            mp.start();
            throwErrors(NO_QR_SCANNED);
            return;
        }
        Item it = viewModel.qrLoaded(qr);
        if (viewModel.getErrors() == "" && it != null){
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.cool);
            mp.start();
            setItemScanned(it);
        }

        else{
            MediaPlayer mp = MediaPlayer.create(mActivity, R.raw.wrong);
            mp.start();
            throwErrors(viewModel.getErrors());
        }


        if(tabShowed.equals("Tous")){
            viewModel.getAllItems().observe(getActivity(), new Observer<ArrayList<Item>>() {
                @Override
                public void onChanged(ArrayList<Item> items) {
                    itemAdapter.setItems(items);
                    itemAdapter.setGreen(viewModel.getItemDelivered().getValue());
                    itemAdapter.setRed(viewModel.getItemToLoad().getValue());
                    itemAdapter.setBlue(viewModel.getItemLoaded().getValue());
                    itemAdapter.isAll();
                }
            });
        }else if(tabShowed.equals("Scannés")){
            viewModel.getItemLoaded().observe(getActivity(), new Observer<ArrayList<Item>>() {
                @Override
                public void onChanged(ArrayList<Item> items) {
                    itemAdapter.setItems(items);
                    itemAdapter.setBlue(items);
                    itemAdapter.isNotAll();
                }
            });
        }else if(tabShowed.equals("À scanner")){
            viewModel.getItemToLoad().observe(getActivity(), new Observer<ArrayList<Item>>() {
                @Override
                public void onChanged(ArrayList<Item> items) {
                    itemAdapter.setItems(items);
                    itemAdapter.setRed(items);
                    itemAdapter.isNotAll();
                }
            });
        }

    }

    /**
     * Gestion au scan d'un item
     * @param it
     */
    public void setItemScanned(Item it) {
        scQrCodeLabel.setText(Integer.toString(it.getBoxify_id()));
        scQrCodeLabel.setTextColor(getResources().getColor(R.color.green));
        scItemNameLabel.setText(it.getName());
        scItemNameLabel.setTextColor(getResources().getColor(R.color.green));
        scVolumeLabel.setText(it.getVolume() + "m³");
        scVolumeLabel.setTextColor(getResources().getColor(R.color.green));
        String f = it.is_fragile()? "F":"";
        String e = it.is_large()? "E":"";
        lfe.setText(f+e);
        lfe.setTextColor(Color.WHITE);

    }

    /**
     * Affichage dun toast d'erreur lors du scan
     * @param Errors
     */
    public void throwErrors(String Errors) {
        Toast.makeText(getActivity(), Errors, Toast.LENGTH_SHORT).show();
        viewModel.clearErrors();
    }

    /**
     * Gestion de l'affichage des dialogue
     * @param title
     * @param Message
     * @param clk
     */
    public void DialogShow(String title,String Message,DialogInterface.OnClickListener clk){
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(Message)
                .setCancelable(false)
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();


                    }
                })
                .setPositiveButton("Oui", clk)
        ;
        builder.setTitle(title);
        builder.create().show();
    }

    private void setupScan()
    {
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte[] barocode = intent.getByteArrayExtra("barocode");
                int barocodelen = intent.getIntExtra("length", 0);
                byte temp = intent.getByteExtra("barcodeType", (byte) 0);
                byte[] aimid = intent.getByteArrayExtra("aimid");
                barcodeStr = new String(barocode, 0, barocodelen);
                Log.e(TAG,barcodeStr);
                qrScanned(extractQrCode(barcodeStr));
                Log.e(TAG,extractQrCode(barcodeStr));
                sm.stopScan();
            }
        };
        mActivity.registerReceiver(br, new IntentFilter(SCAN_ACTION));
    }

    public void sendCheckedCheckbox(){
        for (final String d : viewModel.getCheckupChecked()) {
            Log.e(TAG, d);
        }
        JSONArray jsCheckedArray = new JSONArray(viewModel.getCheckupChecked());
        apiRequest.sendCheckedCheckbox(GetAndSetToken.getBearerWithToken(),jsCheckedArray).enqueue(new Callback<JsonObject>(){
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                // Si on reçoit un "OK"
                if(response.code() == 200){
                    Navigation.findNavController(v).navigate(R.id.delivering);
                }
                else{
                    Toast.makeText(getActivity(),"Les vérifications ont échouées",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getActivity(),"Les vérifications ont échouées\nVérifier la connexion internet",Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mActivity.unregisterReceiver(br);
    }

    /**
     * Le fragment commence à interragir avec l'utilisateur
     */
    @Override
    public void onResume() {
        super.onResume();
        sm.setOutScanMode(0);
        sm.openScan();
        setupScan();
    }
}
