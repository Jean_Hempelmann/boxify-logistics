package com.example.boxifylogistics.model;

public class Parcel {
    protected Appointement Appointment;
    protected com.example.boxifylogistics.model.User User;
    protected com.example.boxifylogistics.model.Address Address;

    public com.example.boxifylogistics.model.Address getAddress() {
        return Address;
    }
    public int getId(){
        return getAppointment().getAppointment_id();
    }

    public Appointement getAppointment() {
        return Appointment;
    }


    public com.example.boxifylogistics.model.User getUser() {
        return User;
    }

    public String getTypeString(){
        return getAppointment().getTypeString();
    }
    public Appointement.TypeAppointment getType(){
        return getAppointment().getType();
    }




    @Override
    public String toString() {
        return "Package{" +
                "Appointement=" + Appointment +
                ", User=" + User +
                ", Address=" + Address +
                '}';
    }
}
