package com.example.boxifylogistics.model;

public class Customer extends User {
    private int idCustomer;
    private String address;
    private String postalCode;
    private String city;
    private String nbrPhone;
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Customer(String address, String postalCode, String city, String nbrPhone) {
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.nbrPhone = nbrPhone;
    }
    public Customer(){}

    public int getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(int idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNbrPhone() {
        return nbrPhone;
    }

    public void setNbrPhone(String nbrPhone) {
        this.nbrPhone = nbrPhone;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "idCustomer=" + idCustomer +
                ", address='" + address + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", city='" + city + '\'' +
                ", nbrPhone='" + nbrPhone + '\'' +
                ", Name='" + Name + '\'' +
                '}';
    }
}
