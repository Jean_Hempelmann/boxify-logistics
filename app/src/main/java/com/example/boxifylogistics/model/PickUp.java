package com.example.boxifylogistics.model;

import java.util.ArrayList;

public class PickUp extends Parcel {

    private int firstQrCode;
    private int secondQrCode;
    private int nb_total_items;
    private ArrayList<String> allItems = new ArrayList<>();
    private ArrayList<String> itemsLoaded = new ArrayList<>();
    private ArrayList<String> itemsNotLoaded = new ArrayList<>();

    public PickUp(Appointement appointment, com.example.boxifylogistics.model.User user, com.example.boxifylogistics.model.Address address) {
        super();
        super.Appointment = appointment;
        super.User = user;
        super.Address = address;
    }
}

