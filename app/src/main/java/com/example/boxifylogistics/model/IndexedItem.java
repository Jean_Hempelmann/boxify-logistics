package com.example.boxifylogistics.model;

import android.graphics.Bitmap;

import java.io.File;

public class IndexedItem {
    private int item_id;
    private int calculator_item_id;
    private int customer_id;
    private int boxify_id;
    private String name;
    private String description;
    private String status_admin;
    private String status_user;
    private double volume;
    private boolean is_picture_taken;
    private int on_pallet;
    private int is_fragile;
    private int is_large;
    private String created_at;
    private String updated_at;
    private String photoBase64;

    private File photo_file;


    public File getPhoto() {
        return photo_file;
    }

    public boolean setPhoto(File photo) {
        if(photo == null){
            return false;
        }

        this.photo_file= photo;
        return true;
    }
    private String photo;
    public String getPhotoString() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Bitmap photoBitmap() {
        return photoBitmap;
    }

    public boolean setPhotoBitmap(Bitmap photoBitmap) {
        if(photoBitmap == null){
            return false;
        }
        this.photoBitmap = photoBitmap;
        return true;
    }

    private Bitmap photoBitmap;


    public String getPhotoBase64() {
        return photoBase64;
    }

    public boolean setPhotoBase64(String photoBase64) {
        if(photoBase64=="null")
            return false;
        else{
            this.photoBase64 = photoBase64.replaceAll("\\s","");

            return true;
        }
    }

    public String getCreated_at() {
        return created_at;
    }

    public int getBoxify_id() {
        return boxify_id;
    }

    public void setBoxify_id(int boxify_id) {
        this.boxify_id = boxify_id;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public void setCalculator_item_id(int calculator_item_id) {
        this.calculator_item_id = calculator_item_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }


    public boolean setName(String name) {
        if(name.length()>=3) {
            this.name = name;
            return true;
        }else{
            return false;
        }
    }

    public boolean setDescription(String description) {
        if(description.length()>=5) {
            this.description = description;
            return true;
        }else{
            return false;
        }
    }

    public void setStatus_admin(String status_admin) {
        this.status_admin = status_admin;
    }

    public void setStatus_user(String status_user) {
        this.status_user = status_user;
    }

    public boolean setVolume(double volume) {
        if(volume>0) {
            this.volume = volume;
            return true;
        }
        else{
            return false;
        }

    }

    public void setIs_picture_taken(boolean is_picture_taken) {
        this.is_picture_taken = is_picture_taken;
    }

    public void setOn_pallet(int on_pallet) {
        this.on_pallet = on_pallet;
    }

    public void setIs_fragile(int is_fragile) {
        this.is_fragile = is_fragile;
    }

    public void setIs_large(int is_large) {
        this.is_large = is_large;
    }

    public int getItem_id() {
        return item_id;
    }

    public int getCalculator_item_id() {
        return calculator_item_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus_admin() {
        return status_admin;
    }

    public String getStatus_user() {
        return status_user;
    }

    public Double getVolume() {
        return volume;
    }

    public boolean isIs_picture_taken() {
        return is_picture_taken;
    }

    public int isOn_pallet() {
        return on_pallet;
    }

    public int isIs_fragile() {
        return is_fragile;
    }

    public int isIs_large() {
        return is_large;
    }

    @Override
    public String toString() {
        return "\nIndexedItem:{\n" +
                "item_id:" + item_id +"\n"+
                ", calculator_item_id:" + calculator_item_id +"\n"+
                ", customer_id:" + customer_id +"\n"+
                ", boxify_id:'" + boxify_id + '\'' +"\n"+
                ", name:'" + name + '\''+"\n"+
                ", description:'" + description + '\'' +"\n"+
                ", status_admin:'" + status_admin + '\'' +"\n"+
                ", status_user:'" + status_user + '\'' +"\n"+
                ", volume:" + volume +"\n"+
                ", is_picture_taken:" + is_picture_taken +"\n"+
                ", on_pallet:" + on_pallet +"\n"+
                ", is_fragile:" + is_fragile +"\n"+
                ", is_large:" + is_large +"\n"+
                ", created_at:'" + created_at + '\'' +"\n"+
                ", updated_at:'" + updated_at + '\'' +"\n"+
                ", photo_base64:'" +photoBase64+ '\'' +"\n"+
                ", photo:'" +photo_file+ '\'' +"\n"+
                '}';
    }
    
}
