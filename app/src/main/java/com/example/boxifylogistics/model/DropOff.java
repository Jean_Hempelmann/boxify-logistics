package com.example.boxifylogistics.model;

import java.util.ArrayList;

public class DropOff extends Parcel {

    private ArrayList<Item> Items = new ArrayList<>();


    public ArrayList<Item> getItems() {
        return Items;
    }
    public void setItems(ArrayList<Item> items) {
        Items = items;
    }

    public DropOff(Appointement appointment, com.example.boxifylogistics.model.User user, com.example.boxifylogistics.model.Address address) {
        super();
        super.Appointment = appointment;
        super.User = user;
        super.Address = address;
    }

    @Override
    public String toString() {
        return "Delivery{" +
                "Items=" + Items +
                ", Appointment=" + Appointment +
                ", User=" + User +
                ", Address=" + Address +
                '}';
    }
}

