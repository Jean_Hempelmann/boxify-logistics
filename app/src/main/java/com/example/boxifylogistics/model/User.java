package com.example.boxifylogistics.model;

public class User {
    private int user_id;
    private int customer_id;

    public int getCustomer_id() {
        return customer_id;
    }

    private Language language;
    private Civility civility;
    //////////////////////////
    private String phone;
    private String first_name;
    private String last_name;
    private String full_name;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getLanguage() {
        return language.toString();
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getCivility() {
        return civility.toString();
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    @Override
    public String toString() {
        return "User{" +
                "user_id=" + user_id +
                ", language=" + language +
                ", civility=" + civility +
                ", phone='" + phone + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", full_name='" + full_name + '\'' +
                '}';
    }
    protected enum Language{
        EN,NL,FR
    }
    protected enum Civility{
        M,F
    }
}

