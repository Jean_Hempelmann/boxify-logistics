package com.example.boxifylogistics.model;

import android.util.Log;

import com.example.boxifylogistics.Utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Appointement {
    private int appointment_id;
    private TypeAppointment type;
    private float duration;
    //
    private String volume;
    //
    private String date;
    private String remarks_customer;
    private String remarks_internal;
    private int rental_trucks;
    //
    private String level;
    private String status;



    private int has_parking;
    private int has_lift;
    private int at_office;

    public String getStatus(){
        return status;
    }

    public String getTypeString() {
        return Utils.capitalizeWord(type.name().replace("_","").toLowerCase());
    }

    public TypeAppointment getType(){
        return type;
    }

    public boolean isHas_lift() {
        if(has_lift == 1)
            return true;
        return false;
    }
    public int getAppointment_id() {
        return appointment_id;
    }

    public void setAppointment_id(int appointment_id) {
        this.appointment_id = appointment_id;
    }

    public TypeAppointment getTypeAppointment() {
        return type;
    }

    public void setTypeAppointment(TypeAppointment typeAppointment) {
        this.type = typeAppointment;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String s) {
        Log.d("Appointment", "setDate: "+s);
//        this.dat = date;
    }
    public String getFormattedDate(String pattern){

        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss";
        String outputPattern = pattern;
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date tmpDate = null;
        String str = null;

        try {
            tmpDate = inputFormat.parse(date);
            str = outputFormat.format(tmpDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    public String getHourAppoint(){
        return getFormattedDate("HH':'mm");
    }
    public String getDayAppoint(){
        return getFormattedDate("EEE,dd MMM yyyy");
    }

    public String getRemarks_customer() {
        return remarks_customer;
    }

    public void setRemarks_customer(String remarks_customer) {
        this.remarks_customer = remarks_customer;
    }

    public String getRemarks_internal() {
        return remarks_internal;
    }

    public void setRemarks_internal(String remarks_internal) {
        this.remarks_internal = remarks_internal;
    }

    public int getRental_trucks() {
        return rental_trucks;
    }

    public void setRental_trucks(int rental_trucks) {
        this.rental_trucks = rental_trucks;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public boolean isHas_parking() {
        if(has_parking == 1){
            return true;
        }else{
            return false;
        }
    }

    public boolean isAtOffice(){
        if(at_office == 1){
            return true;
        }else{
            return false;
        }
    }

    public void setHas_parking(int has_parking) {
        this.has_parking = has_parking;
    }

    @Override
    public String toString() {
        return "Appointement{" +
                "appointment_id=" + appointment_id +
                ", status=" + status +
                ", typeAppointment=" + type +
                ", duration=" + duration +
                ", volume=" + volume +
                ", date=" + date +
                ", remarks_customer='" + remarks_customer + '\'' +
                ", remarks_internal='" + remarks_internal + '\'' +
                ", rental_trucks=" + rental_trucks +
                ", level=" + level +
                ", has_parking=" + has_parking +
                ", at_office=" + at_office +
                '}';
    }
    public enum TypeAppointment{
        DROP_OFF,PICK_UP,DELIVERY

    }
}

