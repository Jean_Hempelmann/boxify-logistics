package com.example.boxifylogistics.model;

public class Item {
    private int calculator_item_id;
    private int customer_id;
    private String slug;
    private String volume;
    private boolean is_fragile;
    private boolean is_large;
    private boolean is_visible_residential;
    private boolean is_visible_buisness;
    private String created_at;
    private String updated_at;
    private String image;
    private int boxify_id;
    private String name;
    private String quantity;

    public boolean is_fragile() {
        return is_fragile;
    }

    public boolean is_large() {
        return is_large;
    }

    @Override
    public String toString() {
        return "Item{" +
                "calculator_item_id=" + calculator_item_id +
                ", customer_id=" + customer_id +
                ", boxify_id=" + boxify_id +
                ", name='" + name +
                ", name='" + quantity + '\'' +
                ", volume=" + volume +
                '}';
    }



    public int getCalculator_item_id() {
        return calculator_item_id;
    }

    public void setCalculator_item_id(int calculator_item_id) {
        this.calculator_item_id = calculator_item_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getBoxify_id() {
        return boxify_id;
    }

    public void setBoxify_id(int boxify_id) {
        this.boxify_id = boxify_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity= quantity;
    }

    public boolean getIsFragile(){
        return is_fragile;
    }
}
