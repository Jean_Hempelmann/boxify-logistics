package com.example.boxifylogistics.model;

import java.util.ArrayList;

public class Delivery extends Parcel {

    private ArrayList<Item> Items = new ArrayList<>();


    public ArrayList<Item> getItems() {
        return Items;
    }
    public void setItems(ArrayList<Item> items) {
        Items = items;
    }

    @Override
    public String toString() {
        return "Delivery{" +
                "Items=" + Items +
                ", Appointment=" + Appointment +
                ", User=" + User +
                ", Address=" + Address +
                '}';
    }
}
