package com.example.boxifylogistics.model;

public class ItemType {

    private int calculator_item_id;
    private int calculator_category_id;
    private String slug;
    private double volume;
    private int is_fragile;
    private int is_large;
    private String created_at;
    private String fr;
    private String image;
    private String name;

    public String getName() {
        return name;
    }

    public int getCalculator_item_id() {
        return calculator_item_id;
    }

    public void setCalculator_item_id(int calculator_item_id) {
        this.calculator_item_id = calculator_item_id;
    }

    public int getCalculator_category_id() {
        return calculator_category_id;
    }

    public void setCalculator_category_id(int calculator_category_id) {
        this.calculator_category_id = calculator_category_id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public int getIs_fragile() {
        return is_fragile;
    }

    public void setIs_fragile(int is_fragile) {
        this.is_fragile = is_fragile;
    }

    public int getIs_large() {
        return is_large;
    }

    public void setIs_large(int is_large) {
        this.is_large = is_large;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getFr() {
        return fr;
    }


    public void setFr(String fr) {
        this.fr = fr;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ItemType{" +
                "calculator_item_id=" + calculator_item_id +
                ", calculator_category_id=" + calculator_category_id +
                ", slug='" + slug + '\'' +
                ", volume=" + volume +
                ", is_fragile=" + is_fragile +
                ", is_large=" + is_large +
                ", created_at='" + created_at + '\'' +
                ", fr='" + fr + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

}
