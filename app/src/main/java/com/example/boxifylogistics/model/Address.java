package com.example.boxifylogistics.model;

import com.example.boxifylogistics.Utils.Coordinates;
import com.example.boxifylogistics.Utils.Utils;

public class Address {
    private int adress_id;
    //
    private String street;
    //
    private String number;
    private String postal_code;
    //
    private String city;
    private String latitude;
    private String longitude;

    public String getCompletedAddress(){

        String adresse = number
                + " "+ Utils.capitalizeWord(street)
                +", "+postal_code
                +" "+city;
        return adresse;

    }
    public Coordinates getCoordinates(){
        return new Coordinates(Double.valueOf(longitude),Double.valueOf(latitude));
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Address{" +
                "adress_id=" + adress_id +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", zipcode='" + postal_code + '\'' +
                ", city='" + city + '\'' +
                ", latitude='" + latitude + '\'' +
                '}';
    }

    public int getAdress_id() {
        return adress_id;
    }

    public void setAdress_id(int adress_id) {
        this.adress_id = adress_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZipcode() {
        return postal_code;
    }

    public void setZipcode(String zipcode) {
        this.postal_code = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }



}
