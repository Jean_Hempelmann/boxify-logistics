package com.example.boxifylogistics;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class PageFragment extends Fragment {

    // Déclaration des variables statiques servant d'identifiants des futures valeurs passées au Bundle.
    private static final String KEY_POSITION="position";
    //private static final String KEY_COLOR="color";


    public PageFragment() { }


    //Méthode qui retourne un fragment
    public static PageFragment newInstance(int position, int color) {

        // Création d'un fragment
        PageFragment frag = new PageFragment();

        // Création du Bundle et ajout de valeurs
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        //args.putInt(KEY_COLOR, color);
        frag.setArguments(args);

        return(frag);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // 3 - Get layout of PageFragment
        View result = inflater.inflate(R.layout.fragment_page, container, false);

        // 4 - Get widgets from layout and serialise it
        LinearLayout rootView= (LinearLayout) result.findViewById(R.id.fragment_page_rootview);
        TextView textView= (TextView) result.findViewById(R.id.fragment_page_title);

        // 5 - Get data from Bundle (created in method newInstance)
        int position = getArguments().getInt(KEY_POSITION, -1);
        //int color = getArguments().getInt(KEY_COLOR, -1);



        Log.e(getClass().getSimpleName(), "onCreateView called for fragment number "+position);

        return result;
    }

}