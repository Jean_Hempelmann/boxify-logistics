package com.example.boxifylogistics.Retrofit;

import com.example.boxifylogistics.Response.LocalisationResponse;
import com.example.boxifylogistics.Response.PictureResponse;
import com.example.boxifylogistics.model.Delivery;
import com.example.boxifylogistics.model.DropOff;
import com.example.boxifylogistics.model.IndexedItem;
import com.example.boxifylogistics.model.ItemType;
import com.example.boxifylogistics.model.Parcel;
import com.example.boxifylogistics.model.PickUp;
import com.example.boxifylogistics.Response.IndexedItemResponse;
import com.example.boxifylogistics.Response.ItemTypesResponse;
import com.example.boxifylogistics.Response.ListResponse;
import com.example.boxifylogistics.Response.LoginResponse;
import com.example.boxifylogistics.Response.Response;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiRequest {

    /**
     * Permet à l'utilisateur de se login
     * @param email
     * @param password
     * @return
     */
    @POST("login")
    @FormUrlEncoded
    Call<LoginResponse> logisticianLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    /**
     * Récupération du profil de l'utilisateur avec le token
     * Permet de vérifier si le token est toujorus valide
     * @param header
     * @return
     */
    @GET("me")
    Call<LoginResponse> logisticianRelogin(
            @Header("Authorization") String header
    );


    @GET("logout")
    Call<JsonObject> logisticianLogout(
            @Header("Authorization") String header
    );

    @Headers("Accept: application/json")
    @POST("deliverer/item/uploadPhoto")
    @Multipart
    Call<PictureResponse> postItemPicture(
            @Header("Authorization") String header,
            @Part("boxify_id") RequestBody qr,
            @Part MultipartBody.Part photo
    );



    @GET("calculator/items")
    Call<ItemTypesResponse> getItemType(
/*
            @Query("q") String query,
            @Query("apikey") String apiKey */

    );

    //http://51.91.98.11/api/items/byQrCode/123456779

    @GET("items/byQrCode/{id}")
    Call<IndexedItemResponse> getItemByQrCode(
            @Header("Authorization") String header,
            @Path("id") String qr);

    @GET("calculator/item/{id}")
    Call<Response<ItemType>> getItemTypeByitem(
            @Header("Authorization") String header,
            @Path("id") int id
    );

    @POST("deliverer/indexation")
    Call<IndexedItem> postIndexation(
            @Header("Authorization") String header,
            @Body IndexedItem indexedItem
    );

    @PUT("deliverer/items/{qrcode}")
    Call<IndexedItem> putIndexation(
            @Header("Authorization") String header,
            @Path("qrcode") int qr,
            @Body IndexedItem indexedItem
    );

    @GET("deliverer/appointments/list")
    Call<ListResponse<Parcel>> getAppoinmentsList(
            @Header("Authorization") String header,
            @Query("Date") Date date
    );

    @GET("deliverer/appointments/list")
    Call<JsonObject> getAppoinmentsAsJson(
            @Header("Authorization") String header,
            @Query("Date") Date date
    );

    @FormUrlEncoded
    @POST("deliverer/pickup/firstScan")
    Call<JsonObject> checkFirstScan(
            @Header("Authorization") String header,
            @Field("qrCode") String qrCode,
            @Field("pickup_id") int pickup_id
//            ,@Field("access_token")String access_token
    );

    @FormUrlEncoded
    @POST("deliverer/pickup/lastScan")
    Call<JsonObject> checkSecondScan(
            @Header("Authorization") String header,
            @Field("qrCode") String qrCode,
            @Field("pickup_id") int pickup_id
    );

    @FormUrlEncoded
    @POST("deliverer/pickup/addScan")
    Call<JsonObject> addQROutsideList(
            @Header("Authorization") String header,
            @Field("qrCode") String qrCode,
            @Field("pickup_id") int pickup_id
    );

    @FormUrlEncoded
    @POST("deliverer/pickup/loadingItem")
    Call<JsonObject> checkScan(
            @Header("Authorization") String header,
            @Field("qrCode") String qrCode
            , @Field("pickup_id") int pickup_id
    );

    @GET("deliverer/pickup/checkPickup/{pickup_id}")
    Call<JsonObject> getCheckupState(
            @Header("Authorization") String header,
            @Path("pickup_id") int pickupId
    );

    @GET("deliverer/pickup/validPickup/{pickup_id}")
    Call<JsonObject> validatePickup(
            @Header("Authorization") String header,
            @Path("pickup_id") int pickupId
    );

    @GET("deliverer/appointments/detailsDelivery/{delivery_id}")
    Call<Response<Delivery>> getDetailsDelivery(
            @Header("Authorization") String header,
            @Path("delivery_id") int deliveryId
    );

    @GET("deliverer/appointments/detailsPickup/{pickup_id}")
    Call<Response<PickUp>> getDetailsPickup(
            @Header("Authorization") String header,
            @Path("pickup_id") int pickupId
    );

    @GET("deliverer/appointments/detailsDropoff/{dropoff_id}")
    Call<Response<DropOff>> getDetailsDropOff(
            @Header("Authorization") String header,
            @Path("dropoff_id") int dropoff_id
    );

    @GET("deliverer/appointments/list")
    Call<ListResponse<Parcel>> getAppoinments(
            @Header("Authorization") String header,
            @Query("date") String date
    );

    @GET("deliverer/delivery/checkDelivery/{delivery_id}")
    Call<JsonObject> getDeliveryState(
            @Header("Authorization") String header,
            @Path("delivery_id") int deliveryId
    );


    @POST("deliverer/delivery/unloadingItem")
    Call<JsonObject> deliverItem(
            @Header("Authorization") String header,
            @Query("delivery_id") int delivery_id,
            @Query("qrcode") String qrcode
    );


    @POST("deliverer/delivery/loadingItem")
    Call<JsonObject> loadItem(
            @Header("Authorization") String header,
            @Query("delivery_id") int delivery_id,
            @Query("qrcode") String qrcode
    );

    @FormUrlEncoded
    @POST("/deliverer/delivery/validDelivery/{delivery_id}")
    Call<JsonObject> validateDelivery(
            @Header("Authorization") String header,
            @Field("signature") String signature,
            @Path("delivery_id") int delivery_id
    );

    /*@POST("deliverer/delivery/validDelivery/{delivery_id}")
    Call<JsonObject> signDelivery(
            @Header("Authorization") String header,
            @Path("delivery_id") int delivery_id,
            @Body String signature
    );*/

    @Headers("Accept: application/json")
    @POST("deliverer/delivery/validDelivery/{delivery_id}")
    @Multipart
    Call<JsonObject> signDelivery(
            @Header("Authorization") String header,
            @Path("delivery_id") int delivery_id,
            @Part MultipartBody.Part signature
    );

    @Headers("Accept: application/json")
    @POST("deliverer/pickup/validPickup/{pickup_id}")
    @Multipart
    Call<JsonObject> signPickup(
            @Header("Authorization") String header,
            @Path("pickup_id") int pickup_id,
            @Part MultipartBody.Part signature
    );

    @Headers("Accept: application/json")
    @POST("deliverer/dropoff/validDropoff/{dropoff_id}")
    @Multipart
    Call<JsonObject> signDropOff(
            @Header("Authorization") String header,
            @Path("dropoff_id") int dropoff_id,
            @Part MultipartBody.Part signature
    );

    @FormUrlEncoded
    @POST("deliverer/appointments/checklist")
    Call<JsonObject> sendCheckedCheckbox(
                    @Header("Authorization") String header,
                    @Field("qrCode") JSONArray checkupChecked
    );

    @GET("deliverer/warehouse/position/item/{qrCode}")
    Call<JsonObject> getItemLocalisation(
            @Header("Authorization") String header,
            @Path("qrCode") String qrCode
    );

    @GET("deliverer/warehouse/position/pallet/{qrCode}")
    Call<JsonObject> getPalletLocalisation(
            @Header("Authorization") String header,
            @Path("qrCode") String qrCode
    );

    @FormUrlEncoded
    @POST("deliverer/warehouse/linkItem")
    Call<JsonObject> palletizeItem(
            @Header("Authorization") String header,
            @Field("itemsQRCode") JSONArray qrCodesToPalletize,
            @Field("qrPalette") String qrPallet
    );

    @FormUrlEncoded
    @POST("deliverer/warehouse/placePallet")
    Call<JsonObject> placePallet(
            @Header("Authorization") String header,
            @Field("qrCode") String qrCode,
            @Field("qrShelf") String qrShelf
    );

    @GET("user/byQrcode/{qrCode}")
    Call<JsonObject> getUser(
            @Header("Authorization") String header,
            @Path("qrCode") String qrCode
    );

    @GET("deliverer/warehouse/pallet/items/{qrCode}")
    Call<JsonObject> getItemOnPallet(
            @Header("Authorization") String header,
            @Path("qrCode") String qrCode
    );
}
