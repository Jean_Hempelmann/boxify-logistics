package com.example.boxifylogistics.Retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiPhotoRequest {
    @GET("{photoNameJpeg}")
    Call<ResponseBody> getItemByQrCode(
            @Path("photoNameJpeg") String qr
    );
}
