package com.example.boxifylogistics.ViewModel;

import android.app.Activity;
import android.app.Application;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.example.boxifylogistics.Repository.DropOffRepository;
import com.example.boxifylogistics.Utils.GPSTracker;
import com.example.boxifylogistics.model.DropOff;
import com.example.boxifylogistics.model.Item;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class DropOffViewModel extends AndroidViewModel {

    String TAG = "DropOffViewModel2";

    private int dropOffId;
    private String errors = "";

    private GPSTracker gpsTracker;

    private ArrayList<String> date = new ArrayList<>();

    public ArrayList<String> getDate() {
        return date;
    }

    public void setDate(ArrayList<String> date) {
        this.date = date;
    }

    DropOffRepository rpstry;
    MutableLiveData<DropOff> dropOff = new MutableLiveData<>();
    MutableLiveData<ArrayList<Item>> allItem = new MutableLiveData<>(new ArrayList<Item>());
    MutableLiveData<Float> distance = new MutableLiveData<>();


    public MutableLiveData<Boolean> canNavigate() {
        return canNavigate;
    }

    private MutableLiveData<Boolean> canNavigate = new MutableLiveData<>(false);
    private boolean IsAllChecked = false;

    public boolean getIsAllChecked() {
        return IsAllChecked;
    }
    private ArrayList<String> checkupChecked = new ArrayList<String>();
    private ArrayList<String> materialCheck = new ArrayList<String>() {
        {
            add("Rouleau de QrCodes");
            add("Collier de serrage");
            add("Couverture de Protection");
            add("Papier Collant");
            add("Diable et mini-Rolls");
            add("Porte-GSM Charger Smartphone");
        }
    };

    public ArrayList<String> getCheckupChecked(){return checkupChecked;}

    private ArrayList<String> carCheck = new ArrayList<String>() {
        {
            add("État du vehicule");
            add("Niveau essence");
            add("Validité assurance");
            add("Carte essence");
            add("Constat d'accident");
            add("Permis de conduire");
        }
    };

    public void addCheck(String s) {
        if (!checkupChecked.contains(s)) {
            checkupChecked.add(s);
        }
        if(checkupChecked.contains("Validité assurance") && checkupChecked.contains("Permis de conduire")){
            canNavigate.setValue(true);
        }
        if(checkupChecked.size() == carCheck.size() + materialCheck.size()){
            IsAllChecked=true;
        }
    }

    public void removeCheck(String s) {
        if (checkupChecked.contains(s)) {
            checkupChecked.remove(s);
        }
        if(!checkupChecked.contains("Validité assurance")||!checkupChecked.contains("Permis de conduire"))
            canNavigate.setValue(false);
        if(checkupChecked.size() != carCheck.size() + materialCheck.size()){
            IsAllChecked=false;
        }
    }

    public ArrayList<String> getMaterialCheck() {
        return materialCheck;
    }

    public ArrayList<String> getCarCheck() {
        return carCheck;
    }

    public DropOffViewModel(@NonNull Application application) {
        super(application);
        rpstry = new DropOffRepository();
    }

    public void setGpsTracker(Activity ctx) {
        gpsTracker = new GPSTracker(ctx);
        refreshDistance();
    }

    public void refreshDistance() {
        gpsTracker.getLocationBinded().observeForever(new Observer<Location>() {
            @Override
            public void onChanged(Location b) {
                if (b != null) {
                    Location a = new Location("pointA");
                    a.setLatitude(Double.valueOf(dropOff.getValue().getAddress().getLatitude()));
                    a.setLongitude(Double.valueOf(dropOff.getValue().getAddress().getLongitude()));
                    distance.setValue(a.distanceTo(b));
                }

            }
        });
    }

    public LiveData<Float> getDistance() {
        return distance;
    }

    public void setDropOffId(int dropOffId) {
        this.dropOffId = dropOffId;
    }

    public LiveData<DropOff> getDropOffDetails() {
        if (dropOff.getValue() == null) {
            rpstry.getDropOffDetails(dropOffId).observeForever(new Observer<DropOff>() {
                @Override
                public void onChanged(DropOff result) {
                    if (result != null) {
                        dropOff.setValue(result);
                    }
                }
            });
        }

        return dropOff;
    }

    public int getDropOffId(){
        return dropOffId;
    }


    public String getUrlForNavigation() {
        String url = "https://waze.com/ul?ll=";
        DropOff to = dropOff.getValue();
        if (to != null) {
            url = url + to.getAddress().getLatitude() + "," + to.getAddress().getLongitude()
                    + "navigate=yes";
        }
        return url;
    }

    public LiveData<ArrayList<Item>> getAllItems() {
        return allItem;
    }


    public void endDropOff(File signature){
        rpstry.endDropOff(dropOffId,signature);
    }
}
