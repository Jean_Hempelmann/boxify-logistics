package com.example.boxifylogistics.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.boxifylogistics.model.Parcel;
import com.example.boxifylogistics.Repository.ParcelRepository;

import java.util.ArrayList;

public class ParcelViewModel extends ViewModel {
    ParcelRepository rpstry;
    public ParcelViewModel() {
        super();
        rpstry = new ParcelRepository();
    }

    public LiveData<ArrayList<Parcel>> getAllPackages(String date){
        return rpstry.getPackagesList(date);
    }
}
