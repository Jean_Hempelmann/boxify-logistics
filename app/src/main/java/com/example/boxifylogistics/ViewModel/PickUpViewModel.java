package com.example.boxifylogistics.ViewModel;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.location.Location;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.example.boxifylogistics.model.PickUp;
import com.example.boxifylogistics.Repository.PickUpRepository;
import com.example.boxifylogistics.Utils.GPSTracker;
import com.example.boxifylogistics.model.PickUp;
import com.example.boxifylogistics.Repository.PickUpRepository;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class PickUpViewModel extends AndroidViewModel {

    String TAG = "PickupViewModel2";

    private int pickupId;
    private String errors = "";

    private GPSTracker gpsTracker;

    private ArrayList<String> date = new ArrayList<>();

    public ArrayList<String> getDate() {
        return date;
    }

    public void setDate(ArrayList<String> date) {
        this.date = date;
    }

    PickUpRepository rpstry;
    MutableLiveData<PickUp> pickup = new MutableLiveData<>();

    MutableLiveData<Float> distance = new MutableLiveData<>();


    private MutableLiveData<String> firstQrCode = new MutableLiveData<>();
    private MutableLiveData<String> secondQrCode = new MutableLiveData<>();

    private MutableLiveData<ArrayList<String>> allQr = new MutableLiveData<>(new ArrayList<String>());
    private MutableLiveData<ArrayList<String>> qrScanned = new MutableLiveData<>(new ArrayList<String>());
    private MutableLiveData<ArrayList<String>> qrNotScanned = new MutableLiveData<>(new ArrayList<String>());

    private MutableLiveData<Integer> nbTotQr = new MutableLiveData<>(0);
    private MutableLiveData<Integer> nbQrScanned = new MutableLiveData<>(0);
    private MutableLiveData<Integer> nbQrNotScanned = new MutableLiveData<>(0);


    public MutableLiveData<Boolean> canNavigate() {
        return canNavigate;
    }
    private boolean isScannerUsed = false;
    private MutableLiveData<Boolean> canNavigate = new MutableLiveData<>(false);
    private boolean IsAllChecked = false;

    final ArrayList<String> qrTotToScan = new ArrayList<>();
    final ArrayList<String> qrRemToScan = new ArrayList<>();
    final ArrayList<String> qrCodeScannd = new ArrayList<>();

    private ArrayList<String> qrOutsideList = new ArrayList<String>();

    public boolean getIsAllChecked() {
        return IsAllChecked;
    }
    private ArrayList<String> checkupChecked = new ArrayList<String>();
    private ArrayList<String> materialCheck = new ArrayList<String>() {
        {
            add("Rouleau de QrCodes");
            add("Collier de serrage");
            add("Couverture de Protection");
            add("Papier Collant");
            add("Diable et mini-Rolls");
            add("Porte-GSM Charger Smartphone");
        }
    };

    public ArrayList<String> getCheckupChecked(){return checkupChecked;}

    private ArrayList<String> carCheck = new ArrayList<String>() {
        {
            add("État du vehicule");
            add("Niveau essence");
            add("Validité assurance");
            add("Carte essence");
            add("Constat d'accident");
            add("Permis de conduire");
        }
    };

    public void addCheck(String s) {
        if (!checkupChecked.contains(s)) {
            checkupChecked.add(s);
        }
        if(checkupChecked.contains("Validité assurance") && checkupChecked.contains("Permis de conduire") && checkupChecked.contains("Rouleau de QrCodes")){
            canNavigate.setValue(true);
        }
        if(checkupChecked.size() == carCheck.size() + materialCheck.size()){
            IsAllChecked=true;
        }
    }

    public LiveData<ArrayList<String>> getAllQr() {
        return allQr;
    }

    public LiveData<ArrayList<String>> getQrScanned() {
        return qrScanned;
    }

    public LiveData<ArrayList<String>> getQrNotScanned() {
        return qrNotScanned;
    }

    public void removeCheck(String s) {
        if (checkupChecked.contains(s)) {
            checkupChecked.remove(s);
        }
        if(!checkupChecked.contains("Validité assurance")||!checkupChecked.contains("Permis de conduire") || !checkupChecked.contains("Rouleau de QrCodes"))
            canNavigate.setValue(false);
        if(checkupChecked.size() != carCheck.size() + materialCheck.size()){
            IsAllChecked=false;
        }
    }

    public ArrayList<String> getMaterialCheck() {
        return materialCheck;
    }

    public ArrayList<String> getCarCheck() {
        return carCheck;
    }

    public PickUpViewModel(@NonNull Application application) {
        super(application);
        rpstry = new PickUpRepository();
    }

    public void setGpsTracker(Activity ctx) {
        gpsTracker = new GPSTracker(ctx);
        refreshDistance();
    }

    public void refreshDistance() {
        gpsTracker.getLocationBinded().observeForever(new Observer<Location>() {
            @Override
            public void onChanged(Location b) {
                if (b != null) {
                    Location a = new Location("pointA");
                    a.setLatitude(Double.valueOf(pickup.getValue().getAddress().getLatitude()));
                    a.setLongitude(Double.valueOf(pickup.getValue().getAddress().getLongitude()));
                    distance.setValue(a.distanceTo(b));
                }

            }
        });
    }

    public LiveData<Float> getDistance() {
        return distance;
    }

    public void setPickupId(int pickupId) {
        this.pickupId = pickupId;
    }

    public LiveData<PickUp> getPickupDetails(Date date) {
        if (pickup.getValue() == null) {
            rpstry.getPickUpDetails(pickupId).observeForever(new Observer<PickUp>() {
                @Override
                public void onChanged(PickUp result) {
                    if (result != null) {
                        pickup.setValue(result);
                        fetchQrCodes();
                    }
                }
            });
        }

        return pickup;
    }

    public int getPickupId(){
        return pickupId;
    }

    public LiveData<String> getFirstQrCode() {
        return firstQrCode;
    }

    public LiveData<String> getSecondQrCode() {
        return secondQrCode;
    }

    public void fetchQrCodes() {
        MutableLiveData<Map<String, Integer>> dataCheckPickup;
        if (allQr.getValue().isEmpty()) {
            dataCheckPickup = rpstry.getCheckUpState(pickupId);
            dataCheckPickup.observeForever(new Observer<Map<String, Integer>>() {
                @Override
                public void onChanged(Map<String, Integer> stringIntegerMap) {
                    if (stringIntegerMap != null && !stringIntegerMap.isEmpty()) {
                        if (stringIntegerMap.get("first_qr_code") != 0) {
                            firstQrCode.setValue(stringIntegerMap.get("first_qr_code").toString());
                        }
                        if (stringIntegerMap.get("last_qr_code") != 0) {
                            secondQrCode.setValue(stringIntegerMap.get("last_qr_code").toString());
                        }
                        if (stringIntegerMap.get("first_qr_code") != 0 && stringIntegerMap.get("last_qr_code") != 0) {
                            setAllQr();
                        }
                    }

                }
            });
            rpstry.getPickupItemRemaining(pickupId).observeForever(new Observer<ArrayList<String>>() {
                @Override
                public void onChanged(ArrayList<String> strings) {

                }
            });
        }

    }

    public LiveData<Boolean> pushFirstLastQrCode() {
        final MutableLiveData<Boolean> result = new MutableLiveData<>(false);
        if (secondQrCode.getValue() == null) {
            rpstry.checkFirstQrCode(firstQrCode.getValue(), pickupId).observeForever(new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    result.setValue(aBoolean);
                }
            });
        }
        else{
            rpstry.checkSecondQrCode(secondQrCode.getValue(), pickupId).observeForever(new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean aBoolean) {
                    result.setValue(aBoolean);
                    setAllQr();
                }
            });
        }

        return result;
    }

    public String getUrlForNavigation() {
        String url = "https://waze.com/ul?ll=";
        PickUp to = pickup.getValue();
        if (to != null) {
            url = url + to.getAddress().getLatitude() + "," + to.getAddress().getLongitude()
                    + "navigate=yes";
        }
        return url;
    }

    public void isFirstScanValid(String qr) {
        firstQrCode.setValue(qr);

    }

    public void isSecondScanValid(String qr) {
        if (Integer.decode(firstQrCode.getValue()) >= Integer.decode(qr)) {
            Log.e(TAG, "isSecondScanValid: "+"ok" );
            errors = "Le second QrCode doit être strictement supérieur au premier";
            return;
        }

        secondQrCode.setValue(qr);

    }

    public void clearError() {
        errors = "";
    }

    public String getErrors() {
        return errors;
    }

    public void clearFirstQr() {
        firstQrCode.setValue("");
    }

    public void clearSecondQr() {
        secondQrCode.setValue("");
    }

    public void setAllQr() {
        for (int i = Integer.decode(firstQrCode.getValue()); i < Integer.decode(secondQrCode.getValue()); i++) {
            qrTotToScan.add(String.valueOf(i));
        }

        for (int i=0; i<qrOutsideList.size(); i++){
            Log.e(TAG,qrOutsideList.get(i));
            qrTotToScan.add(qrOutsideList.get(i));
            qrCodeScannd.add(qrOutsideList.get(i));
        }
        allQr.setValue(qrTotToScan);
        rpstry.getPickupItemRemaining(pickupId).observeForever(new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(ArrayList<String> strings) {
                if (!strings.isEmpty()) {
                    qrCodeScannd.clear();
                    qrRemToScan.clear();
                    for (String s : qrOutsideList) {
                        qrTotToScan.add(s);
                    }
                    for (String s : strings) {
                        qrRemToScan.add(s);
                    }
                    for (String s : qrTotToScan) {
                        if (!qrRemToScan.contains(s)) {
                            qrCodeScannd.add(s);
                        }
                    }


                } else {
                    qrCodeScannd.clear();
                    qrCodeScannd.addAll(qrTotToScan);
                }
                allQr.setValue(qrTotToScan);
                qrScanned.setValue(qrCodeScannd);
                qrNotScanned.setValue(qrRemToScan);
                nbTotQr.setValue(allQr.getValue().size());
                nbQrScanned.setValue(qrScanned.getValue().size());
                nbQrNotScanned .setValue(qrRemToScan.size());
            }
        });

    }

    public LiveData<Integer> getNbTotQr() {
        return nbTotQr;
    }

    public LiveData<Integer> getNbQrScanned() {
        return nbQrScanned;
    }

    public LiveData<Integer> getNbQrNotScanned() {
        return nbQrNotScanned;
    }

    public void scanIsUsed() {
        isScannerUsed = true;
    }

    public void scanIsNotUsed() {
        isScannerUsed = false;
    }

    public boolean scannerHasBeenUsed() {
        return isScannerUsed;
    }

    public void qrScanned(final String qr) {
        final ArrayList<String> qrCodesScanned = new ArrayList<>(qrScanned.getValue());
        final ArrayList<String> qrCodesNotScanned = new ArrayList<>(qrNotScanned.getValue());
        if (!allQr.getValue().contains(qr)) {
            errors = "Le QR scanné ne fait pas partie des QR à scanner";
            return;
        }
        if (qrScanned.getValue().contains(qr)) {
            errors = "Le QR scanné a déjà été scanné";
            return;
        }
        rpstry.checkQrCode(qr, pickupId).observeForever(new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    if (!qrCodesScanned.contains(qr)) {
                        qrCodesScanned.add(qr);
                        qrScanned.setValue(qrCodesScanned);
                    }
                    nbQrScanned.setValue(qrScanned.getValue().size());
                    if (qrCodesNotScanned.contains(qr)) {
                        qrCodesNotScanned.remove(qr);
                        qrNotScanned.setValue(qrCodesNotScanned);

                    }
                    nbQrNotScanned.setValue(qrNotScanned.getValue().size());
                }
            }
        });
    }

    public void addQROutsideList(String qr){
        qrTotToScan.add(qr);
        qrCodeScannd.add(qr);
        qrOutsideList.add(qr);
        Log.e(TAG,""+rpstry.checkQrCode(qr, pickupId).getValue());
    }

    public void finishPickup(File signature){
        rpstry.endPickup(pickupId,signature);
    }
}
