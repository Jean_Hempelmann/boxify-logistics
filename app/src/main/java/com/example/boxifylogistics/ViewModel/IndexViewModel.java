package com.example.boxifylogistics.ViewModel;

import android.graphics.Bitmap;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.example.boxifylogistics.model.IndexedItem;
import com.example.boxifylogistics.model.ItemType;
import com.example.boxifylogistics.Repository.ItemsRepository;
import com.example.boxifylogistics.Response.IndexedItemResponse;
import com.example.boxifylogistics.Response.ItemTypesResponse;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Response;


public class IndexViewModel extends ViewModel {

    private ItemsRepository itemsRepository;

  //  private MutableLiveData<List<ItemTypesResponse>> itemsResponseLiveData;

    private LiveData<ItemTypesResponse> itemsResponseLiveData;

    private MutableLiveData<String> description = new MutableLiveData<>();

    private MutableLiveData<Response<ResponseBody>> indexedItemResponse ;
    MutableLiveData<Bitmap> btp = new MutableLiveData<>();

    private ItemType itemType;

    private IndexedItem indexedItem =new IndexedItem();
    private boolean editing;



    public LiveData<ItemTypesResponse> getObjectTypes() {
        if (itemsResponseLiveData == null) {
            loadItems();
        }
        return itemsResponseLiveData;
    }

    private void loadItems() {
        // Do an asynchronous operation to fetch users.
        this.itemsResponseLiveData = itemsRepository.getItemType();
    }

    /*
    public MutableLiveData<Response<ResponseBody>> loadIndexedItem(String qr){

        itemsRepository = new ItemsRepository();
        this.indexedItemResponse=itemsRepository.getItemByQrCode(qr);
        return indexedItemResponse;
    }*/

    public LiveData<IndexedItemResponse> loadIndexedItem(String qr){
        return itemsRepository.getItemByQrCode(qr);
    }
    public LiveData<IndexedItem> postIndexation(){
        return itemsRepository.postIndexation(this.indexedItem);
    }

    public LiveData<IndexedItem> putIndexation() {
        return itemsRepository.putIndexation(this.indexedItem);
    }

    public IndexViewModel() {
        itemsRepository = new ItemsRepository();
    }

    public void setCalculatorItemId(int calculatorItemId){
        indexedItem.setCalculator_item_id(calculatorItemId);
    }

    public boolean setName(String name){
        return indexedItem.setName(name);
    }

    public boolean setDescription(String desc){
        return indexedItem.setDescription(desc);
    }

    public void setIsFragile(int isFragile){
        indexedItem.setIs_fragile(isFragile);
    }

    public void setIsLarge(int isLarge){
        indexedItem.setIs_large(isLarge);
    }
    public void setOnPallet(int onPallet){
        indexedItem.setOn_pallet(onPallet);
    }

    public boolean setVolume(double volume){
        return indexedItem.setVolume(volume);
    }

    public boolean setPhotoBase64(String photoBase64){
        return indexedItem.setPhotoBase64(photoBase64);
    }
    public boolean setPhotoBitmap(Bitmap photoBase64){
        if(indexedItem.setPhotoBitmap(photoBase64)){
            btp.setValue(indexedItem.photoBitmap());
            return true;
        }
        return  false;
    }

    public LiveData<Bitmap> getBtp() {
        return btp;
    }

    public boolean setPhotoFile(File photoBase64){
        return indexedItem.setPhoto(photoBase64);
    }

    /*public String getPhotoString() {
        return indexedItem.getPhotoString();
    }*/

    public MutableLiveData<String> getDescription(){
        return description;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.i("viewModel2"," onCleared");
    }

    public void setDefaultValues(ItemType itemtType){
        this.indexedItem.setIs_fragile(itemType.getIs_fragile());
        this.indexedItem.setIs_large(itemtType.getIs_large());
        this.indexedItem.setVolume(itemtType.getVolume());
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public IndexedItem getIndexedItem(){
        return indexedItem;
    }


    public void setIndexedItem(IndexedItem indexedItem) {
        this.indexedItem =indexedItem;
    }

    public boolean isEditing() {
        return editing;
    }

    public void setEditing(boolean editing) {
        this.editing = editing;
    }

    public void fetchItemType(int id){
        itemsRepository.getItemTypeOf(id).observeForever(new Observer<ItemType>() {
            @Override
            public void onChanged(ItemType itemType) {
                setItemType(itemType);
            }
        });

    }
    public void getPhotoJPG(IndexedItem it){
         itemsRepository.getPhoto(it,it.getPhotoString()).observeForever(new Observer<IndexedItem>() {
             @Override
             public void onChanged(IndexedItem indexedItem) {
                 btp.setValue(indexedItem.photoBitmap());
             }
         });
    }
}
