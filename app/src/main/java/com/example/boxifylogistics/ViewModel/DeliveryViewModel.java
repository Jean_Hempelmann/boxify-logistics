package com.example.boxifylogistics.ViewModel;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.location.Location;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.example.boxifylogistics.Utils.GPSTracker;
import com.example.boxifylogistics.model.Delivery;
import com.example.boxifylogistics.model.Item;
import com.example.boxifylogistics.Repository.DeliveryRepository;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Map;


public class DeliveryViewModel extends AndroidViewModel {

    String TAG = "DeliveryViewModel";
    private String errors = "";
    private ArrayList<String> date = new ArrayList<>();

    private File signatureFile;

    public ArrayList<String> getDate() {
        return date;
    }

    public void setDate(ArrayList<String> date) {
        this.date = date;
    }

    private GPSTracker gpsTracker;

    DeliveryRepository rpstry;
    MutableLiveData<Delivery> delivery = new MutableLiveData<>();


    MutableLiveData<ArrayList<Item>> itemToLoad = new MutableLiveData<>(new ArrayList<Item>());
    MutableLiveData<ArrayList<Item>> itemLoaded = new MutableLiveData<>(new ArrayList<Item>());
    MutableLiveData<ArrayList<Item>> allItem = new MutableLiveData<>(new ArrayList<Item>());

    MutableLiveData<Integer> nbTotalItems = new MutableLiveData<>();
    MutableLiveData<Integer> nbLoadedItems = new MutableLiveData<>();
    MutableLiveData<Integer> nbNotLoadedItems = new MutableLiveData<>();
    MutableLiveData<Integer> nbDeliveredItems = new MutableLiveData<>();


    MutableLiveData<Float> distance = new MutableLiveData<>();

    public MutableLiveData<Boolean> canNavigate() {
        return canNavigate;
    }

    MutableLiveData<ArrayList<Item>> itemDelivered = new MutableLiveData<>(new ArrayList<Item>());

    private MutableLiveData<Boolean> canNavigate = new MutableLiveData<>(false);
    private boolean IsAllChecked = false;


    private ArrayList<String> checkupChecked = new ArrayList<String>();
    public ArrayList<String> getCheckupChecked(){return checkupChecked;}
    private ArrayList<String> materialCheck = new ArrayList<String>() {
        {
            add("Collier de serrage");
            add("Couverture de Protection");
            add("Papier Collant");
            add("Diable et mini-Rolls");
            add("Porte-GSM Charger Smartphone");
        }
    };

    private ArrayList<String> carCheck = new ArrayList<String>() {
        {
            add("État du vehicule");
            add("Niveau essence");
            add("Validité assurance");
            add("Carte essence");
            add("Constat d'accident");
            add("Permis de conduire");
        }
    };
    public boolean isAllLoaded(){
        if(nbNotLoadedItems != null && nbNotLoadedItems.getValue() != 0){
            return false;
        }
        return true;
    }
    public boolean isAllDelivered(){
        if(nbNotLoadedItems != null && nbNotLoadedItems.getValue() != 0 &&  nbDeliveredItems.getValue() != 0)
            return false;
        return true;
    }

    public LiveData<ArrayList<Item>> getItemDelivered() {
        return itemDelivered;
    }

    public boolean getIsAllChecked() {
        return IsAllChecked;
    }

    public void addCheck(String s) {
        if (!checkupChecked.contains(s)) {
            checkupChecked.add(s);
        }
        if(checkupChecked.contains("Validité assurance") && checkupChecked.contains("Permis de conduire")){
            canNavigate.setValue(true);
        }
        if(checkupChecked.size() == carCheck.size() + materialCheck.size()){
            IsAllChecked=true;
        }
    }

    public void removeCheck(String s) {
        if (checkupChecked.contains(s)) {
            checkupChecked.remove(s);
        }
        if(!checkupChecked.contains("Validité assurance")||!checkupChecked.contains("Permis de conduire"))
            canNavigate.setValue(false);
        if(checkupChecked.size() != carCheck.size() + materialCheck.size()){
            IsAllChecked=false;
        }
    }

    public LiveData<Integer> getNbTotalItems() {
        return nbTotalItems;
    }

    public LiveData<Integer> getNbLoadedItems() {
        return nbLoadedItems;
    }

    public LiveData<Integer> getNbNotLoadedItems() {
        return nbNotLoadedItems;
    }

    public LiveData<Integer> getNbDeliveredItems() {
        return nbDeliveredItems;
    }

    public ArrayList<String> getMaterialCheck() {
        return materialCheck;
    }

    public ArrayList<String> getCarCheck() {
        return carCheck;
    }

    private int deliveryId;

    public DeliveryViewModel(@NonNull Application application) {
        super(application);
        rpstry = new DeliveryRepository();

    }

    public void setGpsTracker(Activity ctx) {
        gpsTracker = new GPSTracker(ctx);
        refreshDistance();
    }

    public String getErrors() {
        return errors;
    }

    public void setDeliveryId(int id){
        deliveryId = id;
    }

    public LiveData<Delivery> getDelivery(){
        if (delivery.getValue() == null) {
            rpstry.getDeliveryDetails(deliveryId).observeForever(new Observer<Delivery>() {
                @Override
                public void onChanged(Delivery result) {
                    if (result != null) {
                        delivery.setValue(result);
                        if (result.getItems() != null) {
                            allItem.setValue(result.getItems());
                            itemToLoad.setValue(result.getItems());
                            nbTotalItems.setValue(allItem.getValue().size());
                        }

                    }
                }
            });
        }
        setAllItems();
        return delivery;
    }

    public void refreshDistance() {
        gpsTracker.getLocationBinded().observeForever(new Observer<Location>() {
            @Override
            public void onChanged(Location b) {
                if (b != null) {
                    Location a = new Location("pointA");
                    a.setLatitude(Double.valueOf(delivery.getValue().getAddress().getLatitude()));
                    a.setLongitude(Double.valueOf(delivery.getValue().getAddress().getLongitude()));
                    distance.setValue(a.distanceTo(b));
                }

            }
        });
    }

    public LiveData<Float> getDistance() {
        return distance;
    }

    public Item getItemByQr(final int qr) {
        final Item[] it = {null};
        delivery.observeForever(new Observer<Delivery>() {
            @Override
            public void onChanged(Delivery delivery) {
                if(delivery != null){
                    for (Item items : delivery.getItems()) {
                        if (items.getBoxify_id() == qr)
                            it[0] =  items;
                    }
                }

            }
        });

        return it[0];
    }

    public void fetchItemsAlreadyLoaded() {
        rpstry.getItemsAlreadyLoaded(deliveryId).observeForever(new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(ArrayList<String> strings) {
                if (!strings.isEmpty()) {
                    for (String qr : strings) {
                        Item it = getItemByQr(Integer.valueOf(qr));
                        addToItemLoaded(it);
                    }

                }
            }
        });


    }

    public void fetchItemsAlreadyDelivered() {
        rpstry.getItemsAlreadyDelivered(deliveryId).observeForever(new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(ArrayList<String> strings) {
                if (!strings.isEmpty()) {
                    for (String qr : strings) {
                        Item it = getItemByQr(Integer.valueOf(qr));
                        deliverItem(it);
                        addToItemDelivered(it);
                    }

                }
            }
        });
    }
    public int getNbTotal(){
        return allItem.getValue().size();
    }

    private void addToItemLoaded(Item it){
        ArrayList<Item> tmp_itemToLoad = new ArrayList<>(itemToLoad.getValue());
        ArrayList<Item> tmp_itemLoaded = new ArrayList<>(itemLoaded.getValue());

        if(tmp_itemLoaded.contains(it)){
            tmp_itemLoaded.remove(it);
        }
        if(!tmp_itemToLoad.contains(it)){
            tmp_itemToLoad.add(it);
        }
        itemToLoad.setValue(tmp_itemToLoad);
        itemLoaded.setValue(tmp_itemLoaded);

        nbNotLoadedItems.setValue(itemToLoad.getValue().size());
        nbLoadedItems.setValue(itemLoaded.getValue().size());

    }
    private void addToItemDelivered(Item it){
        ArrayList<Item> tmp_itemToLoad = new ArrayList<>(itemToLoad.getValue());
        ArrayList<Item> tmp_itemLoaded = new ArrayList<>(itemLoaded.getValue());
        ArrayList<Item> tmp_itemDelivered = new ArrayList<>(itemDelivered.getValue());

        if(tmp_itemLoaded.contains(it)){
            tmp_itemLoaded.remove(it);
        }
        if(tmp_itemToLoad.contains(it)){
            tmp_itemToLoad.remove(it);
        }
        if(!tmp_itemDelivered.contains(it)){
            tmp_itemDelivered.add(it);
        }
        itemToLoad.setValue(tmp_itemToLoad);
        itemLoaded.setValue(tmp_itemLoaded);
        itemDelivered.setValue(tmp_itemDelivered);

        nbNotLoadedItems.setValue(itemToLoad.getValue().size());
        nbLoadedItems.setValue(itemLoaded.getValue().size());
        nbDeliveredItems.setValue(itemDelivered.getValue().size());

    }

    public LiveData<ArrayList<Item>> getItemLoaded() {
        return itemLoaded;
    }

    public LiveData<ArrayList<Item>> getAllItems() {
        return allItem;
    }

    public LiveData<ArrayList<Item>> getItemToLoad() {
        return itemToLoad;
    }

    public Item qrLoaded(String qr) {

        Item it = getItemByQr(Integer.valueOf(qr));

        if (!allItem.getValue().contains(it)) {
            errors += "Item pas dans la liste";
            return null;
        }
        if (itemDelivered.getValue().contains(it)) {
            errors += "Item déjà livré";
            return null;
        }
        if (itemLoaded.getValue().contains(it)) {
            errors += "Item already loaded";
            return null;
        }
        loadItem(it);
        rpstry.loadItem(deliveryId, String.valueOf(it.getBoxify_id()));
        return it;
    }

    public Item qrDelivered(String qr) {

        Item it = getItemByQr(Integer.valueOf(qr));

        if (!allItem.getValue().contains(it)) {
            errors += "Item pas dans la liste";
            return null;
        }
        if (itemDelivered.getValue().contains(it)) {
            errors += "Item déjà livré";
            return null;
        }
        if (!itemLoaded.getValue().contains(it)) {
            errors += "Item pas encore chargé";
            return null;
        }
        deliverItem(it);
        rpstry.deliverItem(deliveryId, String.valueOf(it.getBoxify_id()));
        return it;
    }

    public void clearErrors() {
        errors = "";
    }

    public void loadItem(Item it) {
        ArrayList<Item> tmp_itemToLoad = new ArrayList<>(itemToLoad.getValue());
        ArrayList<Item> tmp_itemLoaded = new ArrayList<>(itemLoaded.getValue());
        if(tmp_itemToLoad.contains(it))
            tmp_itemToLoad.remove(it);
        if(!tmp_itemLoaded.contains(it))
            tmp_itemLoaded.add(it);
        itemToLoad.setValue(tmp_itemToLoad);
        itemLoaded.setValue(tmp_itemLoaded);

        nbNotLoadedItems.setValue(itemToLoad.getValue().size());
        nbLoadedItems.setValue(itemLoaded.getValue().size());
        nbDeliveredItems.setValue(itemDelivered.getValue().size());
    }

    public void deliverItem(Item it) {
        ArrayList<Item> tmp_itemToLoad = new ArrayList<>(itemLoaded.getValue());
        ArrayList<Item> tmp_itemLoaded = new ArrayList<>(itemDelivered.getValue());
        if(tmp_itemToLoad.contains(it))
            tmp_itemToLoad.remove(it);
        if(!tmp_itemLoaded.contains(it))
            tmp_itemLoaded.add(it);
        itemLoaded.setValue(tmp_itemToLoad);
        itemDelivered.setValue(tmp_itemLoaded);

        nbNotLoadedItems.setValue(itemToLoad.getValue().size());
        nbLoadedItems.setValue(itemLoaded.getValue().size());
        nbDeliveredItems.setValue(itemDelivered.getValue().size());

    }

    /* WEEKEND */
    public String getUrlForNavigation() {
        String url = "https://waze.com/ul?q=";
        Delivery to = delivery.getValue();
        /*url = url + to.getAddress().getLatitude() + "," + to.getAddress().getLongitude()
                + "navigate=yes";*/
        String number = to.getAddress().getNumber();
        String street = to.getAddress().getStreet();
        String zipcode = to.getAddress().getZipcode();
        String city = to.getAddress().getCity();
        url = url +number+" "+street+" "+zipcode+" "+city;
        url = url.replace(" ","%20");
        url = Normalizer.normalize(url, Normalizer.Form.NFD);
        url = url.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return url;
    }

    public void finishDelivery(File signature) throws IOException {
        rpstry.endDelivery(deliveryId,signature);
    }

    public void setAllItems() {

        ArrayList<Item> AllItems = new ArrayList<>();
        final ArrayList<Item> deliveredItems = new ArrayList<>();
        final ArrayList<Item> scannedItems = new ArrayList<>();
        final ArrayList<Item> notScannedItems = new ArrayList<>();
        AllItems = allItem.getValue();
//        rpstry.getItemsAlreadyLoaded(deliveryId).observeForever(new Observer<ArrayList<String>>() {
//            @Override
//            public void onChanged(ArrayList<String> strings) {
//                if (!strings.isEmpty()) {
//                    for (String qr : strings) {
//                        Item it = getItemByQr(Integer.valueOf(qr));
//                        if(!scannedItems.contains(it)){
//                            scannedItems.add(it);
//                        }
//                        if(notScannedItems.contains(it))
//                            notScannedItems.remove(it);
//                    }
//
//                }
//            }
//        });
//        rpstry.getItemsAlreadyDelivered(deliveryId).observeForever(new Observer<ArrayList<String>>() {
//            @Override
//            public void onChanged(ArrayList<String> strings) {
//                if (!strings.isEmpty()) {
//                    deliveredItems.clear();
//                    for (String qr : strings) {
//                        Item it = getItemByQr(Integer.parseInt(qr));
//                        if(!deliveredItems.contains(it)){
//                            deliveredItems.add(it);
//                        }
//                        if(scannedItems.contains(it)){
//                            scannedItems.remove(it);
//                        }
//                        if(notScannedItems.contains(it))
//                            notScannedItems.remove(it);
//                    }
//
//                }
//                itemDelivered.setValue(deliveredItems);
//                itemToLoad.setValue(notScannedItems);
//                itemLoaded.setValue(scannedItems);
//            }
//        });
        rpstry.getStateDelivery(deliveryId).observeForever(new Observer<Map<String, ArrayList<String>>>() {
            @Override
            public void onChanged(Map<String, ArrayList<String>> stringArrayListMap) {
                Log.e(TAG, "onChanged: "+stringArrayListMap );
                if (!stringArrayListMap.isEmpty()) {
                    if (!stringArrayListMap.get("items_delivered").isEmpty()) {
                        for (String s : stringArrayListMap.get("items_delivered")) {
                            Item it = getItemByQr(Integer.parseInt(s));
                            if (!deliveredItems.contains(it)) {
                                deliveredItems.add(it);
                            }
                        }
                    }
                    if (!stringArrayListMap.get("items_loaded").isEmpty()) {
                        for (String s : stringArrayListMap.get("items_loaded")) {
                            Item it = getItemByQr(Integer.parseInt(s));
                            if (!scannedItems.contains(it)) {
                                scannedItems.add(it);
                            }
                        }
                    }
                    if (!stringArrayListMap.get("items_not_loaded").isEmpty()) {
                        for (String s : stringArrayListMap.get("items_not_loaded")) {
                            Item it = getItemByQr(Integer.parseInt(s));
                            if (!notScannedItems.contains(it)) {
                                notScannedItems.add(it);
                            }
                        }
                    }
                    itemDelivered.setValue(deliveredItems);
                    itemToLoad.setValue(notScannedItems);
                    itemLoaded.setValue(scannedItems);

                    nbNotLoadedItems.setValue(itemToLoad.getValue().size());
                    nbLoadedItems.setValue(itemLoaded.getValue().size());
                    nbDeliveredItems.setValue(itemDelivered.getValue().size());
                }
            }
        });

    }



}
