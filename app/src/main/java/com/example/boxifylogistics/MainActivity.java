package com.example.boxifylogistics;

import android.content.DialogInterface;
import android.content.Intent;
import android.device.ScanDevice;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.example.boxifylogistics.Indexation.IndexationActivity;
import com.example.boxifylogistics.Palletization.PalletizationActivity;
import com.example.boxifylogistics.Retrofit.ApiRequest;
import com.example.boxifylogistics.Retrofit.RetrofitRequest;
import com.example.boxifylogistics.Utils.GPSTracker;
import com.example.boxifylogistics.Utils.GetAndSetToken;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private ApiRequest apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    private ScanDevice sm = new ScanDevice();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        // Mise en palce de la vue
        setContentView(R.layout.activity_main);
        // Confiugraiton du ViewPager et du TabLayout
        this.configureViewPagerAndTabs();
        // Mise en place de la ToolBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        GPSTracker gpsTracker = new GPSTracker(this);

        // Intent pour récupérer le nom du livreur
        Intent intent = getIntent();
        if (intent != null){
            // Récupération des extras de l'intent
            if (intent.hasExtra("logisticianFirstname")){ // vérifie qu'une valeur est associée à la clé “edittext”
                Toast.makeText(this , "Bon retour "+intent.getStringExtra("logisticianFirstname")+" !", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this , "Bonjour !", Toast.LENGTH_LONG).show();
        }


    }

    /**
     * Configuration du ViewPager et du TabLayout
     */
    private void configureViewPagerAndTabs(){
        /// On récupère le ViewPager du layout
        ViewPager pager = findViewById(R.id.activity_main_viewpager);
        // On set l'Adapter et on le colle avec le PageAdapter
        pager.setAdapter(new PageAdapter(getSupportFragmentManager()));
        // Récupération du TabLayout du layout
        TabLayout tabs= (TabLayout)findViewById(R.id.activity_main_tabs);
        // On colle le TabLayout et le ViewPager ensemble
        tabs.setupWithViewPager(pager);
        // On fixe le TabLayout
        tabs.setTabMode(TabLayout.MODE_FIXED);
    }

    /**
     * Si on appuie sur le bouton retour, il ne se passe rien
     */
    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.e(TAG,"keyCode : "+keyCode);
        if(keyCode == 131){
            Intent intent = new Intent(this, IndexationActivity.class);
            finish();
            startActivity(intent);
        }
        if(keyCode == 132){
            Intent intent = new Intent(this, PalletizationActivity.class);
            finish();
            startActivity(intent);
        }

        if(keyCode == 67){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true)
                    .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    })
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'OUI' Button
                            String url = "https://waze.com/ul?q=Weihoek%203Weihoek%201930Weihoek%20Zaventem%20Belgique";
                            Log.e(TAG,url);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);
                        }
                    })
            ;
            builder.setTitle("On rentre à l'entrepôt ?");
            builder.create().show();
        }
        return super.onKeyDown(keyCode, event);
    }





    /* MODIFICATION DU WEEKEND */
    /**
     * Méthode appelée à la création du menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Méthode permettant de gérer l'appui sur les différents items du menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Êtes-vous sûr de vouloir vous déconnecter ?")
                        .setCancelable(false)
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        })
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                logout();
                            }
                        })
                ;
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Attention");
                alert.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void logout(){
        apiRequest.logisticianLogout((GetAndSetToken.getBearerWithToken())).enqueue(new Callback<JsonObject>(){
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                // Si on reçoit un "OK"
                if(response.code() == 200){
                    Toast.makeText(MainActivity.this , "Vous vous êtes déconnecté", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else{
                    Toast.makeText(MainActivity.this , "Impossible de se déconnecter", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG,t.getMessage());
                Toast.makeText(MainActivity.this , "Impossible de se déconnecter", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        sm.closeScan();
    }
}
